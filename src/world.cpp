#include <algorithm>
#include <raytracer/colors.hpp>
#include <raytracer/schlick.hpp>
#include <raytracer/transforms.hpp>
#include <raytracer/world.hpp>

Color World::color_at(const Ray &ray, std::vector<Intersection> &intersections,
                      int remaining) {
  intersect(ray, intersections);
  auto hit_intersection = Intersection::hit(intersections);

  if (hit_intersection) {
    IntersectionData intersection_data(*hit_intersection, ray, intersections);
    return shade_hit(intersection_data, intersections, remaining);
  }

  return background_color;
}

void World::intersect(const Ray &ray,
                      std::vector<Intersection> &intersections) {
  intersections.clear();

  auto add_intersections_for_shapes = [&](auto &shapes) {
    for (auto &shape : shapes) {
      shape.intersect(ray, intersections);
    }
  };

  add_intersections_for_shapes(spheres);
  add_intersections_for_shapes(planes);
  add_intersections_for_shapes(cubes);
  add_intersections_for_shapes(cylinders);
  add_intersections_for_shapes(cones);
  add_intersections_for_shapes(csgs);
  add_intersections_for_shapes(groups);

  std::sort(intersections.begin(), intersections.end());
}

Color World::shade_hit(const IntersectionData &intersection_data,
                       std::vector<Intersection> &intersections,
                       int remaining) {
  Color surface = intersection_data.object->material.lighting(
      intersection_data.object, light, intersection_data.intersection_point,
      intersection_data.eye_vector, intersection_data.normal_vector,
      is_shadowed(intersection_data.over_point, intersections));
  Color reflected =
      reflected_color(intersection_data, intersections, remaining);
  Color refracted =
      refracted_color(intersection_data, intersections, remaining);

  Material &material = intersection_data.object->material;
  if (material.reflective > 0.0f && material.transparency > 0.0f) {
    float reflectance = schlick(intersection_data);
    return surface + reflected * reflectance + refracted * (1 - reflectance);
  }
  return surface + reflected + refracted;
}

bool World::is_shadowed(const Point &query_point,
                        std::vector<Intersection> &intersections) {
  Vec light_to_query_point_vector = light.position - query_point;
  float dist = mag(light_to_query_point_vector);
  Vec direction = norm(light_to_query_point_vector);

  Ray ray(query_point, direction);
  intersect(ray, intersections);
  auto hit_intersection = Intersection::hit(intersections);

  // if there was a hit and it happened in between the light and the point
  return hit_intersection && (*hit_intersection).t < dist;
}

Color World::reflected_color(const IntersectionData &intersection_data,
                             std::vector<Intersection> &intersections,
                             int remaining) {
  if (remaining <= 0 ||
      eql(intersection_data.object->material.reflective, 0.0f)) {
    return colors::black;
  }

  Ray reflect_ray(intersection_data.over_point,
                  intersection_data.reflect_vector);
  Color c = color_at(reflect_ray, intersections, --remaining);

  return c * intersection_data.object->material.reflective;
}

Color World::refracted_color(const IntersectionData &intersection_data,
                             std::vector<Intersection> &intersections,
                             int remaining) {
  if (remaining <= 0 ||
      eql(intersection_data.object->material.transparency, 0.0f)) {
    return colors::black;
  }

  float refractive_index_ratio = intersection_data.exiting_refractive_index /
                                 intersection_data.entering_refractive_index;
  float cos_i =
      dot(intersection_data.eye_vector, intersection_data.normal_vector);
  float sin2_t =
      refractive_index_ratio * refractive_index_ratio * (1 - cos_i * cos_i);

  // total internal reflection
  if (sin2_t > 1.0f) {
    return colors::black;
  }

  float cos_t = sqrtf(1.0f - sin2_t);
  Vec direction = intersection_data.normal_vector *
                      (refractive_index_ratio * cos_i - cos_t) -
                  intersection_data.eye_vector * refractive_index_ratio;

  Ray refracted_ray(intersection_data.under_point, direction);

  Color c = color_at(refracted_ray, intersections, remaining - 1) *
            intersection_data.object->material.transparency;

  return c;
}

World default_world() {
  Sphere sphere1;
  sphere1.material.color = color(0.8, 1.0, 0.6);
  sphere1.material.diffuse = 0.7f;
  sphere1.material.specular = 0.2f;

  Sphere sphere2;
  sphere2.set_transform(scaling(0.5, 0.5, 0.5));

  World world(Light(point(-10, 10, -10), color(1, 1, 1)));
  world.spheres = {sphere1, sphere2};

  return world;
}
