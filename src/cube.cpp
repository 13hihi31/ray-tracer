#include <algorithm>
#include <cmath>
#include <limits>
#include <raytracer/constants.hpp>
#include <raytracer/cube.hpp>

Cube::Cube(const Matrix<4> &transform, const Material &material)
    : Shape(transform, material) {}

Vec Cube::local_normal_at(const Point &local_point,
                          const Intersection &intersection) const {
  float maxc =
      std::max(std::abs(local_point.x),
               std::max(std::abs(local_point.y), std::abs(local_point.z)));
  if (maxc == std::abs(local_point.x))
    return vec(local_point.x, 0, 0);
  else if (maxc == std::abs(local_point.y))
    return vec(0, local_point.y, 0);
  return vec(0, 0, local_point.z);
}

struct Bounds {
  float min;
  float max;
};

Bounds check_axis(float origin, float direction) {
  float tmin_numerator = (-1 - origin);
  float tmax_numerator = (1 - origin);

  float tmin = tmin_numerator / direction;
  float tmax = tmax_numerator / direction;
  if (std::abs(direction) < consts::epsilon) {
    tmin = tmin_numerator * std::numeric_limits<float>::max();
    tmax = tmax_numerator * std::numeric_limits<float>::max();
  }

  if (tmin > tmax) std::swap(tmin, tmax);

  return {.min = tmin, .max = tmax};
}

void Cube::local_intersect(const Ray &local_ray,
                           std::vector<Intersection> &intersections) {
  Bounds xt = check_axis(local_ray.origin.x, local_ray.direction.x);
  Bounds yt = check_axis(local_ray.origin.y, local_ray.direction.y);
  Bounds zt = check_axis(local_ray.origin.z, local_ray.direction.z);

  float tmin = std::max(xt.min, std::max(yt.min, zt.min));
  float tmax = std::min(xt.max, std::min(yt.max, zt.max));

  if (tmin > tmax) return;

  Shape *s = this;

  intersections.push_back(Intersection(tmin, s));
  intersections.push_back(Intersection(tmax, s));
}

BoundingBox Cube::get_bounds() const {
  return BoundingBox(point(-1, -1, -1), point(1, 1, 1));
}
