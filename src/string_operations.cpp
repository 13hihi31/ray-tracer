#include <algorithm>
#include <raytracer/string_operations.hpp>
#include <regex>

std::vector<std::string> string_operations::split(const std::string str,
                                                  const std::string delim_str) {
  const std::regex delim_re(delim_str);
  return {std::sregex_token_iterator(str.begin(), str.end(), delim_re, -1),
          std::sregex_token_iterator()};
}

void string_operations::remove_double_spaces(std::string &line) {
  line.erase(std::unique(line.begin(), line.end(),
                         [](char lhs, char rhs) {
                           return (lhs == rhs) && (lhs == ' ');
                         }),
             line.end());
}

void string_operations::remove_character(std::string &line,
                                         const char character) {
  line.erase(std::remove(line.begin(), line.end(), character), line.end());
}
