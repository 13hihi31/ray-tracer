#include <algorithm>
#include <forward_list>
#include <raytracer/constants.hpp>
#include <raytracer/intersection_data.hpp>

IntersectionData::IntersectionData(const Intersection &intersection,
                                   const Ray &ray,
                                   const std::vector<Intersection> &ints) {
  if (!ints.empty()) {
    const std::vector<Intersection> &ints2 = ints;
  } else {
    const std::vector<Intersection> ints2 = {intersection};
  }

  t = intersection.t;
  object = intersection.object;
  intersection_point = ray.position(intersection.t);
  eye_vector = -ray.direction;
  normal_vector =
      intersection.object->normal_at(intersection_point, intersection);
  inside_object = false;
  if (dot(normal_vector, eye_vector) < 0.0f) {
    inside_object = true;
    normal_vector = -normal_vector;
  }
  over_point = intersection_point + normal_vector * consts::epsilon;
  under_point = intersection_point - normal_vector * consts::epsilon;
  reflect_vector = reflect(ray.direction, normal_vector);

  // determine the refractive indicies at the border of intersection
  std::forward_list<Shape *> inside_object_stack;
  for (const auto &i : ints) {
    if (i == intersection) {
      if (inside_object_stack.empty())
        exiting_refractive_index = 1.0f;
      else
        exiting_refractive_index =
            inside_object_stack.front()->material.refractive_index;
    }

    if (std::find(inside_object_stack.begin(), inside_object_stack.end(),
                  i.object) != inside_object_stack.end())
      inside_object_stack.remove(i.object);
    else
      inside_object_stack.push_front(i.object);

    if (i == intersection) {
      if (inside_object_stack.empty())
        entering_refractive_index = 1.0f;
      else
        entering_refractive_index =
            inside_object_stack.front()->material.refractive_index;

      break;
    }
  }
}
