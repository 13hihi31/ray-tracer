#include <raytracer/triangle.hpp>

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
    : p1(p1), p2(p2), p3(p3) {
  e1 = p2 - p1;
  e2 = p3 - p1;
  normal = norm(cross(e2, e1));
}

Vec Triangle::local_normal_at(const Point &local_point,
                              const Intersection &intersection) const {
  return normal;
}

void Triangle::local_intersect(const Ray &local_ray,
                               std::vector<Intersection> &intersections) {
  Vec dir_cross_e2 = cross(local_ray.direction, e2);
  float det = dot(e1, dir_cross_e2);
  if (eql(det, 0.0f)) return;

  float f = 1.0f / det;
  Vec p1_to_origin = local_ray.origin - p1;
  float u = f * dot(p1_to_origin, dir_cross_e2);
  if (u < 0.0f || u > 1.0f) return;

  Vec origin_cross_e1 = cross(p1_to_origin, e1);
  float v = f * dot(local_ray.direction, origin_cross_e1);
  if (v < 0 || (u + v) > 1) return;

  float t = f * dot(e2, origin_cross_e1);

  Shape *s = this;
  intersections.push_back(Intersection(t, s));
}

BoundingBox Triangle::get_bounds() const {
  BoundingBox triangle_box;
  triangle_box.add_point(p1);
  triangle_box.add_point(p2);
  triangle_box.add_point(p3);
  return triangle_box;
}
