#include <algorithm>
#include <chrono>
#include <cmath>
#include <random>
#include <raytracer/camera.hpp>
#include <raytracer/intersection.hpp>
#include <raytracer/tuple.hpp>
#include <thread>
#include <vector>

Camera::Camera(unsigned horizontal_size, unsigned vertical_size,
               float field_of_view)
    : horizontal_size(horizontal_size),
      vertical_size(vertical_size),
      field_of_view(field_of_view) {
  world_transform_inverse = identity_matrix<4>();

  // the canvas is one unit away from the camera
  // then half_view / 1 = tan(filed_of_view / 2) from trigonometry
  float half_view = tanf(field_of_view / 2.0f);
  float aspect_ratio = float(horizontal_size) / float(vertical_size);

  if (aspect_ratio >= 1) {
    half_width = half_view;
    half_height = half_view / aspect_ratio;
  } else {
    half_width = half_view * aspect_ratio;
    half_height = half_view;
  }

  pixel_size = (half_width * 2) / horizontal_size;
}

Canvas Camera::render(World &world, size_t initial_intersections_number,
                      unsigned num_samples_per_pixel, float antialias_threshold,
                      bool paint_edges_only) {
  Canvas canvas(horizontal_size, vertical_size);

  render_lines(world, canvas, 0, vertical_size, initial_intersections_number);

  return canvas;
}

Canvas Camera::render_threaded(World &world, unsigned threads_number,
                               size_t initial_intersections_number,
                               unsigned num_samples_per_pixel,
                               float antialias_threshold,
                               bool paint_edges_only) {
  Canvas canvas(horizontal_size, vertical_size);

  std::vector<std::thread> threads;
  threads_number = std::max(
      1u, std::min(threads_number, std::thread::hardware_concurrency()));

  unsigned lines_per_thread = vertical_size / threads_number;
  unsigned start_line = 0;
  unsigned end_line = lines_per_thread;

  for (unsigned thread_idx = 0; thread_idx < threads_number - 1; ++thread_idx) {
    threads.emplace_back(std::thread(
        &Camera::render_lines, this, std::ref(world), std::ref(canvas),
        start_line, end_line, initial_intersections_number));
    start_line = end_line;
    end_line += lines_per_thread;
  }

  render_lines(world, canvas, start_line, vertical_size,
               initial_intersections_number);

  for (auto &t : threads) {
    t.join();
  }

  if (num_samples_per_pixel > 1) {
    std::vector<std::thread> smoothing_threads;
    unsigned start_line = 0;
    unsigned end_line = lines_per_thread;

    Canvas canvas_antialiased(canvas);

    for (unsigned thread_idx = 0; thread_idx < threads_number - 1;
         ++thread_idx) {
      smoothing_threads.emplace_back(std::thread(
          &Camera::smoothout_edges, this, std::ref(world), std::ref(canvas),
          std::ref(canvas_antialiased), start_line, end_line,
          initial_intersections_number, num_samples_per_pixel,
          antialias_threshold, paint_edges_only));
      start_line = end_line;
      end_line += lines_per_thread;
    }

    smoothout_edges(world, canvas, canvas_antialiased, start_line,
                    vertical_size, initial_intersections_number,
                    num_samples_per_pixel, antialias_threshold,
                    paint_edges_only);

    for (auto &t : smoothing_threads) {
      t.join();
    }

    canvas = std::move(canvas_antialiased);
  }

  return canvas;
}

void Camera::render_lines(World &world, Canvas &canvas, unsigned start_line,
                          unsigned end_line,
                          size_t initial_intersections_number) const {
  std::vector<Intersection> intersections;
  intersections.reserve(initial_intersections_number);

  for (unsigned pixel_y_idx = start_line; pixel_y_idx < end_line;
       ++pixel_y_idx) {
    for (unsigned pixel_x_idx = 0; pixel_x_idx < horizontal_size;
         ++pixel_x_idx) {
      canvas(pixel_x_idx, pixel_y_idx) = world.color_at(
          ray_for_pixel(pixel_x_idx, pixel_y_idx), intersections);
    }
  }
}

Ray Camera::ray_for_pixel(unsigned canvas_pixel_x_index,
                          unsigned canvas_pixel_y_index, float pixel_x_offset,
                          float pixel_y_offset) const {
  // the offset from the edge of the canvas to the pixel's center
  float canvas_pixel_x_center_position =
      (canvas_pixel_x_index + pixel_x_offset) * pixel_size;
  float canvas_pixel_y_center_position =
      (canvas_pixel_y_index + pixel_y_offset) * pixel_size;

  // the untransformed coordinates of the pixel in camera space
  float canvas_pixel_x_camera_position =
      half_width - canvas_pixel_x_center_position;
  float canvas_pixel_y_camera_position =
      half_height - canvas_pixel_y_center_position;
  constexpr float canvas_pixel_z_camera_position = -1.f;

  // the camera looks from the origin of the coordinate system onto a canvas
  // compute the ray components in the world by transforming the ray from camera
  // space to world space
  // the camera transform is actualy a view transform that transforms the world
  // so that the camera being at the origin sees the world as if it was
  // transformed in space thus taking the inverse of this transform allows
  // transforming ray components from camera space to world space

  Point pixel =
      mul(world_transform_inverse,
          point(canvas_pixel_x_camera_position, canvas_pixel_y_camera_position,
                canvas_pixel_z_camera_position));
  Point origin = mul(world_transform_inverse, point(0.0f, 0.0f, 0.0f));
  Vec direction = norm(pixel - origin);

  // return a ray in world space for given canvas pixel indicies
  return Ray(origin, direction);
}

void Camera::smoothout_edges(World &world, Canvas &canvas,
                             Canvas &canvas_antialiased, unsigned start_line,
                             unsigned end_line,
                             size_t initial_intersections_number,
                             unsigned num_samples_per_pixel,
                             float antialias_threshold, bool paint_edges_only) {
  std::vector<Intersection> intersections;
  intersections.reserve(initial_intersections_number);

  std::default_random_engine generator_engine;
  generator_engine.seed(
      std::chrono::system_clock::now().time_since_epoch().count());

  std::uniform_real_distribution<float> offset_distribution(0.f, 1.f);

  std::vector<float> x_offsets(num_samples_per_pixel, 0.f);
  for (auto &x : x_offsets) x = offset_distribution(generator_engine);

  std::vector<float> y_offsets(num_samples_per_pixel, 0.f);
  for (auto &y : y_offsets) y = offset_distribution(generator_engine);

  // do not apply smoothing at the border of the image
  if (start_line == 0) ++start_line;
  if (end_line == vertical_size) --end_line;

  for (unsigned pixel_y_idx = start_line; pixel_y_idx < end_line;
       ++pixel_y_idx) {
    // do not apply smoothing at the border of the image
    for (unsigned pixel_x_idx = 1; pixel_x_idx < horizontal_size - 1;
         ++pixel_x_idx) {
      Color laplacian = canvas.laplacian(pixel_x_idx, pixel_y_idx);

      if (std::abs(laplacian.x) > antialias_threshold ||
          std::abs(laplacian.y) > antialias_threshold ||
          std::abs(laplacian.z) > antialias_threshold) {
        if (paint_edges_only) {
          canvas_antialiased(pixel_x_idx, pixel_y_idx) = color(1.f, 1.f, 1.f);
        } else {
          Color sampled_average_color;

          for (unsigned sample_i = 0; sample_i < num_samples_per_pixel;
               ++sample_i) {
            sampled_average_color =
                sampled_average_color +
                world.color_at(
                    ray_for_pixel(pixel_x_idx, pixel_y_idx, x_offsets[sample_i],
                                  y_offsets[sample_i]),
                    intersections);
          }

          canvas_antialiased(pixel_x_idx, pixel_y_idx) =
              sampled_average_color * (1.f / num_samples_per_pixel);
        }
      } else if (paint_edges_only) {
        canvas_antialiased(pixel_x_idx, pixel_y_idx) = color(0.f, 0.f, 0.f);
      }
    }
  }
}

void Camera::set_transform(const Matrix<4> &transform) {
  world_transform_inverse = transform.inverse();
}
