#include <raytracer/shape.hpp>

Shape::Shape() {
  transform = identity_matrix<4>();
  transform_inverse = identity_matrix<4>();
}

Shape::Shape(const Matrix<4> &transform, const Material &material)
    : transform(transform),
      transform_inverse(transform.inverse()),
      material(material) {}

Shape &Shape::operator=(const Shape &other) {
  if (this == &other) return *this;

  transform = other.transform;
  transform_inverse = other.transform_inverse;
  material = other.material;

  return *this;
}

Vec Shape::normal_at(const Point &point, const Intersection &intersection) {
  Point local_point = world_to_object(point);
  Vec local_normal = local_normal_at(local_point, intersection);
  return normal_to_world(local_normal);
}

Point Shape::world_to_object(Point point) const {
  if (parent != nullptr) point = parent->world_to_object(point);

  return mul(transform_inverse, point);
}

Vec Shape::normal_to_world(Vec normal) const {
  normal = mul(transform_inverse.transpose(), normal);
  normal.w = 0;
  normal = norm(normal);

  if (parent != nullptr) normal = parent->normal_to_world(normal);
  return normal;
}

void Shape::intersect(const Ray &ray,
                      std::vector<Intersection> &intersections) {
  local_intersect(ray.transform(transform_inverse), intersections);
}

void Shape::set_transform(const Matrix<4> &new_transform) {
  transform = new_transform;
  transform_inverse = new_transform.inverse();
}

void Shape::set_parent(Shape *new_parent) { parent = new_parent; }

void Shape::set_material(const Material &new_material) {
  material = new_material;
}

bool Shape::eq(const Shape &other) const { return this == &other; }

bool Shape::includes(const Shape *other) const { return this == other; }

BoundingBox Shape::get_bounds() const { return BoundingBox(); }

void Shape::divide(unsigned division_granularity) {}

BoundingBox Shape::parent_space_bounds() const {
  return get_bounds().transform(transform);
}
