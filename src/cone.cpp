#include <algorithm>
#include <cmath>
#include <raytracer/cone.hpp>
#include <raytracer/constants.hpp>
#include <raytracer/helper_functions.hpp>

Cone::Cone(const Matrix<4> &transform, const Material &material)
    : Shape(transform, material) {}

Vec Cone::local_normal_at(const Point &local_point,
                          const Intersection &intersection) const {
  float dist =
      sqrtf(local_point.x * local_point.x + local_point.z * local_point.z);
  if (dist < std::abs(maximum) && local_point.y >= maximum - consts::epsilon) {
    return vec(0, 1, 0);
  } else if (dist < std::abs(minimum) &&
             local_point.y <= minimum + consts::epsilon) {
    return vec(0, -1, 0);
  } else {
    if (local_point.y > 0.0f) dist = -dist;
    return vec(local_point.x, dist, local_point.z);
  }
}

bool check_caps(const Ray &ray, float t, float radius) {
  float x = ray.origin.x + t * ray.direction.x;
  float z = ray.origin.z + t * ray.direction.z;
  float d = x * x + z * z;
  return d < radius || eql(d, radius);
}

void Cone::local_intersect(const Ray &local_ray,
                           std::vector<Intersection> &intersections) {
  Shape *s = this;

  // intersect caps
  if (closed && std::abs(local_ray.direction.y) > consts::epsilon) {
    float t = (minimum - local_ray.origin.y) / local_ray.direction.y;
    if (check_caps(local_ray, t, std::abs(minimum)))
      intersections.push_back(Intersection(t, s));

    t = (maximum - local_ray.origin.y) / local_ray.direction.y;
    if (check_caps(local_ray, t, std::abs(maximum)))
      intersections.push_back(Intersection(t, s));
  }

  float a = local_ray.direction.x * local_ray.direction.x +
            local_ray.direction.z * local_ray.direction.z -
            local_ray.direction.y * local_ray.direction.y;
  float b = 2 * local_ray.origin.x * local_ray.direction.x +
            2 * local_ray.origin.z * local_ray.direction.z -
            2 * local_ray.origin.y * local_ray.direction.y;
  float c = local_ray.origin.x * local_ray.origin.x +
            local_ray.origin.z * local_ray.origin.z -
            local_ray.origin.y * local_ray.origin.y;

  if (eql(a, 0.0f)) {
    if (!eql(b, 0.0f)) {
      intersections.push_back(Intersection(-c / (2 * b), s));
    }
    return;
  }

  float disc = b * b - 4 * a * c;

  if (disc < 0) return;

  float t0 = (-b - sqrtf(disc)) / (2 * a);
  float t1 = (-b + sqrtf(disc)) / (2 * a);

  if (t0 > t1) std::swap(t0, t1);

  float y0 = local_ray.origin.y + t0 * local_ray.direction.y;
  if (minimum < y0 && y0 < maximum)
    intersections.push_back(Intersection(t0, s));

  float y1 = local_ray.origin.y + t1 * local_ray.direction.y;
  if (minimum < y1 && y1 < maximum)
    intersections.push_back(Intersection(t1, s));
}

BoundingBox Cone::get_bounds() const {
  float limit = std::max(std::abs(maximum), std::abs(minimum));
  return BoundingBox(point(-limit, minimum, -limit),
                     point(limit, maximum, limit));
}
