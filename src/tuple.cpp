#include <cmath>
#include <raytracer/helper_functions.hpp>
#include <raytracer/tuple.hpp>

bool Tuple::operator==(const Tuple &other) const {
  return eql(x, other.x) && eql(y, other.y) && eql(z, other.z) &&
         eql(w, other.w);
}

Tuple Tuple::operator+(const Tuple &other) const {
  return Tuple(x + other.x, y + other.y, z + other.z, w + other.w);
}

Tuple Tuple::operator-(const Tuple &other) const {
  return Tuple(x - other.x, y - other.y, z - other.z, w - other.w);
}

Tuple Tuple::operator*(const Tuple &other) const {
  return Tuple(x * other.x, y * other.y, z * other.z, w * other.w);
}

Tuple Tuple::operator*(float scale) const {
  return Tuple(x * scale, y * scale, z * scale, w * scale);
}

Tuple operator-(const Tuple &t) { return Tuple(-t.x, -t.y, -t.z, -t.w); }

float mag(const Tuple &t) {
  return sqrtf(t.x * t.x + t.y * t.y + t.z * t.z + t.w * t.w);
}

Tuple norm(const Tuple &t) {
  float m = mag(t);
  return Tuple(t.x / m, t.y / m, t.z / m, t.w / m);
}

float dot(const Tuple &t1, const Tuple &t2) {
  return t1.x * t2.x + t1.y * t2.y + t1.z * t2.z + t1.w * t2.w;
}

Tuple cross(const Tuple &t1, const Tuple &t2) {
  return vec(t1.y * t2.z - t1.z * t2.y, t1.z * t2.x - t1.x * t2.z,
             t1.x * t2.y - t1.y * t2.x);
}

std::ostream &operator<<(std::ostream &stream, const Tuple &a) {
  stream << a.x << " " << a.y << " " << a.z << " " << a.w;
  return stream;
}

Tuple point(float x, float y, float z) { return Tuple(x, y, z, 1.0f); }

Tuple vec(float x, float y, float z) { return Tuple(x, y, z, 0.0f); }

Tuple color(float red, float green, float blue) {
  return vec(red, green, blue);
}

bool pointp(const Tuple &a) { return eql(a.w, 1.0f); }

bool vecp(const Tuple &a) { return eql(a.w, 0.0f); }

Vec reflect(const Vec &in, const Vec &normal) {
  return in - normal * (2.0f * dot(in, normal));
}
