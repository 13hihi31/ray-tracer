#include <raytracer/intersection.hpp>

Intersection::Intersection(float t, Shape *object) : t(t), object(object) {}

Intersection::Intersection(float t, Shape *object, float u, float v)
    : t(t), object(object), u(u), v(v) {}

bool Intersection::operator==(const Intersection &other) const {
  return t == other.t && object == other.object;
}

bool Intersection::operator<(const Intersection &other) const {
  return t < other.t;
}

std::optional<Intersection> Intersection::hit(
    const std::vector<Intersection> &intersections) {
  if (intersections.empty()) {
    return {};
  } else {
    for (const auto &in : intersections) {
      if (in.t >= 0.0f) return in;
    }
  }
  return {};
}
