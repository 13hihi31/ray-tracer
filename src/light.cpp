#include <raytracer/light.hpp>
#include <raytracer/tuple.hpp>

Light::Light(const Point &position, const Color &intensity)
    : position(position), intensity(intensity) {}

bool Light::operator==(const Light &other) const {
  return position == other.position && intensity == other.intensity;
}
