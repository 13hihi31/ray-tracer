#include <cmath>
#include <raytracer/schlick.hpp>

float schlick(const IntersectionData &intersection_data) {
  float cos =
      dot(intersection_data.eye_vector, intersection_data.normal_vector);

  // total internal reflection can only occur if exiting_refractive_index >
  // entering_refractive_index
  if (intersection_data.exiting_refractive_index >
      intersection_data.entering_refractive_index) {
    float refractive_index_ratio = intersection_data.exiting_refractive_index /
                                   intersection_data.entering_refractive_index;
    float sin2_t =
        refractive_index_ratio * refractive_index_ratio * (1.0f - cos * cos);
    if (sin2_t > 1.0f) return 1.0f;

    cos = sqrtf(1.0f - sin2_t);
  }

  float r0 = ((intersection_data.exiting_refractive_index -
               intersection_data.entering_refractive_index) /
              (intersection_data.exiting_refractive_index +
               intersection_data.entering_refractive_index));
  r0 = r0 * r0;
  return r0 +
         (1 - r0) * (1 - cos) * (1 - cos) * (1 - cos) * (1 - cos) * (1 - cos);
}
