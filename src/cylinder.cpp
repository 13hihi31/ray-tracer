#include <algorithm>
#include <cmath>
#include <limits>
#include <raytracer/constants.hpp>
#include <raytracer/cylinder.hpp>
#include <raytracer/helper_functions.hpp>

Cylinder::Cylinder(const Matrix<4> &transform, const Material &material)
    : Shape(transform, material) {}

Vec Cylinder::local_normal_at(const Point &local_point,
                              const Intersection &intersection) const {
  float dist = local_point.x * local_point.x + local_point.z * local_point.z;
  if (dist < 1.0f && local_point.y >= maximum - consts::epsilon) {
    return vec(0, 1, 0);
  } else if (dist < 1.0f && local_point.y <= minimum + consts::epsilon) {
    return vec(0, -1, 0);
  } else {
    return vec(local_point.x, 0, local_point.z);
  }
}

bool check_caps(const Ray &ray, float t) {
  float x = ray.origin.x + t * ray.direction.x;
  float z = ray.origin.z + t * ray.direction.z;
  float d = x * x + z * z;
  return d < 1.0f || eql(d, 1.0f);
}

void Cylinder::local_intersect(const Ray &local_ray,
                               std::vector<Intersection> &intersections) {
  Shape *s = this;

  // intersect caps
  if (closed && std::abs(local_ray.direction.y) > consts::epsilon) {
    float t = (minimum - local_ray.origin.y) / local_ray.direction.y;
    if (check_caps(local_ray, t)) intersections.push_back(Intersection(t, s));

    t = (maximum - local_ray.origin.y) / local_ray.direction.y;
    if (check_caps(local_ray, t)) intersections.push_back(Intersection(t, s));
  }

  float a = local_ray.direction.x * local_ray.direction.x +
            local_ray.direction.z * local_ray.direction.z;
  if (a < consts::epsilon) return;

  float b = 2 * local_ray.origin.x * local_ray.direction.x +
            2 * local_ray.origin.z * local_ray.direction.z;
  float c = local_ray.origin.x * local_ray.origin.x +
            local_ray.origin.z * local_ray.origin.z - 1;
  float disc = b * b - 4 * a * c;

  if (disc < 0) return;

  float t0 = (-b - sqrtf(disc)) / (2 * a);
  float t1 = (-b + sqrtf(disc)) / (2 * a);

  if (t0 > t1) std::swap(t0, t1);

  float y0 = local_ray.origin.y + t0 * local_ray.direction.y;
  if (minimum < y0 && y0 < maximum)
    intersections.push_back(Intersection(t0, s));

  float y1 = local_ray.origin.y + t1 * local_ray.direction.y;
  if (minimum < y1 && y1 < maximum)
    intersections.push_back(Intersection(t1, s));
}

BoundingBox Cylinder::get_bounds() const {
  return BoundingBox(point(-1, minimum, -1), point(1, maximum, 1));
}
