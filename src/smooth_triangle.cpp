#include <raytracer/smooth_triangle.hpp>

SmoothTriangle::SmoothTriangle(const Point &p1, const Point &p2,
                               const Point &p3, const Vec &n1, const Vec &n2,
                               const Vec &n3)
    : Triangle(p1, p2, p3), n1(n1), n2(n2), n3(n3) {}

Vec SmoothTriangle::local_normal_at(const Point &local_point,
                                    const Intersection &intersection) const {
  return n2 * intersection.u + n3 * intersection.v +
         n1 * (1 - intersection.u - intersection.v);
}

void SmoothTriangle::local_intersect(const Ray &local_ray,
                                     std::vector<Intersection> &intersections) {
  Vec dir_cross_e2 = cross(local_ray.direction, e2);
  float det = dot(e1, dir_cross_e2);
  if (eql(det, 0.0f)) return;

  float f = 1.0f / det;
  Vec p1_to_origin = local_ray.origin - p1;
  float u = f * dot(p1_to_origin, dir_cross_e2);
  if (u < 0.0f || u > 1.0f) return;

  Vec origin_cross_e1 = cross(p1_to_origin, e1);
  float v = f * dot(local_ray.direction, origin_cross_e1);
  if (v < 0 || (u + v) > 1) return;

  float t = f * dot(e2, origin_cross_e1);

  Shape *s = this;
  intersections.push_back(Intersection(t, s, u, v));
}
