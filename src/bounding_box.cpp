#include <limits>
#include <raytracer/bounding_box.hpp>
#include <raytracer/constants.hpp>

BoundingBox::BoundingBox() {
  min_point = point(std::numeric_limits<float>::max(),
                    std::numeric_limits<float>::max(),
                    std::numeric_limits<float>::max());

  max_point = point(std::numeric_limits<float>::lowest(),
                    std::numeric_limits<float>::lowest(),
                    std::numeric_limits<float>::lowest());
}

BoundingBox::BoundingBox(const BoundingBox &other) {
  min_point = other.min_point;
  max_point = other.max_point;
}

BoundingBox &BoundingBox::operator=(const BoundingBox &other) {
  if (this == &other) return *this;
  min_point = other.min_point;
  max_point = other.max_point;
  return *this;
}

BoundingBox::BoundingBox(const Point &min_point, const Point &max_point)
    : min_point(min_point), max_point(max_point) {}

void BoundingBox::add_point(const Point &point) {
  min_point.x = std::min(min_point.x, point.x);
  min_point.y = std::min(min_point.y, point.y);
  min_point.z = std::min(min_point.z, point.z);

  max_point.x = std::max(max_point.x, point.x);
  max_point.y = std::max(max_point.y, point.y);
  max_point.z = std::max(max_point.z, point.z);
}

void BoundingBox::add_box(const BoundingBox &box) {
  min_point.x = std::min(min_point.x, box.min_point.x);
  min_point.y = std::min(min_point.y, box.min_point.y);
  min_point.z = std::min(min_point.z, box.min_point.z);

  max_point.x = std::max(max_point.x, box.max_point.x);
  max_point.y = std::max(max_point.y, box.max_point.y);
  max_point.z = std::max(max_point.z, box.max_point.z);
}

bool BoundingBox::contains(const Point &point) const {
  return point.x >= min_point.x && point.x <= max_point.x &&
         point.y >= min_point.y && point.y <= max_point.y &&
         point.z >= min_point.z && point.z <= max_point.z;
}

bool BoundingBox::contains(const BoundingBox &box) const {
  return box.min_point.x >= min_point.x && box.min_point.y >= min_point.y &&
         box.min_point.z >= min_point.z && box.max_point.x <= max_point.x &&
         box.max_point.y <= max_point.y && box.max_point.z <= max_point.z;
}

BoundingBox BoundingBox::transform(const Matrix<4> &transformation) const {
  BoundingBox transformed_box;
  transformed_box.add_point(
      mul(transformation, point(min_point.x, min_point.y, min_point.z)));
  transformed_box.add_point(
      mul(transformation, point(min_point.x, min_point.y, max_point.z)));
  transformed_box.add_point(
      mul(transformation, point(min_point.x, max_point.y, min_point.z)));
  transformed_box.add_point(
      mul(transformation, point(min_point.x, max_point.y, max_point.z)));
  transformed_box.add_point(
      mul(transformation, point(max_point.x, min_point.y, min_point.z)));
  transformed_box.add_point(
      mul(transformation, point(max_point.x, min_point.y, max_point.z)));
  transformed_box.add_point(
      mul(transformation, point(max_point.x, max_point.y, min_point.z)));
  transformed_box.add_point(
      mul(transformation, point(max_point.x, max_point.y, max_point.z)));
  return transformed_box;
}

bool BoundingBox::intersects(const Ray &ray) const {
  struct Bounds {
    float min;
    float max;
  };

  auto check_axis = [](float origin, float direction, float min,
                       float max) -> Bounds {
    float tmin_numerator = (min - origin);
    float tmax_numerator = (max - origin);

    float tmin = tmin_numerator / direction;
    float tmax = tmax_numerator / direction;
    if (std::abs(direction) < consts::epsilon) {
      tmin = tmin_numerator * std::numeric_limits<float>::max();
      tmax = tmax_numerator * std::numeric_limits<float>::max();
    }

    if (tmin > tmax) std::swap(tmin, tmax);

    return {.min = tmin, .max = tmax};
  };

  Bounds xt =
      check_axis(ray.origin.x, ray.direction.x, min_point.x, max_point.x);
  Bounds yt =
      check_axis(ray.origin.y, ray.direction.y, min_point.y, max_point.y);
  Bounds zt =
      check_axis(ray.origin.z, ray.direction.z, min_point.z, max_point.z);

  float tmin = std::max(xt.min, std::max(yt.min, zt.min));
  float tmax = std::min(xt.max, std::min(yt.max, zt.max));

  return (tmin <= tmax);
}

std::pair<BoundingBox, BoundingBox> BoundingBox::split() const {
  float dx = max_point.x - min_point.x;
  float dy = max_point.y - min_point.y;
  float dz = max_point.z - min_point.z;

  float greatest_box_side = std::max(dx, std::max(dy, dz));

  float split_min_x = min_point.x;
  float split_min_y = min_point.y;
  float split_min_z = min_point.z;

  float split_max_x = max_point.x;
  float split_max_y = max_point.y;
  float split_max_z = max_point.z;

  if (greatest_box_side == dx) {
    split_min_x += dx / 2.f;
    split_max_x = split_min_x;
  } else if (greatest_box_side == dy) {
    split_min_y += dy / 2.f;
    split_max_y = split_min_y;
  } else {
    split_min_z += dz / 2.f;
    split_max_z = split_min_z;
  }

  Point split_min_point = point(split_min_x, split_min_y, split_min_z);
  Point split_max_point = point(split_max_x, split_max_y, split_max_z);

  return {BoundingBox(min_point, split_max_point),
          BoundingBox(split_min_point, max_point)};
}
