#include <raytracer/group.hpp>
#include <stdexcept>

Group::Group(const Group &other) : Shape(other) {
  shapes = other.shapes;

  bounds = other.bounds;
  is_empty = other.is_empty;
  count = other.count;

  set_parent_for_all_in_group();
}

Group::Group(Group &&other) noexcept
    : Shape(std::move(other)), shapes(std::move(other.shapes)) {
  bounds = other.bounds;
  is_empty = other.is_empty;
  count = other.count;

  set_parent_for_all_in_group();
}

Group &Group::operator=(const Group &other) {
  if (this == &other) return *this;

  Shape::operator=(other);

  shapes = other.shapes;

  bounds = other.bounds;
  is_empty = other.is_empty;
  count = other.count;

  set_parent_for_all_in_group();

  return *this;
}

Group &Group::operator=(Group &&other) noexcept {
  if (this == &other) return *this;

  Shape::operator=(std::move(other));

  shapes = std::move(other.shapes);

  bounds = other.bounds;
  is_empty = other.is_empty;
  count = other.count;

  set_parent_for_all_in_group();

  return *this;
}

void Group::set_parent(Shape *new_parent) {
  parent = new_parent;
  for (auto &x : get<Group>()) x.set_parent(this);
}

void Group::set_material(const Material &new_material) {
  for (auto &x : get<Sphere>()) x.set_material(new_material);
  for (auto &x : get<Plane>()) x.set_material(new_material);
  for (auto &x : get<Cube>()) x.set_material(new_material);
  for (auto &x : get<Cylinder>()) x.set_material(new_material);
  for (auto &x : get<Cone>()) x.set_material(new_material);
  for (auto &x : get<CSG>()) x.set_material(new_material);
  for (auto &x : get<Triangle>()) x.set_material(new_material);
  for (auto &x : get<SmoothTriangle>()) x.set_material(new_material);
  for (auto &x : get<Group>()) x.set_material(new_material);
}

bool Group::includes(const Shape *other) const {
  for (auto &x : get<Sphere>())
    if (x.includes(other)) return true;
  for (auto &x : get<Plane>())
    if (x.includes(other)) return true;
  for (auto &x : get<Cube>())
    if (x.includes(other)) return true;
  for (auto &x : get<Cylinder>())
    if (x.includes(other)) return true;
  for (auto &x : get<Cone>())
    if (x.includes(other)) return true;
  for (auto &x : get<CSG>())
    if (x.includes(other)) return true;
  for (auto &x : get<Triangle>())
    if (x.includes(other)) return true;
  for (auto &x : get<SmoothTriangle>())
    if (x.includes(other)) return true;
  for (auto &x : get<Group>())
    if (x.includes(other)) return true;

  return false;
}

Vec Group::local_normal_at(const Point &local_point,
                           const Intersection &intersection) const {
  throw std::logic_error("a group has no local normal");
}

void Group::local_intersect(const Ray &local_ray,
                            std::vector<Intersection> &intersections) {
  if (is_empty || !bounds.intersects(local_ray)) return;

  auto add_intersections_for_shapes = [&](auto &shapes) {
    for (auto &shape : shapes) {
      shape.intersect(local_ray, intersections);
    }
  };

  add_intersections_for_shapes(get<Sphere>());
  add_intersections_for_shapes(get<Plane>());
  add_intersections_for_shapes(get<Cube>());
  add_intersections_for_shapes(get<Cylinder>());
  add_intersections_for_shapes(get<Cone>());
  add_intersections_for_shapes(get<CSG>());
  add_intersections_for_shapes(get<Triangle>());
  add_intersections_for_shapes(get<SmoothTriangle>());
  add_intersections_for_shapes(get<Group>());
}

BoundingBox Group::get_bounds() const { return bounds; }

void Group::divide(unsigned division_granularity) {
  if (division_granularity < count) {
    auto [left_group, right_group] = partition();
    if (!left_group.empty()) add_child(std::move(left_group));
    if (!right_group.empty()) add_child(std::move(right_group));
  }

  for (auto &group : get<Group>()) {
    group.divide(division_granularity);
  }
}

void Group::set_parent_for_all_in_group() {
  for (auto &x : get<Sphere>()) x.set_parent(this);
  for (auto &x : get<Plane>()) x.set_parent(this);
  for (auto &x : get<Cube>()) x.set_parent(this);
  for (auto &x : get<Cylinder>()) x.set_parent(this);
  for (auto &x : get<Cone>()) x.set_parent(this);
  for (auto &x : get<CSG>()) x.set_parent(this);
  for (auto &x : get<Triangle>()) x.set_parent(this);
  for (auto &x : get<SmoothTriangle>()) x.set_parent(this);
  for (auto &x : get<Group>()) x.set_parent(this);
}

bool Group::empty() const { return is_empty; }

std::pair<Group, Group> Group::partition() {
  auto [left_box, right_box] = bounds.split();

  Group left_group;
  Group right_group;

  count = 0;

  auto move_shapes_to_appropriate_group = [&](auto &shapes) {
    unsigned head = 0;

    for (auto &shape : shapes) {
      BoundingBox shape_bounds = shape.parent_space_bounds();
      if (left_box.contains(shape_bounds))
        left_group.add_child(std::move(shape));
      else if (right_box.contains(shape_bounds))
        right_group.add_child(std::move(shape));
      else {
        shapes[head++] = shape;
        ++count;
      }
    }

    shapes.erase(shapes.begin() + head, shapes.end());
  };

  move_shapes_to_appropriate_group(get<Sphere>());
  move_shapes_to_appropriate_group(get<Plane>());
  move_shapes_to_appropriate_group(get<Cube>());
  move_shapes_to_appropriate_group(get<Cylinder>());
  move_shapes_to_appropriate_group(get<Cone>());
  move_shapes_to_appropriate_group(get<CSG>());
  move_shapes_to_appropriate_group(get<Triangle>());
  move_shapes_to_appropriate_group(get<SmoothTriangle>());
  move_shapes_to_appropriate_group(get<Group>());

  return {left_group, right_group};
}
