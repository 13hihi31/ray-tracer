#include <raytracer/test_shape.hpp>

TestShape::TestShape(const Matrix<4> &transform, const Material &material)
    : Shape(transform, material) {}

Vec TestShape::local_normal_at(const Point &point,
                               const Intersection &intersection) const {
  return vec(point.x, point.y, point.z);
}

void TestShape::local_intersect(const Ray &local_ray,
                                std::vector<Intersection> &intersections) {
  return;
}

BoundingBox TestShape::get_bounds() const {
  return BoundingBox(point(-1, -1, -1), point(1, 1, 1));
}
