#include <raytracer/matrix.hpp>
#include <raytracer/ray.hpp>
#include <raytracer/tuple.hpp>

Ray::Ray(Point origin, Vec direction) : origin(origin), direction(direction) {}

Point Ray::position(float t) const { return origin + direction * t; }

Ray Ray::transform(const Matrix<4> &m) const {
  return Ray(mul(m, origin), mul(m, direction));
}
