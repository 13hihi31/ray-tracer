#include <raytracer/canvas.hpp>

Canvas::Canvas(unsigned width, unsigned height)
    : width(width),
      height(height),
      canvas(std::vector<std::vector<Color>>(height + 2,
                                             std::vector<Color>(width + 2))) {}

Canvas::Canvas(const Canvas& other)
    : width(other.width), height(other.height), canvas(other.canvas) {}

Canvas::Canvas(Canvas&& other) noexcept
    : width(other.width),
      height(other.height),
      canvas(std::move(other.canvas)) {}

Canvas& Canvas::operator=(const Canvas& other) {
  if (this == &other) return *this;
  width = other.width;
  height = other.height;
  canvas = other.canvas;
  return *this;
}
Canvas& Canvas::operator=(Canvas&& other) noexcept {
  if (this == &other) return *this;
  width = other.width;
  height = other.height;
  canvas = std::move(other.canvas);
  return *this;
}

Color& Canvas::operator()(unsigned x, unsigned y) {
  return canvas[y + 1][x + 1];
}

const Color& Canvas::operator()(unsigned x, unsigned y) const {
  return canvas[y + 1][x + 1];
}

std::string Canvas::to_ppm() const {
  constexpr int max_row_len = 70;
  std::string ppm = std::string("P3\n");
  ppm += std::to_string(width) + " " + std::to_string(height) + "\n255\n";

  std::string pixel_channel_string;
  unsigned row_length;

  auto add_pixel_channel_to_ppm = [&](const float pixel_channel) {
    pixel_channel_string = std::to_string(
        unsigned(std::max(0.0f, std::min(1.0f, pixel_channel)) * 255));
    if (row_length + pixel_channel_string.length() + 1 > max_row_len) {
      ppm += "\n";
      row_length = 0;
    } else if (row_length != 0) {
      ppm += " ";
      ++row_length;
    }
    ppm += pixel_channel_string;
    row_length += pixel_channel_string.length();
  };

  for (unsigned row = 0; row < height; ++row) {
    row_length = 0;
    for (unsigned col = 0; col < width; ++col) {
      const Color pixel = (*this)(col, row);
      add_pixel_channel_to_ppm(pixel.x);
      add_pixel_channel_to_ppm(pixel.y);
      add_pixel_channel_to_ppm(pixel.z);
    }
    ppm += "\n";
  }

  return ppm;
}

Color Canvas::laplacian(unsigned pixel_x_index, unsigned pixel_y_index) const {
  Color result_laplacian;

  constexpr float kernel[3][3] = {
      {0.f, 1.f, 0.f}, {1.f, -4.f, 1.f}, {0.f, 1.f, 0.f}};

  for (unsigned y = 0; y < 3; ++y) {
    for (unsigned x = 0; x < 3; ++x) {
      result_laplacian =
          result_laplacian +
          kernel[y][x] * canvas[pixel_y_index + y][pixel_x_index + x];
    }
  }

  return result_laplacian;
}
