#include <algorithm>
#include <raytracer/csg.hpp>

CSG::CSG(const Operation &operation, Shape *left, Shape *right)
    : operation(operation), left(left), right(right) {
  left->parent = this;
  right->parent = this;
  bounds.add_box(left->parent_space_bounds());
  bounds.add_box(right->parent_space_bounds());
}

void CSG::set_material(const Material &new_material) {
  left->set_material(new_material);
  right->set_material(new_material);
}

bool CSG::includes(const Shape *other) const {
  return left->includes(other) || right->includes(other);
}

Vec CSG::local_normal_at(const Point &local_point,
                         const Intersection &intersection) const {
  return vec(0.f, 0.f, 0.f);
}

void CSG::local_intersect(const Ray &local_ray,
                          std::vector<Intersection> &intersections) {
  if (!bounds.intersects(local_ray)) return;

  size_t old_size = intersections.size();

  left->intersect(local_ray, intersections);
  right->intersect(local_ray, intersections);

  filter_intersections(intersections, old_size);
}

BoundingBox CSG::get_bounds() const { return bounds; }

void CSG::divide(unsigned division_granularity) {
  left->divide(division_granularity);
  right->divide(division_granularity);
}

bool CSG::intersection_allowed(const Operation &op, bool hit_left,
                               bool inside_left, bool inside_right) const {
  switch (op) {
    case Operation::Union:
      return (hit_left && !inside_right) || (!hit_left && !inside_left);
    case Operation::Intersection:
      return (hit_left && inside_right) || (!hit_left && inside_left);
    case Operation::Difference:
      return (hit_left && !inside_right) || (!hit_left && inside_left);
    default:
      return false;
  }
}

void CSG::filter_intersections(std::vector<Intersection> &intersections,
                               const size_t old_size) const {
  bool inside_left = false;
  bool inside_right = false;

  auto new_intersections_start =
      intersections.end() - (intersections.size() - old_size);
  std::sort(new_intersections_start, intersections.end());

  intersections.erase(
      std::remove_if(new_intersections_start, intersections.end(),
                     [&](const Intersection &i) {
                       bool left_hit = left->includes(i.object);
                       bool allowed = intersection_allowed(
                           operation, left_hit, inside_left, inside_right);
                       if (left_hit)
                         inside_left = !inside_left;
                       else
                         inside_right = !inside_right;
                       return !allowed;
                     }),
      intersections.end());
}
