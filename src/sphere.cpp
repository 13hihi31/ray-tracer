#include <cmath>
#include <raytracer/intersection.hpp>
#include <raytracer/ray.hpp>
#include <raytracer/shape.hpp>
#include <raytracer/sphere.hpp>
#include <raytracer/tuple.hpp>
#include <vector>

Sphere::Sphere(const Matrix<4> &transform, const Material &material)
    : Shape(transform, material) {}

bool Sphere::operator==(const Sphere &other) const {
  return transform_inverse == other.transform_inverse;
}

Vec Sphere::local_normal_at(const Point &local_point,
                            const Intersection &intersection) const {
  Vec local_normal = local_point - point(0, 0, 0);
  return local_normal;
}

void Sphere::local_intersect(const Ray &local_ray,
                             std::vector<Intersection> &intersections) {
  Vec sphere_to_ray = local_ray.origin - point(0, 0, 0);
  float a = dot(local_ray.direction, local_ray.direction);
  float b = 2 * dot(local_ray.direction, sphere_to_ray);
  float c = dot(sphere_to_ray, sphere_to_ray) - 1;
  float discriminant = b * b - 4 * a * c;

  if (discriminant < 0) return;

  float t1 = (-b - sqrtf(discriminant)) / (2 * a);
  float t2 = (-b + sqrtf(discriminant)) / (2 * a);

  Shape *s = this;

  intersections.push_back(Intersection(t1, s));
  intersections.push_back(Intersection(t2, s));
}

BoundingBox Sphere::get_bounds() const {
  return BoundingBox(point(-1, -1, -1), point(1, 1, 1));
}

Sphere glass_sphere() {
  Sphere sphere;
  sphere.set_transform(identity_matrix<4>());
  sphere.material.transparency = 1.0f;
  sphere.material.refractive_index = 1.5f;

  return sphere;
}
