#include <cmath>
#include <raytracer/helper_functions.hpp>
#include <raytracer/material.hpp>
#include <raytracer/pattern_funcs.hpp>

bool Material::operator==(const Material &other) const {
  return color == other.color && eql(ambient, other.ambient) &&
         eql(diffuse, other.diffuse) && eql(specular, other.specular) &&
         eql(shininess, other.shininess);
}

Color Material::lighting(Shape *shape, const Light &light, const Point &point,
                         const Vec &eyev, const Vec &normalv,
                         bool in_shadow) const {
  Color mcolor = color;
  if (pattern.has_value()) {
    mcolor = pattern_at_shape(*pattern, *shape, point);
  }
  Color effective_color = mcolor * light.intensity;
  Vec lightv = norm(light.position - point);
  Color ambient_color = effective_color * ambient;
  float light_dot_normal = dot(lightv, normalv);

  Color diffuse_color;
  Color specular_color;

  if (light_dot_normal >= 0.0f) {
    diffuse_color = effective_color * diffuse * light_dot_normal;
    Vec reflectv = reflect(-lightv, normalv);
    float reflect_dot_eye = dot(reflectv, eyev);
    if (reflect_dot_eye > 0.0f) {
      float factor = powf(reflect_dot_eye, shininess);
      specular_color = light.intensity * specular * factor;
    }
  }

  return ambient_color + (!in_shadow) * (diffuse_color + specular_color);
}
