#include <algorithm>
#include <fstream>
#include <optional>
#include <raytracer/obj_parser.hpp>
#include <raytracer/smooth_triangle.hpp>
#include <raytracer/string_operations.hpp>
#include <raytracer/triangle.hpp>
#include <raytracer/tuple.hpp>
#include <regex>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>

bool OBJParser::parse_from_file(const std::string &file_path) {
  std::ifstream obj_file(file_path);

  if (obj_file.is_open()) {
    std::stringstream buffer;
    buffer << obj_file.rdbuf();
    parse(buffer.str());
    return true;
  }

  return false;
}

Group OBJParser::to_group() const {
  Group obj_group;
  auto &obj_group_groups = obj_group.get<Group>();

  if (!default_group.empty()) obj_group_groups.push_back(default_group);

  for (auto const &key_group : groups) {
    auto &group = key_group.second;
    if (!group.empty()) obj_group_groups.push_back(group);
  }

  return obj_group;
}

std::map<std::string, Group> OBJParser::get_groups() { return groups; }

void OBJParser::parse(const std::string &file) {
  ignored_lines = 0;
  current_group = &default_group;

  vertices.clear();
  normals.clear();

  vertices.push_back(point(0, 0, 0));  // dummy vertex
  normals.push_back(point(0, 0, 0));   // dummy normal

  std::istringstream iss(file);

  for (std::string line; std::getline(iss, line);) {
    string_operations::remove_double_spaces(line);
    string_operations::remove_character(line, '\r');

    if (parse_group(line)) continue;
    if (parse_vertex(line)) continue;
    if (parse_normal(line)) continue;
    if (parse_extended_polygonal_face(line)) continue;
    if (parse_polygonal_face(line)) continue;

    ++ignored_lines;
  }

  if (!default_group.empty())
    groups.insert(std::make_pair("default", default_group));
}

bool OBJParser::parse_group(const std::string &line) {
  auto tokens = string_operations::split(line, " ");
  if (tokens.size() == 2 && tokens[0] == "g") {
    if (groups.find(tokens[1]) == groups.end())
      groups.insert(std::make_pair(tokens[1], Group()));
    current_group = &groups[tokens[1]];
    return true;
  }
  return false;
}

bool OBJParser::parse_vertex(const std::string &line) {
  float x, y, z;
  if (std::sscanf(line.c_str(), "v %f %f %f", &x, &y, &z) == 3) {
    vertices.push_back(point(x, y, z));
    return true;
  }
  return false;
}

bool OBJParser::parse_normal(const std::string &line) {
  float x, y, z;
  if (std::sscanf(line.c_str(), "vn %f %f %f", &x, &y, &z) == 3) {
    normals.push_back(vec(x, y, z));
    return true;
  }
  return false;
}

bool OBJParser::parse_polygonal_face(const std::string &line) {
  std::vector<int> vertex_idxs;

  auto tokens = string_operations::split(line, " ");

  if (tokens.size() >= 4 && tokens[0] == "f" &&
      std::find_if_not(
          tokens.begin() + 1, tokens.end(), [&](const auto &token) {
            int vertex_id;
            if (std::sscanf(token.c_str(), "%d", &vertex_id) == 1) {
              vertex_idxs.push_back(vertex_id);
              return true;
            }
            return false;
          }) == tokens.end()) {
    if (tokens.size() == 4) {
      current_group->add_child(Triangle(vertices[vertex_idxs[0]],
                                        vertices[vertex_idxs[1]],
                                        vertices[vertex_idxs[2]]));
    } else {
      fan_triangulation(vertex_idxs);
    }
    return true;
  }
  return false;
}

bool OBJParser::parse_extended_polygonal_face(const std::string &line) {
  std::vector<int> vertex_idxs;
  std::vector<int> texture_idxs;
  std::vector<int> normal_idxs;

  auto tokens = string_operations::split(line, " ");

  if (tokens.size() >= 4 && tokens[0] == "f" &&
      std::find_if_not(
          tokens.begin() + 1, tokens.end(), [&](const auto &token) {
            int vertex_id, texture_id, normal_id;
            if (std::sscanf(token.c_str(), "%d/%d/%d", &vertex_id, &texture_id,
                            &normal_id) == 3 ||
                std::sscanf(token.c_str(), "%d//%d", &vertex_id, &normal_id) ==
                    2) {
              vertex_idxs.push_back(vertex_id);
              texture_idxs.push_back(texture_id);
              normal_idxs.push_back(normal_id);
              return true;
            }
            return false;
          }) == tokens.end()) {
    if (tokens.size() == 4) {
      current_group->add_child(
          SmoothTriangle(vertices[vertex_idxs[0]], vertices[vertex_idxs[1]],
                         vertices[vertex_idxs[2]], normals[normal_idxs[0]],
                         normals[normal_idxs[1]], normals[normal_idxs[2]]));
    } else {
      fan_triangulation(vertex_idxs, texture_idxs, normal_idxs);
    }
    return true;
  }
  return false;
}

void OBJParser::fan_triangulation(const std::vector<int> &vertex_idxs) {
  for (size_t i = 1; i < vertex_idxs.size() - 1; ++i) {
    current_group->add_child(Triangle(vertices[vertex_idxs[0]],
                                      vertices[vertex_idxs[i]],
                                      vertices[vertex_idxs[i + 1]]));
  }
}

void OBJParser::fan_triangulation(const std::vector<int> &vertex_idxs,
                                  const std::vector<int> &texture_idxs,
                                  const std::vector<int> &normal_idxs) {
  for (size_t i = 1; i < vertex_idxs.size() - 1; ++i) {
    current_group->add_child(
        SmoothTriangle(vertices[vertex_idxs[0]], vertices[vertex_idxs[i]],
                       vertices[vertex_idxs[i + 1]], normals[normal_idxs[0]],
                       normals[normal_idxs[i]], normals[normal_idxs[i + 1]]));
  }
}
