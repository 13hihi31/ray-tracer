#include <cmath>
#include <iostream>
#include <limits>
#include <raytracer/constants.hpp>
#include <raytracer/plane.hpp>

Plane::Plane(const Matrix<4> &transform, const Material &material)
    : Shape(transform, material) {}

Vec Plane::local_normal_at(const Point &point,
                           const Intersection &intersection) const {
  return vec(0, 1, 0);
}

void Plane::local_intersect(const Ray &local_ray,
                            std::vector<Intersection> &intersections) {
  if (std::abs(local_ray.direction.y) < consts::epsilon) return;

  float t = -local_ray.origin.y / local_ray.direction.y;
  Shape *s = this;

  intersections.push_back(Intersection(t, s));
}

BoundingBox Plane::get_bounds() const {
  return BoundingBox(point(std::numeric_limits<float>::lowest(), 0,
                           std::numeric_limits<float>::lowest()),
                     point(std::numeric_limits<float>::max(), 0,
                           std::numeric_limits<float>::max()));
}
