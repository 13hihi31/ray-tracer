
#include <cmath>
#include <raytracer/pattern.hpp>

void TwoColorPattern::set_transform(const Matrix<4> &transform) {
  transform_inverse = transform.inverse();
}

Color StripePattern::pattern_at(const Point &point) const {
  Point pattern_point = mul(transform_inverse, point);
  if (int(std::floor(pattern_point.x)) % 2 == 0) {
    return color_a;
  }
  return color_b;
}

Color GradientPattern::pattern_at(const Point &point) const {
  Point pattern_point = mul(transform_inverse, point);
  Vec distance = color_b - color_a;
  float fraction = point.x - std::floor(pattern_point.x);
  return color_a + distance * fraction;
}

Color RingPattern::pattern_at(const Point &point) const {
  Point pattern_point = mul(transform_inverse, point);
  if (int(std::floor(sqrtf(pattern_point.x * pattern_point.x +
                           pattern_point.z * pattern_point.z))) %
          2 ==
      0) {
    return color_a;
  }
  return color_b;
}

Color CheckerPattern::pattern_at(const Point &point) const {
  Point pattern_point = mul(transform_inverse, point);
  if (int(std::floor(pattern_point.x) + std::floor(pattern_point.y) +
          std::floor(pattern_point.z)) %
          2 ==
      0) {
    return color_a;
  }
  return color_b;
}

Color TestPattern::pattern_at(const Point &point) const {
  return color(point.x, point.y, point.z);
}
