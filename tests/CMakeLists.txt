
FetchContent_Declare(
    googletest
    FETCHCONTENT_UPDATES_DISCONNECTED=ON
    GIT_REPOSITORY "https://github.com/google/googletest.git"
    GIT_TAG release-1.11.0)

set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

add_executable(test_tuple test_tuple.cpp)
target_compile_features(test_tuple PRIVATE cxx_std_17)
target_link_libraries(test_tuple PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_tuple)

add_executable(test_canvas test_canvas.cpp)
target_compile_features(test_canvas PRIVATE cxx_std_17)
target_link_libraries(test_canvas PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_canvas)

add_executable(test_matrix test_matrix.cpp)
target_compile_features(test_matrix PRIVATE cxx_std_17)
target_link_libraries(test_matrix PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_matrix)

add_executable(test_transform test_transform.cpp)
target_compile_features(test_transform PRIVATE cxx_std_17)
target_link_libraries(test_transform PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_transform)

add_executable(test_ray test_ray.cpp)
target_compile_features(test_ray PRIVATE cxx_std_17)
target_link_libraries(test_ray PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_ray)

add_executable(test_sphere test_sphere.cpp)
target_compile_features(test_sphere PRIVATE cxx_std_17)
target_link_libraries(test_sphere PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_sphere)

add_executable(test_intersection test_intersection.cpp)
target_compile_features(test_intersection PRIVATE cxx_std_17)
target_link_libraries(test_intersection PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_intersection)

add_executable(test_light test_light.cpp)
target_compile_features(test_light PRIVATE cxx_std_17)
target_link_libraries(test_light PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_light)

add_executable(test_material test_material.cpp)
target_compile_features(test_material PRIVATE cxx_std_17)
target_link_libraries(test_material PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_material)

add_executable(test_world test_world.cpp)
target_compile_features(test_world PRIVATE cxx_std_17)
target_link_libraries(test_world PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_world)

add_executable(test_camera test_camera.cpp)
target_compile_features(test_camera PRIVATE cxx_std_17)
target_link_libraries(test_camera PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_camera)

add_executable(test_shape test_shape.cpp)
target_compile_features(test_shape PRIVATE cxx_std_17)
target_link_libraries(test_shape PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_shape)

add_executable(test_plane test_plane.cpp)
target_compile_features(test_plane PRIVATE cxx_std_17)
target_link_libraries(test_plane PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_plane)

add_executable(test_pattern test_pattern.cpp)
target_compile_features(test_pattern PRIVATE cxx_std_17)
target_link_libraries(test_pattern PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_pattern)

add_executable(test_cube test_cube.cpp)
target_compile_features(test_cube PRIVATE cxx_std_17)
target_link_libraries(test_cube PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_cube)

add_executable(test_cylinder test_cylinder.cpp)
target_compile_features(test_cylinder PRIVATE cxx_std_17)
target_link_libraries(test_cylinder PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_cylinder)

add_executable(test_cone test_cone.cpp)
target_compile_features(test_cone PRIVATE cxx_std_17)
target_link_libraries(test_cone PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_cone)

add_executable(test_group test_group.cpp)
target_compile_features(test_group PRIVATE cxx_std_17)
target_link_libraries(test_group PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_group)

add_executable(test_triangle test_triangle.cpp)
target_compile_features(test_triangle PRIVATE cxx_std_17)
target_link_libraries(test_triangle PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_triangle)

add_executable(test_obj_parser test_obj_parser.cpp)
target_compile_features(test_obj_parser PRIVATE cxx_std_17)
target_link_libraries(test_obj_parser PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_obj_parser)

add_executable(test_smooth_triangle test_smooth_triangle.cpp)
target_compile_features(test_smooth_triangle PRIVATE cxx_std_17)
target_link_libraries(test_smooth_triangle PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_smooth_triangle)

add_executable(test_csg test_csg.cpp)
target_compile_features(test_csg PRIVATE cxx_std_17)
target_link_libraries(test_csg PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_csg)

add_executable(test_string_operations test_string_operations.cpp)
target_compile_features(test_string_operations PRIVATE cxx_std_17)
target_link_libraries(test_string_operations PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_string_operations)

add_executable(test_bounding_box test_bounding_box.cpp)
target_compile_features(test_bounding_box PRIVATE cxx_std_17)
target_link_libraries(test_bounding_box PRIVATE raytracer gtest gtest_main)
gtest_discover_tests(test_bounding_box)

