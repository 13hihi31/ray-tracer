#include <gtest/gtest.h>

#include <cmath>
#include <raytracer/helper_functions.hpp>
#include <raytracer/tuple.hpp>

TEST(Tuple, ATupleWithW1IsAPoint) {
  Tuple a = Tuple(4.3f, -4.2f, 3.1f, 1.0f);

  EXPECT_EQ(a.x, 4.3f);
  EXPECT_EQ(a.y, -4.2f);
  EXPECT_EQ(a.z, 3.1f);
  EXPECT_EQ(a.w, 1.0f);
  EXPECT_TRUE(pointp(a));
  EXPECT_FALSE(vecp(a));
}

TEST(Tuple, ATupleWithW0IsAVector) {
  Tuple a = Tuple(4.3f, -4.2f, 3.1f, 0.0f);

  EXPECT_EQ(a.x, 4.3f);
  EXPECT_EQ(a.y, -4.2f);
  EXPECT_EQ(a.z, 3.1f);
  EXPECT_EQ(a.w, 0.0f);
  EXPECT_FALSE(pointp(a));
  EXPECT_TRUE(vecp(a));
}

TEST(Tuple, CreateAPoint) {
  Tuple a = point(4, -4, 3);

  ASSERT_EQ(a, Tuple(4, -4, 3, 1));
}

TEST(Tuple, CreateAVector) {
  Tuple a = vec(4, -4, 3);

  ASSERT_EQ(a, Tuple(4, -4, 3, 0));
}

TEST(Tuple, AddingTwoTuples) {
  Tuple a1 = Tuple(3, -2, 5, 1);
  Tuple a2 = Tuple(-2, 3, 1, 0);

  EXPECT_EQ(a1 + a2, Tuple(1, 1, 6, 1));
}

TEST(Tuple, SubtractPoints) {
  Tuple p1 = point(3, 2, 1);
  Tuple p2 = point(5, 6, 7);

  EXPECT_EQ(p1 - p2, vec(-2, -4, -6));
}

TEST(Tuple, SubtractingAVectorFromAPoint) {
  Tuple p = point(3, 2, 1);
  Tuple v = vec(5, 6, 7);

  EXPECT_EQ(p - v, point(-2, -4, -6));
}

TEST(Tuple, SubtractingTwoVectors) {
  Tuple v1 = vec(3, 2, 1);
  Tuple v2 = vec(5, 6, 7);

  EXPECT_EQ(v1 - v2, vec(-2, -4, -6));
}

TEST(Tuple, NegatingATuple) {
  Tuple a = Tuple(1, -2, 3, -4);

  EXPECT_EQ(-a, Tuple(-1, 2, -3, 4));
}

TEST(Tuple, MultiplingATupleByAScalarFromTheRightSide) {
  Tuple a = Tuple(1, -2, 3, -4);

  EXPECT_EQ(a * 3.5, Tuple(3.5, -7.0, 10.5, -14.0));
}

TEST(Tuple, MultiplingATupleByAScalarFromTheLeftSide) {
  Tuple a = Tuple(1, -2, 3, -4);

  EXPECT_EQ(3.5 * a, Tuple(3.5, -7.0, 10.5, -14.0));
}

TEST(Tuple, DivideATupleByAScalar) {
  Tuple a = Tuple(1, -2, 3, -4);

  EXPECT_EQ(a / 2, Tuple(0.5, -1, 1.5, -2));
}

TEST(Tuple, ComputingTheMagnitudeOfVec123) {
  Tuple a = vec(1, 2, 3);

  EXPECT_TRUE(eql(mag(a), sqrt(14)));
}

TEST(Tuple, NormalizingAVector) {
  Tuple v = vec(4, 0, 0);

  EXPECT_TRUE(norm(v) == vec(1, 0, 0));
}

TEST(Tuple, TheMagnitudeOfANormalizedVector) {
  Tuple v = vec(1, 2, 3);
  Tuple n = norm(v);

  EXPECT_TRUE(eql(mag(n), 1.0f));
}

TEST(Tuple, DotProduct) {
  Tuple v1 = vec(1, 2, 3);
  Tuple v2 = vec(2, 3, 4);

  EXPECT_TRUE(eql(dot(v1, v2), 20));
}

TEST(Tuple, CrossProductOfTwoVectors) {
  Tuple v1 = vec(1, 2, 3);
  Tuple v2 = vec(2, 3, 4);

  EXPECT_EQ(cross(v1, v2), vec(-1, 2, -1));
  EXPECT_EQ(cross(v2, v1), vec(1, -2, 1));
}

TEST(Tuple, IndexingATuple) {
  Tuple v = vec(1, 2, 3);

  EXPECT_TRUE(eql(v.x, 1));
  EXPECT_TRUE(eql(v.y, 2));
  EXPECT_TRUE(eql(v.z, 3));
  EXPECT_TRUE(eql(v.w, 0));
}

TEST(Tuple, ColorsAreRedGreenBlueTuples) {
  Tuple c = color(-0.5, 0.4, 1.7);

  EXPECT_EQ(c.x, -0.5f);
  EXPECT_EQ(c.y, 0.4f);
  EXPECT_EQ(c.z, 1.7f);
}

TEST(Tuple, AddingColors) {
  Color c1 = color(0.9, 0.6, 0.75);
  Color c2 = color(0.7, 0.1, 0.25);

  EXPECT_EQ(c1 + c2, color(1.6, 0.7, 1.0));
}

TEST(Tuple, SubtractingColors) {
  Color c1 = color(0.9, 0.6, 0.75);
  Color c2 = color(0.7, 0.1, 0.25);

  EXPECT_EQ(c1 - c2, color(0.2, 0.5, 0.5));
}

TEST(Tuple, MultiplyingAColorByAScalar) {
  Color c = color(0.2, 0.3, 0.4);

  EXPECT_EQ(c * 2, color(0.4, 0.6, 0.8));
}

TEST(Tuple, MultiplyingColors) {
  Color c1 = color(1, 0.2, 0.4);
  Color c2 = color(0.9, 1, 0.1);

  EXPECT_EQ(c1 * c2, color(0.9, 0.2, 0.04));
}

TEST(Tuple, ReflectingAVectorApproachingAt45Deg) {
  Vec v = vec(1, -1, 0);
  Vec n = vec(0, 1, 0);
  Vec r = reflect(v, n);

  EXPECT_EQ(r, vec(1, 1, 0));
}

TEST(Tuple, ReflectingAVectorOffASlantedSurface) {
  Vec v = vec(0, -1, 0);
  Vec n = vec(sqrtf(2.0f) / 2.0f, sqrtf(2.0f) / 2.0f, 0);
  Vec r = reflect(v, n);

  EXPECT_EQ(r, vec(1, 0, 0));
}
