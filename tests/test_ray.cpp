#include <gtest/gtest.h>

#include <raytracer/matrix.hpp>
#include <raytracer/ray.hpp>
#include <raytracer/transforms.hpp>
#include <raytracer/tuple.hpp>

TEST(Ray, CreatingAndQueryingARay) {
  Point origin = point(1, 2, 3);
  Vec direction = vec(4, 5, 6);
  Ray ray = Ray(origin, direction);

  EXPECT_EQ(ray.origin, origin);
  EXPECT_EQ(ray.direction, direction);
}

TEST(Ray, ComputingAPointFromADistance) {
  Ray ray = Ray(point(2, 3, 4), vec(1, 0, 0));

  EXPECT_EQ(ray.position(0), point(2, 3, 4));
  EXPECT_EQ(ray.position(1), point(3, 3, 4));
  EXPECT_EQ(ray.position(-1), point(1, 3, 4));
  EXPECT_EQ(ray.position(2.5), point(4.5, 3, 4));
}

TEST(Ray, TranslatingARay) {
  Ray ray(point(1, 2, 3), vec(0, 1, 0));
  Matrix<4> matrix_transform = translation(3, 4, 5);
  Ray ray2 = ray.transform(matrix_transform);

  EXPECT_EQ(ray2.origin, point(4, 6, 8));
  EXPECT_EQ(ray2.direction, vec(0, 1, 0));
}

TEST(Ray, ScalingARay) {
  Ray ray(point(1, 2, 3), vec(0, 1, 0));
  Matrix<4> matrix_transform = scaling(2, 3, 4);
  Ray ray2 = ray.transform(matrix_transform);

  EXPECT_EQ(ray2.origin, point(2, 6, 12));
  EXPECT_EQ(ray2.direction, vec(0, 3, 0));
}
