#include <gtest/gtest.h>

#include <raytracer/helper_functions.hpp>
#include <raytracer/intersection_data.hpp>
#include <raytracer/ray.hpp>
#include <raytracer/smooth_triangle.hpp>

TEST(SmoothTriangle, ConstructingASmoothTriangle) {
  Point p1 = point(0, 1, 0);
  Point p2 = point(-1, 0, 0);
  Point p3 = point(1, 0, 0);
  Vec n1 = vec(0, 1, 0);
  Vec n2 = vec(-1, 0, 0);
  Vec n3 = vec(1, 0, 0);
  SmoothTriangle smooth_triangle(p1, p2, p3, n1, n2, n3);

  EXPECT_EQ(smooth_triangle.p1, p1);
  EXPECT_EQ(smooth_triangle.p2, p2);
  EXPECT_EQ(smooth_triangle.p3, p3);
  EXPECT_EQ(smooth_triangle.n1, n1);
  EXPECT_EQ(smooth_triangle.n2, n2);
  EXPECT_EQ(smooth_triangle.n3, n3);
}

TEST(SmoothTriangle, AnIntersectionWithASmoothTriangleStoresUAndV) {
  Ray ray(point(-0.2, 0.3, -2), vec(0, 0, 1));
  SmoothTriangle smooth_triangle(point(0, 1, 0), point(-1, 0, 0),
                                 point(1, 0, 0), vec(0, 1, 0), vec(-1, 0, 0),
                                 vec(1, 0, 0));

  std::vector<Intersection> intersections;
  smooth_triangle.intersect(ray, intersections);

  EXPECT_TRUE(eql(intersections[0].u, 0.45));
  EXPECT_TRUE(eql(intersections[0].v, 0.25));
}

TEST(SmoothTriangle, ASmoothTriangleUsesUAndVToInterpolateTheNormal) {
  SmoothTriangle smooth_triangle(point(0, 1, 0), point(-1, 0, 0),
                                 point(1, 0, 0), vec(0, 1, 0), vec(-1, 0, 0),
                                 vec(1, 0, 0));

  Intersection intersection(1, &smooth_triangle, 0.45, 0.25);
  Vec normal_vec =
      smooth_triangle.local_normal_at(point(0, 0, 0), intersection);

  EXPECT_EQ(normal_vec, vec(-0.2, 0.3, 0));
  // ASSERT_EQ(normal_vec, vec(-0.5547, 0.83205, 0));
}

TEST(SmoothTriangle, PreparingTheNormalOnASmoothTriangle) {
  SmoothTriangle smooth_triangle(point(0, 1, 0), point(-1, 0, 0),
                                 point(1, 0, 0), vec(0, 1, 0), vec(-1, 0, 0),
                                 vec(1, 0, 0));

  Intersection intersection(1, &smooth_triangle, 0.45, 0.25);
  Ray ray(point(-0.2, 0.3, -2), vec(0., 0., 1.));
  auto intersections = std::vector<Intersection>({intersection});
  IntersectionData intersection_data(intersection, ray, intersections);

  ASSERT_EQ(intersection_data.normal_vector, vec(-0.5547, 0.83205, 0));
}
