#include <gtest/gtest.h>

#include <cmath>
#include <raytracer/colors.hpp>
#include <raytracer/helper_functions.hpp>
#include <raytracer/light.hpp>
#include <raytracer/transforms.hpp>
#include <raytracer/world.hpp>
#include <vector>

TEST(World, CreatingAWorld) {
  World world;

  EXPECT_TRUE(world.spheres.empty());
}

TEST(World, TheDefaultWorld) {
  Light light(point(-10, 10, -10), color(1, 1, 1));

  Sphere sphere1;
  sphere1.material.color = color(0.8, 1.0, 0.6);
  sphere1.material.diffuse = 0.7f;
  sphere1.material.specular = 0.2f;

  Sphere sphere2;
  sphere2.set_transform(scaling(0.5, 0.5, 0.5));

  World world = default_world();

  EXPECT_EQ(world.light, light);
  EXPECT_EQ(world.spheres[0], sphere1);
  EXPECT_EQ(world.spheres[1], sphere2);
}

TEST(World, IntersectAWorldWithARay) {
  World world = default_world();

  Ray ray(point(0, 0, -5), vec(0, 0, 1));
  std::vector<Intersection> intersections;
  world.intersect(ray, intersections);

  EXPECT_EQ(intersections.size(), 4);
  EXPECT_TRUE(eql(intersections[0].t, 4.0f));
  EXPECT_TRUE(eql(intersections[1].t, 4.5f));
  EXPECT_TRUE(eql(intersections[2].t, 5.5f));
  EXPECT_TRUE(eql(intersections[3].t, 6.0f));
}

TEST(World, ShadingAnIntersection) {
  World world = default_world();
  Ray ray(point(0, 0, -5), vec(0, 0, 1));
  auto shape = world.spheres[0];
  auto intersection = Intersection(4, &shape);
  IntersectionData intersection_data(intersection, ray);
  std::vector<Intersection> intersections;

  EXPECT_EQ(world.shade_hit(intersection_data, intersections),
            color(0.38066, 0.47583, 0.2855));
}

TEST(World, ShadingAnIntersectionFromTheInside) {
  World world = default_world();
  world.light = Light(point(0, 0.25, 0), color(1, 1, 1));
  Ray ray(point(0, 0, 0), vec(0, 0, 1));
  auto shape = world.spheres[1];

  auto intersection = Intersection(0.5, &shape);
  IntersectionData intersection_data(intersection, ray);
  std::vector<Intersection> intersections;

  EXPECT_EQ(world.shade_hit(intersection_data, intersections),
            color(0.90498, 0.90498, 0.90498));
}

TEST(World, TheColorWhenTheRayMisses) {
  World world = default_world();
  Ray ray(point(0, 0, -5), vec(0, 1, 0));
  std::vector<Intersection> intersections;

  EXPECT_EQ(world.color_at(ray, intersections), color(0, 0, 0));
}

TEST(World, TheColorWhenARayHits) {
  World world = default_world();
  Ray ray(point(0, 0, -5), vec(0, 0, 1));
  std::vector<Intersection> intersections;

  EXPECT_EQ(world.color_at(ray, intersections),
            color(0.38066, 0.47583, 0.2855));
}

TEST(World, TheColorWithAnIntersectionBehindTheRay) {
  World world = default_world();

  auto outer = world.spheres[0];
  outer.material.ambient = 1.0f;
  auto inner = world.spheres[1];
  inner.material.ambient = 1.0f;

  Ray ray(point(0, 0, 0.75), vec(0, 0, -1));
  std::vector<Intersection> intersections;

  EXPECT_EQ(world.color_at(ray, intersections), 0.1 * inner.material.color);
  // EXPECT_EQ(world.color_at(ray, intersections), inner.material.color);
}

TEST(World, ThereIsNoShadowWhenNothingIsCollinearWithPointAndLight) {
  World world = default_world();
  Point p = point(0, 10, 0);
  std::vector<Intersection> intersections;

  EXPECT_FALSE(world.is_shadowed(p, intersections));
}

TEST(World, TheShadowWhenAnObjectIsBetweenThePointAndTheLight) {
  World world = default_world();
  Point p = point(10, -10, 10);
  std::vector<Intersection> intersections;

  EXPECT_TRUE(world.is_shadowed(p, intersections));
}

TEST(World, ThereIsNoShadowWhenAnObjectIsBehindTheLight) {
  World world = default_world();
  Point p = point(-20, 20, -20);
  std::vector<Intersection> intersections;

  EXPECT_FALSE(world.is_shadowed(p, intersections));
}

TEST(World, ThereIsNoShadowWhenAnObjectIsBehindThePoint) {
  World world = default_world();
  Point p = point(-2, 2, -2);
  std::vector<Intersection> intersections;

  EXPECT_FALSE(world.is_shadowed(p, intersections));
}

TEST(World, ShadeHitIsGivenAnIntersectionInShadow) {
  Sphere sphere1;

  Sphere sphere2;
  sphere2.set_transform(translation(0, 0, 10));

  World world(Light(point(0, 0, -10), color(1, 1, 1)));
  world.spheres = {sphere1, sphere2};

  Ray ray(point(0, 0, 5), vec(0, 0, 1));
  Intersection intersection(4, &sphere2);
  std::vector<Intersection> intersections;
  IntersectionData intersection_data(intersection, ray, intersections);

  EXPECT_EQ(world.shade_hit(intersection_data, intersections),
            color(0.1, 0.1, 0.1));
}

TEST(World, TheReflectedColorForANonreflectiveMaterial) {
  World world = default_world();
  Ray ray(point(0, 0, 0), vec(0, 0, 1));

  Sphere sphere = world.spheres[1];
  sphere.material.ambient = 1;

  Intersection intersection(1, &sphere);
  std::vector<Intersection> intersections;
  IntersectionData intersection_data(intersection, ray, intersections);

  EXPECT_EQ(world.reflected_color(intersection_data, intersections),
            color(0, 0, 0));
}

TEST(World, TheReflectedColorForAReflectiveMaterial) {
  World world = default_world();

  Plane plane;
  plane.material.reflective = 0.5f;
  plane.set_transform(translation(0, -1, 0));
  world.planes = {plane};

  Ray ray(point(0, 0, -3), vec(0, -sqrtf(2.0f) / 2.0f, sqrtf(2.0f) / 2.0f));
  Intersection intersection(sqrtf(2.0f), &plane);
  std::vector<Intersection> intersections;
  IntersectionData intersection_data(intersection, ray, intersections);

  EXPECT_EQ(world.reflected_color(intersection_data, intersections),
            color(0.190503, 0.238129, 0.142877));
}

TEST(World, ShadeHitWithAReflectiveMaterial) {
  World world = default_world();

  Plane plane;
  plane.material.reflective = 0.5f;
  plane.set_transform(translation(0, -1, 0));
  world.planes = {plane};

  Ray ray(point(0, 0, -3), vec(0, -sqrtf(2.0f) / 2.0f, sqrtf(2.0f) / 2.0f));
  Intersection intersection(sqrtf(2.0f), &plane);
  std::vector<Intersection> intersections;
  IntersectionData intersection_data(intersection, ray, intersections);

  EXPECT_EQ(world.shade_hit(intersection_data, intersections),
            color(0.876929, 0.924554, 0.829303));
}

TEST(World, TheReflectedColorAtTheMaximumRecursiveDepth) {
  World world = default_world();

  Plane plane;
  plane.material.reflective = 0.5f;
  plane.set_transform(translation(0, -1, 0));
  world.planes = {plane};

  Ray ray(point(0, 0, -3), vec(0, -sqrtf(2.0f) / 2.0f, sqrtf(2.0f) / 2.0f));
  Intersection intersection(sqrtf(2.0f), &plane);
  std::vector<Intersection> intersections;
  IntersectionData intersection_data(intersection, ray, intersections);

  EXPECT_EQ(world.reflected_color(intersection_data, intersections, 0),
            colors::black);
}

TEST(World, TheRefractedColorWithAnOpaqueSurface) {
  World world = default_world();

  Sphere sphere = world.spheres[0];

  Ray ray(point(0, 0, -5), vec(0, 0, 1));
  std::vector<Intersection> intersections(
      {Intersection(4, &sphere), Intersection(6, &sphere)});
  IntersectionData intersection_data(intersections[0], ray, intersections);

  EXPECT_EQ(world.refracted_color(intersection_data, intersections, 5),
            color(0, 0, 0));
}

TEST(World, TheRefractedColorAtTheMaximumRecursiveDepth) {
  World world = default_world();

  Sphere sphere = world.spheres[0];
  sphere.material.transparency = 1.0f;
  sphere.material.refractive_index = 1.5f;

  Ray ray(point(0, 0, -5), vec(0, 0, 1));
  std::vector<Intersection> intersections(
      {Intersection(4, &sphere), Intersection(6, &sphere)});
  IntersectionData intersection_data(intersections[0], ray, intersections);

  EXPECT_EQ(world.refracted_color(intersection_data, intersections, 0),
            color(0, 0, 0));
}

TEST(World, TheRefractedColorUnderTotalInternalReflection) {
  World world = default_world();

  Sphere sphere = world.spheres[0];
  sphere.material.transparency = 1.0f;
  sphere.material.refractive_index = 1.5f;

  Ray ray(point(0, 0, sqrtf(2.0f) / 2.0f), vec(0, 1, 0));
  std::vector<Intersection> intersections(
      {Intersection(-sqrtf(2.0f) / 2.0f, &sphere),
       Intersection(sqrtf(2.0f) / 2.0f, &sphere)});
  IntersectionData intersection_data(intersections[1], ray, intersections);

  EXPECT_EQ(world.refracted_color(intersection_data, intersections, 5),
            color(0, 0, 0));
}

TEST(World, TheRefractedColorWithARefractedRay) {
  World world = default_world();

  Sphere sphere1 = world.spheres[0];
  sphere1.material.ambient = 1.0f;
  sphere1.material.pattern = TestPattern();

  Sphere sphere2 = world.spheres[0];
  sphere2.material.transparency = 1.0f;
  sphere2.material.refractive_index = 1.5f;

  Ray ray(point(0, 0, 0.1), vec(0, 1, 0));
  std::vector<Intersection> intersections(
      {Intersection(-0.9899, &sphere1), Intersection(-0.4899, &sphere2),
       Intersection(0.4899, &sphere2), Intersection(0.9899, &sphere1)});
  IntersectionData intersection_data(intersections[2], ray, intersections);

  EXPECT_EQ(world.refracted_color(intersection_data, intersections, 5),
            color(0.08, 0.1, 0.06));
}

TEST(World, ShadeHitWithATransparentMaterial) {
  World world = default_world();

  Plane floor;
  floor.set_transform(translation(0, -1, 0));
  floor.material.transparency = 0.5f;
  floor.material.refractive_index = 1.5f;
  world.planes.push_back(floor);

  Sphere ball;
  ball.set_transform(translation(0, -3.5, -0.5));
  ball.material.color = color(1, 0, 0);
  ball.material.ambient = 0.5f;
  world.spheres.push_back(ball);

  Ray ray(point(0, 0, -3), vec(0, -sqrtf(2.0f) / 2.0f, sqrtf(2.0f) / 2.0f));
  std::vector<Intersection> intersections({Intersection(sqrtf(2.0f), &floor)});
  IntersectionData intersection_data(intersections[0], ray, intersections);

  EXPECT_EQ(world.shade_hit(intersection_data, intersections, 5),
            color(0.93642, 0.68642, 0.68642));
}

TEST(World, ShadeHitWithAReflectiveTransparentMaterial) {
  World world = default_world();
  Ray ray(point(0, 0, -3), vec(0, -sqrtf(2.0f) / 2.0f, sqrtf(2.0f) / 2.0f));

  Plane floor;
  floor.set_transform(translation(0, -1, 0));
  floor.material.reflective = 0.5f;
  floor.material.transparency = 0.5f;
  floor.material.refractive_index = 1.5f;
  world.planes.push_back(floor);

  Sphere ball;
  ball.material.color = color(1, 0, 0);
  ball.material.ambient = 0.5f;
  ball.set_transform(translation(0, -3.5, -0.5));
  world.spheres.push_back(ball);

  std::vector<Intersection> intersections({Intersection(sqrtf(2.0f), &floor)});
  IntersectionData intersection_data(intersections[0], ray, intersections);

  EXPECT_EQ(world.shade_hit(intersection_data, intersections, 5),
            color(0.933922, 0.696443, 0.692436));
}
