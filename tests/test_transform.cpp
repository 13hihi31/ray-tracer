#include <gtest/gtest.h>

#include <cmath>
#include <iostream>
#include <raytracer/matrix.hpp>
#include <raytracer/transforms.hpp>
#include <raytracer/tuple.hpp>

TEST(Translation, MultiplyingByATranslationMatrix) {
  Matrix<4> transform = translation(5, -3, 2);
  Point p = point(-3, 4, 5);

  ASSERT_EQ(mul(transform, p), point(2, 1, 7));
}

TEST(Translation, MultiplyingByTheInverseOfATranslationMatrix) {
  Matrix<4> transform = translation(5, -3, 2);
  Matrix<4> inv = transform.inverse();
  Point p = point(-3, 4, 5);

  ASSERT_EQ(mul(inv, p), point(-8, 7, 3));
}

TEST(Translation, TranslationDoesNotAffectVectors) {
  Matrix<4> transform = translation(5, -3, 2);
  Vec v = vec(-3, 4, 5);

  ASSERT_EQ(mul(transform, v), v);
}

TEST(Scaling, AScalingMatrixAppliedToAPoint) {
  Matrix<4> transform = scaling(2, 3, 4);
  Point p = point(-4, 6, 8);

  ASSERT_EQ(mul(transform, p), point(-8, 18, 32));
}

TEST(Scaling, AScalingMArixAppliedToAVector) {
  Matrix<4> transform = scaling(2, 3, 4);
  Vec v = vec(-4, 6, 8);

  ASSERT_EQ(mul(transform, v), vec(-8, 18, 32));
}

TEST(Scaling, MultiplyingByTheInverseOfAScalingMatrix) {
  Matrix<4> transform = scaling(2, 3, 4);
  Matrix<4> inv = transform.inverse();
  Vec v = vec(-4, 6, 8);

  ASSERT_EQ(mul(inv, v), vec(-2, 2, 2));
}

TEST(Scaling, ReflectionIsScalingByANegativeValue) {
  Matrix<4> transform = scaling(-1, 1, 1);
  Point p = point(2, 3, 4);

  ASSERT_EQ(mul(transform, p), point(-2, 3, 4));
}

TEST(Rotation, RotatingAPointAroundTheXAxis) {
  Point p = point(0, 1, 0);
  Matrix<4> half_quarter_rotation = rotation_x(M_PI / 4.0f);
  Matrix<4> full_quarter_rotation = rotation_x(M_PI / 2.0f);

  EXPECT_EQ(mul(half_quarter_rotation, p),
            point(0, sqrt(2.0f) / 2.0f, sqrt(2.0f) / 2.0f));
  EXPECT_EQ(mul(full_quarter_rotation, p), point(0, 0, 1));
}

TEST(Rotation, TheInverseOfAnXRotatioRotatesInTheOppositeDirection) {
  Point p = point(0, 1, 0);
  Matrix<4> half_quarter_rotation = rotation_x(M_PI / 4.0f);
  Matrix<4> inv = half_quarter_rotation.inverse();

  ASSERT_EQ(mul(inv, p), point(0, sqrtf(2.0f) / 2.0f, -sqrtf(2.0f) / 2.0f));
}

TEST(Rotation, RotatingAPointAroundTheYAxis) {
  Point p = point(0, 0, 1);
  Matrix<4> half_quarter_rotation = rotation_y(M_PI / 4.0f);
  Matrix<4> full_quarter_rotation = rotation_y(M_PI / 2.0f);

  EXPECT_EQ(mul(half_quarter_rotation, p),
            point(sqrtf(2.0f) / 2.0f, 0.0f, sqrtf(2.0f) / 2.0f));
  EXPECT_EQ(mul(full_quarter_rotation, p), point(1.0f, 0.0f, 0.0f));
}

TEST(Rotation, RotatingAPointAroundTheZAxis) {
  Point p = point(0, 1, 0);
  Matrix<4> half_quarter_rotation = rotation_z(M_PI / 4.0f);
  Matrix<4> full_quarter_rotation = rotation_z(M_PI / 2.0f);

  EXPECT_EQ(mul(half_quarter_rotation, p),
            point(-sqrtf(2.0f) / 2.0f, sqrtf(2.0f) / 2.0f, 0.0f));
  EXPECT_EQ(mul(full_quarter_rotation, p), point(-1, 0, 0));
}

TEST(Shearing, AShearingTransformationMovesXInProportionToY) {
  Matrix<4> transform = shearing(1, 0, 0, 0, 0, 0);
  Point p = point(2, 3, 4);

  ASSERT_EQ(mul(transform, p), point(5, 3, 4));
}

TEST(Shearing, AShearingTransformationMovesXInProportionToZ) {
  Matrix<4> transform = shearing(0, 1, 0, 0, 0, 0);
  Point p = point(2, 3, 4);
  ASSERT_TRUE(mul(transform, p) == point(6, 3, 4));
}

TEST(Shearing, AShearingTransformationMovesYInProportionToX) {
  Matrix<4> transform = shearing(0, 0, 1, 0, 0, 0);
  Point p = point(2, 3, 4);

  ASSERT_EQ(mul(transform, p), point(2, 5, 4));
}

TEST(Shearing, AShearingTransformationMovesYInProportionToZ) {
  Matrix<4> transform = shearing(0, 0, 0, 1, 0, 0);
  Point p = point(2, 3, 4);

  ASSERT_EQ(mul(transform, p), point(2, 7, 4));
}

TEST(Shearing, AShearingTransformationMovesZInProportionToX) {
  Matrix<4> transform = shearing(0, 0, 0, 0, 1, 0);
  Point p = point(2, 3, 4);

  ASSERT_EQ(mul(transform, p), point(2, 3, 6));
}

TEST(Shearing, AShearingTransformationMovesZInProportionToY) {
  Matrix<4> transform = shearing(0, 0, 0, 0, 0, 1);
  Point p = point(2, 3, 4);

  ASSERT_EQ(mul(transform, p), point(2, 3, 7));
}

TEST(ViewTransform, TheTransformationMatrixForTheDefaultOrientation) {
  Point from = point(0, 0, 0);
  Point to = point(0, 0, -1);
  Vec up = vec(0, 1, 0);
  Matrix<4> view = view_transform(from, to, up);

  ASSERT_EQ(view, identity_matrix<4>());
}

TEST(ViewTransform, AViewTransformationMatrixLookingInPositiveZDirection) {
  Point from = point(0, 0, 0);
  Point to = point(0, 0, 1);
  Vec up = vec(0, 1, 0);
  Matrix<4> view = view_transform(from, to, up);

  ASSERT_EQ(view, scaling(-1, 1, -1));
}

TEST(ViewTransform, AViewTransformationMovesTheWorld) {
  Point from = point(0, 0, 8);
  Point to = point(0, 0, 0);
  Vec up = vec(0, 1, 0);
  Matrix<4> view = view_transform(from, to, up);

  ASSERT_EQ(view, translation(0, 0, -8));
}

TEST(ViewTransform, AnArbitraryViewTransformation) {
  Point from = point(1, 3, 2);
  Point to = point(4, -2, 8);
  Vec up = vec(1, 1, 0);
  Matrix<4> view = view_transform(from, to, up);

  ASSERT_EQ(view, Matrix<4>({{-0.50709, 0.50709, 0.67612, -2.36643},
                             {0.76772, 0.60609, 0.12122, -2.82843},
                             {-0.35857, 0.59761, -0.71714, 0.00000},
                             {0.00000, 0.00000, 0.00000, 1.00000}}));
}
