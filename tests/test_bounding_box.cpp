#include <gtest/gtest.h>

#include <limits>
#include <raytracer/bounding_box.hpp>
#include <raytracer/transforms.hpp>
#include <vector>

TEST(BoundingBox, CreatingAnEmptyBoundingBox) {
  BoundingBox default_bounding_box;

  EXPECT_EQ(default_bounding_box.min_point,
            point(std::numeric_limits<float>::max(),
                  std::numeric_limits<float>::max(),
                  std::numeric_limits<float>::max()));

  EXPECT_EQ(default_bounding_box.max_point,
            point(std::numeric_limits<float>::lowest(),
                  std::numeric_limits<float>::lowest(),
                  std::numeric_limits<float>::lowest()));
}

TEST(BoundingBox, CreatingABoundingBoxWithVolume) {
  BoundingBox bounding_box{point(-1, -2, -3), point(3, 2, 1)};

  EXPECT_EQ(bounding_box.min_point, point(-1, -2, -3));
  EXPECT_EQ(bounding_box.max_point, point(3, 2, 1));
}

TEST(BoundingBox, AddingPointsToAnEmptyBoundingBox) {
  BoundingBox bounding_box;
  bounding_box.add_point(point(-5, 2, 0));
  bounding_box.add_point(point(7, 0, -3));

  EXPECT_EQ(bounding_box.min_point, point(-5, 0, -3));
  EXPECT_EQ(bounding_box.max_point, point(7, 2, 0));
}

TEST(BoundingBox, AddingOneBoundingBoxToAnother) {
  BoundingBox box_1(point(-5, -2, 0), point(7, 4, 4));
  BoundingBox box_2(point(8, -7, -2), point(14, 2, 8));
  box_1.add_box(box_2);

  EXPECT_EQ(box_1.min_point, point(-5, -7, -2));
  EXPECT_EQ(box_1.max_point, point(14, 4, 8));
}

TEST(BoundingBox, CheckingToSeeIfABoxContainsAGivenPoint) {
  BoundingBox box(point(5, -2, 0), point(11, 4, 7));

  EXPECT_TRUE(box.contains(point(5, -2, 0)));
  EXPECT_TRUE(box.contains(point(11, 4, 7)));
  EXPECT_TRUE(box.contains(point(8, 1, 3)));
  EXPECT_FALSE(box.contains(point(3, 0, 3)));
  EXPECT_FALSE(box.contains(point(8, -4, 3)));
  EXPECT_FALSE(box.contains(point(8, 1, -1)));
  EXPECT_FALSE(box.contains(point(13, 1, 3)));
  EXPECT_FALSE(box.contains(point(8, 5, 3)));
  EXPECT_FALSE(box.contains(point(8, 1, 8)));
}

TEST(BoundingBox, CheckingToSeeIfABoxContainsAGivenBox) {
  BoundingBox box(point(5, -2, 0), point(11, 4, 7));

  EXPECT_TRUE(box.contains(BoundingBox(point(5, -2, 0), point(11, 4, 7))));
  EXPECT_TRUE(box.contains(BoundingBox(point(6, -1, 1), point(10, 3, 6))));
  EXPECT_FALSE(box.contains(BoundingBox(point(4, -3, -1), point(10, 3, 6))));
  EXPECT_FALSE(box.contains(BoundingBox(point(6, -1, 1), point(12, 5, 8))));
}

TEST(BoundingBox, TransformingABoundingBox) {
  BoundingBox box_1(point(-1, -1, -1), point(1, 1, 1));
  Matrix<4> box_2_transform = mul(rotation_x(M_PI / 4), rotation_y(M_PI / 4));
  BoundingBox box_2 = box_1.transform(box_2_transform);

  EXPECT_EQ(box_2.min_point, point(-1.41421, -1.70711, -1.70711));
  EXPECT_EQ(box_2.max_point, point(1.41421, 1.70711, 1.70711));
}

TEST(BoundingBox, IntersectingARayWithABoundingBoxAtTheOrigin) {
  BoundingBox box(point(-1, -1, -1), point(1, 1, 1));

  EXPECT_EQ(box.intersects(Ray(point(5, 0.5, 0), norm(vec(-1, 0, 0)))), true);
  EXPECT_EQ(box.intersects(Ray(point(-5, 0.5, 0), norm(vec(1, 0, 0)))), true);
  EXPECT_EQ(box.intersects(Ray(point(0.5, 5, 0), norm(vec(0, -1, 0)))), true);
  EXPECT_EQ(box.intersects(Ray(point(0.5, -5, 0), norm(vec(0, 1, 0)))), true);
  EXPECT_EQ(box.intersects(Ray(point(0.5, 0, 5), norm(vec(0, 0, -1)))), true);
  EXPECT_EQ(box.intersects(Ray(point(0.5, 0, -5), norm(vec(0, 0, 1)))), true);
  EXPECT_EQ(box.intersects(Ray(point(0, 0.5, 0), norm(vec(0, 0, 1)))), true);

  EXPECT_EQ(box.intersects(Ray(point(-2, 0, 0), norm(vec(2, 4, 6)))), false);
  EXPECT_EQ(box.intersects(Ray(point(0, -2, 0), norm(vec(6, 2, 4)))), false);
  EXPECT_EQ(box.intersects(Ray(point(0, 0, -2), norm(vec(4, 6, 2)))), false);
  EXPECT_EQ(box.intersects(Ray(point(2, 0, 2), norm(vec(0, 0, -1)))), false);
  EXPECT_EQ(box.intersects(Ray(point(0, 2, 2), norm(vec(0, -1, 0)))), false);
  EXPECT_EQ(box.intersects(Ray(point(2, 2, 0), norm(vec(-1, 0, 0)))), false);
}

TEST(BoundingBox, IntersectingARayWithANonCubicBoundingBox) {
  BoundingBox box(point(5, -2, 0), point(11, 4, 7));

  EXPECT_EQ(box.intersects(Ray(point(15, 1, 2), norm(vec(-1, 0, 0)))), true);
  EXPECT_EQ(box.intersects(Ray(point(-5, -1, 4), norm(vec(1, 0, 0)))), true);
  EXPECT_EQ(box.intersects(Ray(point(7, 6, 5), norm(vec(0, -1, 0)))), true);
  EXPECT_EQ(box.intersects(Ray(point(9, -5, 6), norm(vec(0, 1, 0)))), true);
  EXPECT_EQ(box.intersects(Ray(point(8, 2, 12), norm(vec(0, 0, -1)))), true);
  EXPECT_EQ(box.intersects(Ray(point(6, 0, -5), norm(vec(0, 0, 1)))), true);
  EXPECT_EQ(box.intersects(Ray(point(8, 1, 3.5), norm(vec(0, 0, 1)))), true);

  EXPECT_EQ(box.intersects(Ray(point(9, -1, -8), norm(vec(2, 4, 6)))), false);
  EXPECT_EQ(box.intersects(Ray(point(8, 3, -4), norm(vec(6, 2, 4)))), false);
  EXPECT_EQ(box.intersects(Ray(point(9, -1, -2), norm(vec(4, 6, 2)))), false);
  EXPECT_EQ(box.intersects(Ray(point(4, 0, 9), norm(vec(0, 0, -1)))), false);
  EXPECT_EQ(box.intersects(Ray(point(8, 6, -1), norm(vec(0, -1, 0)))), false);
  EXPECT_EQ(box.intersects(Ray(point(12, 5, 4), norm(vec(-1, 0, 0)))), false);
}

TEST(BoundingBox, SplittingAPerfectCube) {
  BoundingBox box(point(-1, -4, -5), point(9, 6, 5));
  auto [left_box, right_box] = box.split();

  EXPECT_EQ(left_box.min_point, point(-1, -4, -5));
  EXPECT_EQ(left_box.max_point, point(4, 6, 5));
  EXPECT_EQ(right_box.min_point, point(4, -4, -5));
  EXPECT_EQ(right_box.max_point, point(9, 6, 5));
}

TEST(BoundingBox, SplittingAnXWideBox) {
  BoundingBox box(point(-1, -2, -3), point(9, 5.5, 3));
  auto [left_box, right_box] = box.split();

  EXPECT_EQ(left_box.min_point, point(-1, -2, -3));
  EXPECT_EQ(left_box.max_point, point(4, 5.5, 3));
  EXPECT_EQ(right_box.min_point, point(4, -2, -3));
  EXPECT_EQ(right_box.max_point, point(9, 5.5, 3));
}

TEST(BoundingBox, SplittingAnYWideBox) {
  BoundingBox box(point(-1, -2, -3), point(5, 8, 3));
  auto [left_box, right_box] = box.split();

  EXPECT_EQ(left_box.min_point, point(-1, -2, -3));
  EXPECT_EQ(left_box.max_point, point(5, 3, 3));
  EXPECT_EQ(right_box.min_point, point(-1, 3, -3));
  EXPECT_EQ(right_box.max_point, point(5, 8, 3));
}

TEST(BoundingBox, SplittingAnZWideBox) {
  BoundingBox box(point(-1, -2, -3), point(5, 3, 7));
  auto [left_box, right_box] = box.split();

  EXPECT_EQ(left_box.min_point, point(-1, -2, -3));
  EXPECT_EQ(left_box.max_point, point(5, 3, 2));
  EXPECT_EQ(right_box.min_point, point(-1, -2, 2));
  EXPECT_EQ(right_box.max_point, point(5, 3, 7));
}
