#include <gtest/gtest.h>

#include <raytracer/colors.hpp>
#include <raytracer/pattern.hpp>
#include <raytracer/pattern_funcs.hpp>
#include <raytracer/sphere.hpp>
#include <raytracer/transforms.hpp>
#include <raytracer/tuple.hpp>

TEST(StripePattern, CreatingAStripePattern) {
  StripePattern stripe_pattern(colors::white, colors::black);

  EXPECT_EQ(stripe_pattern.color_a, colors::white);
  EXPECT_EQ(stripe_pattern.color_b, colors::black);
}

TEST(StripePattern, AStripePatternIsConstantInY) {
  StripePattern stripe_pattern(colors::white, colors::black);

  EXPECT_EQ(stripe_pattern.pattern_at(point(0, 0, 0)), colors::white);
  EXPECT_EQ(stripe_pattern.pattern_at(point(0, 1, 0)), colors::white);
  EXPECT_EQ(stripe_pattern.pattern_at(point(0, 2, 0)), colors::white);
}

TEST(StripePattern, AStripePAtternIsConstantInZ) {
  StripePattern stripe_pattern(colors::white, colors::black);

  EXPECT_EQ(stripe_pattern.pattern_at(point(0, 0, 0)), colors::white);
  EXPECT_EQ(stripe_pattern.pattern_at(point(0, 0, 1)), colors::white);
  EXPECT_EQ(stripe_pattern.pattern_at(point(0, 0, 2)), colors::white);
}

TEST(StripePattern, AStripePAtternAlternatesInX) {
  StripePattern stripe_pattern(colors::white, colors::black);

  EXPECT_EQ(stripe_pattern.pattern_at(point(0, 0, 0)), colors::white);
  EXPECT_EQ(stripe_pattern.pattern_at(point(0.9, 0, 0)), colors::white);
  EXPECT_EQ(stripe_pattern.pattern_at(point(1, 0, 0)), colors::black);
  EXPECT_EQ(stripe_pattern.pattern_at(point(-0.1, 0, 0)), colors::black);
  EXPECT_EQ(stripe_pattern.pattern_at(point(-1, 0, 0)), colors::black);
  EXPECT_EQ(stripe_pattern.pattern_at(point(-1.1, 0, 0)), colors::white);
}

TEST(StripePattern, StripesWithAnObjectTransformation) {
  Sphere sphere;
  sphere.set_transform(scaling(2, 2, 2));
  StripePattern stripe_pattern(colors::white, colors::black);

  EXPECT_EQ(pattern_at_shape(stripe_pattern, sphere, point(1.5, 0, 0)),
            colors::white);
}

TEST(StripePattern, StripesWithAPatternTransformation) {
  Sphere sphere;
  StripePattern stripe_pattern(colors::white, colors::black);
  stripe_pattern.set_transform(scaling(2, 2, 2));

  EXPECT_EQ(pattern_at_shape(stripe_pattern, sphere, point(1.5, 0, 0)),
            colors::white);
}

TEST(StripePattern, StripesWithBothAnObjectAndAPatternTransformation) {
  Sphere sphere;
  sphere.set_transform(scaling(2, 2, 2));
  StripePattern stripe_pattern(colors::white, colors::black);
  stripe_pattern.set_transform(translation(0.5, 0, 0));

  EXPECT_EQ(pattern_at_shape(stripe_pattern, sphere, point(2.5, 0, 0)),
            colors::white);
}

TEST(GradientPattern, AGradientLinearlyInterpolatesBetweenColors) {
  GradientPattern gradient_pattern(colors::white, colors::black);

  EXPECT_EQ(gradient_pattern.pattern_at(point(0, 0, 0)), colors::white);
  EXPECT_EQ(gradient_pattern.pattern_at(point(0.25, 0, 0)),
            color(0.75, 0.75, 0.75));
  EXPECT_EQ(gradient_pattern.pattern_at(point(0.5, 0, 0)),
            color(0.5, 0.5, 0.5));
  EXPECT_EQ(gradient_pattern.pattern_at(point(0.75, 0, 0)),
            color(0.25, 0.25, 0.25));
}

TEST(RingPattern, ARingShouldExtendInBothXAndZ) {
  RingPattern ring_pattern(colors::white, colors::black);

  EXPECT_EQ(ring_pattern.pattern_at(point(0, 0, 0)), colors::white);
  EXPECT_EQ(ring_pattern.pattern_at(point(1, 0, 0)), colors::black);
  EXPECT_EQ(ring_pattern.pattern_at(point(0, 0, 1)), colors::black);
  EXPECT_EQ(ring_pattern.pattern_at(point(0.709, 0, 0.709)), colors::black);
}

TEST(CheckerPattern, CheckersShouldRepeatInX) {
  CheckerPattern checker_pattern(colors::white, colors::black);

  EXPECT_EQ(checker_pattern.pattern_at(point(0, 0, 0)), colors::white);
  EXPECT_EQ(checker_pattern.pattern_at(point(0.99, 0, 0)), colors::white);
  EXPECT_EQ(checker_pattern.pattern_at(point(1.01, 0, 0)), colors::black);
}

TEST(CheckerPattern, CheckersShouldRepeatInY) {
  CheckerPattern checker_pattern(colors::white, colors::black);

  EXPECT_EQ(checker_pattern.pattern_at(point(0, 0, 0)), colors::white);
  EXPECT_EQ(checker_pattern.pattern_at(point(0, 0.99, 0)), colors::white);
  EXPECT_EQ(checker_pattern.pattern_at(point(0, 1.01, 0)), colors::black);
}

TEST(CheckerPattern, CheckersShouldRepeatInZ) {
  CheckerPattern checker_pattern(colors::white, colors::black);

  EXPECT_EQ(checker_pattern.pattern_at(point(0, 0, 0)), colors::white);
  EXPECT_EQ(checker_pattern.pattern_at(point(0, 0, 0.99)), colors::white);
  EXPECT_EQ(checker_pattern.pattern_at(point(0, 0, 1.01)), colors::black);
}
