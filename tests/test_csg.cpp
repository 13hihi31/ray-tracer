#include <gtest/gtest.h>

#include <raytracer/csg.hpp>
#include <raytracer/cube.hpp>
#include <raytracer/group.hpp>
#include <raytracer/helper_functions.hpp>
#include <raytracer/intersection.hpp>
#include <raytracer/sphere.hpp>
#include <raytracer/transforms.hpp>
#include <vector>

TEST(CSG, CSGIsCreatedWithAnOperationAndTwoShapes) {
  Sphere sphere;
  Cube cube;

  CSG sphere_cube_csg(CSG::Operation::Union, &sphere, &cube);

  ASSERT_EQ(sphere_cube_csg.operation, CSG::Operation::Union);
  ASSERT_EQ(sphere_cube_csg.left, &sphere);
  ASSERT_EQ(sphere_cube_csg.right, &cube);
  ASSERT_EQ(sphere.parent, &sphere_cube_csg);
  ASSERT_EQ(cube.parent, &sphere_cube_csg);
}

TEST(CSG, EvaluatingTheRuleForACSGOperation) {
  CSG csg;

  std::vector<bool> left_hit(
      {true, true, true, true, false, false, false, false});
  std::vector<bool> inside_left(
      {true, true, false, false, true, true, false, false});
  std::vector<bool> inside_right(
      {true, false, true, false, true, false, true, false});

  std::vector<bool> union_results(
      {false, true, false, true, false, false, true, true});
  std::vector<bool> intersection_results(
      {true, false, true, false, true, true, false, false});
  std::vector<bool> difference_results(
      {false, true, false, true, true, true, false, false});

  ASSERT_EQ(left_hit.size(), inside_left.size());
  ASSERT_EQ(left_hit.size(), inside_right.size());

  ASSERT_EQ(left_hit.size(), union_results.size());
  ASSERT_EQ(left_hit.size(), intersection_results.size());
  ASSERT_EQ(left_hit.size(), difference_results.size());

  for (size_t i = 0; i < left_hit.size(); ++i) {
    EXPECT_EQ(csg.intersection_allowed(CSG::Operation::Union, left_hit[i],
                                       inside_left[i], inside_right[i]),
              union_results[i]);
  }

  for (size_t i = 0; i < left_hit.size(); ++i) {
    EXPECT_EQ(
        csg.intersection_allowed(CSG::Operation::Intersection, left_hit[i],
                                 inside_left[i], inside_right[i]),
        intersection_results[i]);
  }

  for (size_t i = 0; i < left_hit.size(); ++i) {
    EXPECT_EQ(csg.intersection_allowed(CSG::Operation::Difference, left_hit[i],
                                       inside_left[i], inside_right[i]),
              difference_results[i]);
  }
}

TEST(CSG, IncludesAShape) {
  Sphere sphere;
  Cube cube;

  CSG sphere_cube_csg(CSG::Operation::Union, &sphere, &cube);

  ASSERT_TRUE(sphere_cube_csg.includes(&sphere));
  ASSERT_TRUE(sphere_cube_csg.includes(&cube));
}

TEST(CSG, FilteringAListOfIntersections) {
  Sphere sphere;
  Cube cube;

  std::vector<Intersection> intersections(
      {Intersection(1.f, &sphere), Intersection(2.f, &cube),
       Intersection(3.f, &sphere), Intersection(4.f, &cube)});

  std::vector<CSG::Operation> op({CSG::Operation::Union,
                                  CSG::Operation::Intersection,
                                  CSG::Operation::Difference});
  std::vector<int> first_index({0, 1, 0});
  std::vector<int> second_index({3, 2, 1});

  ASSERT_EQ(op.size(), first_index.size());
  ASSERT_EQ(op.size(), second_index.size());

  constexpr size_t old_size = 0;

  for (size_t i = 0; i < op.size(); ++i) {
    CSG sphere_cube_csg(op[i], &sphere, &cube);
    std::vector<Intersection> filtered(intersections);

    sphere_cube_csg.filter_intersections(filtered, old_size);

    ASSERT_EQ(filtered.size(), 2);
    ASSERT_EQ(filtered[0], intersections[first_index[i]]);
    ASSERT_EQ(filtered[1], intersections[second_index[i]]);
  }
}

TEST(CSG, ARayMissesACSGObject) {
  Sphere sphere;
  Cube cube;

  CSG sphere_cube_union(CSG::Operation::Union, &sphere, &cube);

  Ray ray(point(0, 2, -5), vec(0, 0, 1));
  std::vector<Intersection> intersections;
  sphere_cube_union.intersect(ray, intersections);

  ASSERT_TRUE(intersections.empty());
}

TEST(CSG, ARayHitsACSGObject) {
  Sphere sphere1;

  Sphere sphere2;
  sphere2.set_transform(translation(0, 0, 0.5));

  CSG spheres_union(CSG::Operation::Union, &sphere1, &sphere2);

  Ray ray(point(0, 0, -5), vec(0, 0, 1));
  std::vector<Intersection> intersections;
  spheres_union.intersect(ray, intersections);

  ASSERT_EQ(intersections.size(), 2);
  ASSERT_TRUE(eql(intersections[0].t, 4));
  ASSERT_EQ(intersections[0].object, &sphere1);
  ASSERT_TRUE(eql(intersections[1].t, 6.5));
  ASSERT_EQ(intersections[1].object, &sphere2);
}

TEST(CSG, ACSGShapeHasABoundingBoxThatContainsItsChildren) {
  Sphere left_sphere;

  Sphere right_sphere;
  right_sphere.set_transform(translation(2, 3, 4));

  CSG sphere_sphere(CSG::Operation::Difference, &left_sphere, &right_sphere);
  BoundingBox sphere_sphere_bounds = sphere_sphere.get_bounds();

  ASSERT_EQ(sphere_sphere_bounds.min_point, point(-1, -1, -1));
  ASSERT_EQ(sphere_sphere_bounds.max_point, point(3, 4, 5));
}

TEST(CSG, SubdividingACSGShapeSubdividesItsChildren) {
  Sphere sphere1;
  sphere1.set_transform(translation(-1.5, 0, 0));

  Sphere sphere2;
  sphere2.set_transform(translation(1.5, 0, 0));

  Group left_group;
  left_group.add_child(std::move(sphere1));
  left_group.add_child(std::move(sphere2));

  Sphere sphere3;
  sphere3.set_transform(translation(0, 0, -1.5));

  Sphere sphere4;
  sphere4.set_transform(translation(0, 0, 1.5));

  Group right_group;
  right_group.add_child(std::move(sphere3));
  right_group.add_child(std::move(sphere4));

  CSG csg(CSG::Operation::Difference, &left_group, &right_group);

  csg.divide(1);

  auto& left_group_groups = left_group.get<Group>();
  ASSERT_EQ(left_group_groups.size(), 2);

  auto& left_group_group0_spheres = left_group_groups[0].get<Sphere>();
  ASSERT_EQ(left_group_group0_spheres.size(), 1);
  EXPECT_EQ(left_group_group0_spheres[0], sphere1);

  auto& left_group_group1_spheres = left_group_groups[1].get<Sphere>();
  ASSERT_EQ(left_group_group1_spheres.size(), 1);
  EXPECT_EQ(left_group_group1_spheres[0], sphere2);

  auto& right_group_groups = right_group.get<Group>();
  ASSERT_EQ(right_group_groups.size(), 2);

  auto& right_group_group0_spheres = right_group_groups[0].get<Sphere>();
  ASSERT_EQ(right_group_group0_spheres.size(), 1);
  EXPECT_EQ(right_group_group0_spheres[0], sphere3);

  auto& right_group_group1_spheres = right_group_groups[1].get<Sphere>();
  ASSERT_EQ(right_group_group1_spheres.size(), 1);
  EXPECT_EQ(right_group_group1_spheres[0], sphere4);
}
