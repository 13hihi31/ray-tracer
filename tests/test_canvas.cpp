#include <gtest/gtest.h>

#include <raytracer/canvas.hpp>

TEST(CanvasTest, CreateACanvas) {
  Canvas canvas = Canvas(20, 10);

  ASSERT_EQ(canvas.width, 20);
  ASSERT_EQ(canvas.height, 10);
}

class CanvasAssignTest : public ::testing::Test {
 protected:
  void SetUp() override { canvas = Canvas(20, 10); }

  Canvas canvas;
};

TEST_F(CanvasAssignTest, NoChangeAppliedToACanvas) {
  ASSERT_EQ(canvas(3, 2), color(0, 0, 0));
}

TEST_F(CanvasAssignTest, AssignANewValueToPixel) {
  canvas(3, 2) = color(1, 0, 0);

  ASSERT_EQ(canvas(3, 2), color(1, 0, 0));
}

TEST(CanvasTest, ConstructingThePPMPixelData) {
  Canvas canvas = Canvas(5, 3);
  canvas(0, 0) = color(1.5, 0, 0);
  canvas(2, 1) = color(0, 0.5, 0);
  canvas(4, 2) = color(-0.5, 0, 1);
  std::string ppm = canvas.to_ppm();
  std::string target_ppm = std::string(
      "P3\n5 3\n255\n255 0 0 0 0 0 0 0 0 0 0 0 0 0 0\n0 0 0 0 0 0 "
      "0 127 0 0 0 0 0 0 0\n0 0 0 0 0 0 0 0 0 0 0 0 0 0 255\n");

  ASSERT_EQ(ppm, target_ppm);
}

TEST(CanvasTest, PPMEndsWithANewLineCharacter) {
  Canvas canvas = Canvas(5, 3);
  canvas(0, 0) = color(1.5, 0, 0);
  canvas(2, 1) = color(0, 0.5, 0);
  canvas(4, 2) = color(-0.5, 0, 1);
  std::string ppm = canvas.to_ppm();

  ASSERT_EQ(*(ppm.end() - 1), '\n');
}
