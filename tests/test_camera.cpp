#include <gtest/gtest.h>

#include <cmath>
#include <raytracer/camera.hpp>
#include <raytracer/transforms.hpp>
#include <raytracer/tuple.hpp>

TEST(CameraTest, ConstructingACamera) {
  unsigned horizontal_size = 160;
  unsigned vertical_size = 120;
  float field_of_view = M_PI / 2.0f;
  Camera camera(horizontal_size, vertical_size, field_of_view);

  ASSERT_EQ(camera.horizontal_size, 160);
  ASSERT_EQ(camera.vertical_size, 120);
  ASSERT_TRUE(eql(camera.field_of_view, M_PI / 2.0f));
  ASSERT_EQ(camera.world_transform_inverse, identity_matrix<4>());
}

TEST(CameraTest, ThePixelSizeForAHorizontalCanvas) {
  Camera camera(200, 125, M_PI / 2.0f);

  EXPECT_EQ(camera.pixel_size, 0.01f);
}

TEST(CameraTest, ThePixelSizeForAVerticalCanvas) {
  Camera camera(125, 200, M_PI / 2.0f);

  EXPECT_EQ(camera.pixel_size, 0.01f);
}

TEST(CameraTest, ConstructingARayThroughTheCenterOfTheCanvas) {
  Camera camera(201, 101, M_PI / 2.0f);
  Ray ray = camera.ray_for_pixel(100, 50);

  EXPECT_EQ(ray.origin, point(0, 0, 0));
  EXPECT_EQ(ray.direction, vec(0, 0, -1));
}

TEST(CameraTest, ConstructingARayThroughACornerOfTheCanvas) {
  Camera camera(201, 101, M_PI / 2.0f);
  Ray ray = camera.ray_for_pixel(0, 0);

  EXPECT_EQ(ray.origin, point(0, 0, 0));
  EXPECT_EQ(ray.direction, vec(0.66519, 0.33259, -0.66851));
}

TEST(CameraTest, ConstructingARayWhenTheCameraIsTransformed) {
  Camera camera(201, 101, M_PI / 2.0f);
  camera.set_transform(mul(rotation_y(M_PI / 4.0f), translation(0, -2, 5)));
  Ray ray = camera.ray_for_pixel(100, 50);

  EXPECT_EQ(ray.origin, point(0, 2, -5));
  EXPECT_EQ(ray.direction, vec(sqrtf(2.0f) / 2.0f, 0.0f, -sqrtf(2.0f) / 2.0f));
}

TEST(CameraTest, RenderingAWorldWithACamera) {
  World world = default_world();
  Camera camera(11, 11, M_PI / 2.0f);
  Point from = point(0, 0, -5);
  Point to = point(0, 0, 0);
  Vec up = vec(0, 1, 0);
  camera.set_transform(view_transform(from, to, up));
  Canvas canvas = camera.render(world);

  EXPECT_EQ(canvas(5, 5), color(0.38066, 0.47583, 0.2855));
}
