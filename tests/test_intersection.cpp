#include <gtest/gtest.h>

#include <algorithm>
#include <raytracer/constants.hpp>
#include <raytracer/helper_functions.hpp>
#include <raytracer/intersection.hpp>
#include <raytracer/intersection_data.hpp>
#include <raytracer/plane.hpp>
#include <raytracer/schlick.hpp>
#include <raytracer/shape.hpp>
#include <raytracer/sphere.hpp>
#include <raytracer/transforms.hpp>
#include <raytracer/triangle.hpp>
#include <vector>

TEST(Intersection, AnIntersectionEncapsulatesTAndObject) {
  Sphere sphere;

  Intersection intersection(3.5f, &sphere);

  EXPECT_EQ(intersection.t, 3.5f);
  EXPECT_TRUE(intersection.object->eq(sphere));
}

TEST(Intersection, AggregatingIntersections) {
  Sphere sphere;

  Intersection intersection0(1, &sphere);
  Intersection intersection1(2, &sphere);
  std::vector<Intersection> intersections({intersection0, intersection1});

  EXPECT_EQ(intersections.size(), 2);
  EXPECT_TRUE(eql(intersections[0].t, 1.0f));
  EXPECT_TRUE(eql(intersections[1].t, 2.0f));
}

TEST(Intersection, TheHitWhenAllIntersectionsHavePositiveT) {
  Sphere sphere;

  Intersection intersection0(1, &sphere);
  Intersection intersection1(2, &sphere);
  std::vector<Intersection> intersections({intersection0, intersection1});

  auto hit_intersection = Intersection::hit(intersections);

  ASSERT_TRUE(hit_intersection);
  EXPECT_EQ(*hit_intersection, intersection0);
}

TEST(Intersection, TheHitWhenSomeIntersectionsHaveNegativeT) {
  Sphere sphere;

  Intersection intersection0(-1, &sphere);
  Intersection intersection1(2, &sphere);
  std::vector<Intersection> intersections({intersection0, intersection1});

  auto hit_intersection = Intersection::hit(intersections);

  ASSERT_TRUE(hit_intersection);
  EXPECT_EQ(*hit_intersection, intersection1);
}

TEST(Intersection, TheHitWhenAllIntersectionsHaveNegativeT) {
  Sphere sphere;

  Intersection intersection0(-2, &sphere);
  Intersection intersection1(-1, &sphere);
  std::vector<Intersection> intersections({intersection1, intersection0});

  auto hit_intersection = Intersection::hit(intersections);

  ASSERT_FALSE(hit_intersection);
}

TEST(Intersection, TheHitIsAlwaysTheLowestNonnegativeIntersection) {
  Sphere sphere;

  Intersection intersection0(5, &sphere);
  Intersection intersection1(7, &sphere);
  Intersection intersection2(-3, &sphere);
  Intersection intersection3(2, &sphere);
  std::vector<Intersection> intersections(
      {intersection0, intersection1, intersection2, intersection3});

  std::sort(intersections.begin(), intersections.end());
  std::optional<Intersection> hit_intersection =
      Intersection::hit(intersections);

  ASSERT_TRUE(hit_intersection);
  ASSERT_EQ(*hit_intersection, intersection3);
}

TEST(Intersection, PrecomputingTheStateOfAnIntersection) {
  Ray ray(point(0, 0, -5), vec(0, 0, 1));
  Sphere sphere;

  Intersection intersection(4.0f, &sphere);
  IntersectionData intersection_data(intersection, ray);

  EXPECT_EQ(intersection_data.t, intersection.t);
  EXPECT_EQ(intersection_data.object, intersection.object);
  EXPECT_EQ(intersection_data.intersection_point, point(0, 0, -1));
  EXPECT_EQ(intersection_data.eye_vector, vec(0, 0, -1));
  EXPECT_EQ(intersection_data.normal_vector, vec(0, 0, -1));
}

TEST(Intersection, TheHitWhenAnIntersectionOccursOnTheOutside) {
  Ray ray(point(0, 0, -5), vec(0, 0, 1));
  Sphere sphere;
  Intersection intersection(4, &sphere);
  IntersectionData intersection_data(intersection, ray);

  EXPECT_FALSE(intersection_data.inside_object);
}

TEST(Intersection, TheHitWhenAnIntersectionOccursOnTheInside) {
  Ray ray(point(0, 0, 0), vec(0, 0, 1));
  Sphere sphere;
  Intersection intersection(1, &sphere);
  IntersectionData intersection_data(intersection, ray);

  EXPECT_EQ(intersection_data.intersection_point, point(0, 0, 1));
  EXPECT_EQ(intersection_data.eye_vector, vec(0, 0, -1));
  EXPECT_TRUE(intersection_data.inside_object);
  EXPECT_EQ(intersection_data.normal_vector, vec(0, 0, -1));
}

TEST(Intersection, TheHitShouldOffsetThePoint) {
  Ray ray(point(0, 0, -5), vec(0, 0, 1));
  Sphere sphere;
  sphere.set_transform(translation(0, 0, 1));

  Intersection intersection(5, &sphere);
  IntersectionData intersection_data(intersection, ray);

  EXPECT_LT(intersection_data.over_point.z, -consts::epsilon / 2.0f);
  EXPECT_GT(intersection_data.intersection_point.z,
            intersection_data.over_point.z);
}

TEST(Intersection, PrecomputingTheReflectionVector) {
  Plane plane;
  Ray ray(point(0, 1, -1), vec(0, -sqrtf(2.0f) / 2.0f, sqrtf(2.0f) / 2.0f));

  Intersection intersection(sqrtf(2.0f), &plane);
  IntersectionData intersection_data(intersection, ray);

  EXPECT_EQ(intersection_data.reflect_vector,
            vec(0, sqrtf(2.0f) / 2.0f, sqrtf(2.0f) / 2.0f));
}

TEST(Intersection,
     FindingExitingAndEnteringRefractiveIndicesAtVariousIntersections) {
  Sphere sphere1 = glass_sphere();
  sphere1.set_transform(scaling(2, 2, 2));
  sphere1.material.refractive_index = 1.5f;

  Sphere sphere2 = glass_sphere();
  sphere2.set_transform(translation(0, 0, -0.25));
  sphere2.material.refractive_index = 2.0f;

  Sphere sphere3 = glass_sphere();
  sphere3.set_transform(translation(0, 0, 0.25));
  sphere3.material.refractive_index = 2.5f;

  Ray ray(point(0, 0, -4), vec(0, 0, 1));

  std::vector<Intersection> intersections = {
      Intersection(2.0f, &sphere1),  Intersection(2.75f, &sphere2),
      Intersection(3.25f, &sphere3), Intersection(4.75f, &sphere2),
      Intersection(5.25f, &sphere3), Intersection(6.0f, &sphere1)};

  std::vector<float> exiting_refractive_indices = {1.0f, 1.5f, 2.0f,
                                                   2.5f, 2.5f, 1.5f};
  std::vector<float> entering_refractive_indices = {1.5f, 2.0f, 2.5f,
                                                    2.5f, 1.5f, 1.0f};

  for (size_t i = 0; i < exiting_refractive_indices.size(); ++i) {
    IntersectionData intersection_data(intersections[i], ray, intersections);

    EXPECT_EQ(intersection_data.exiting_refractive_index,
              exiting_refractive_indices[i]);
    EXPECT_EQ(intersection_data.entering_refractive_index,
              entering_refractive_indices[i]);
  }
}

TEST(Intersection, TheUnderPointIsOffsetBelowTheSurface) {
  Ray ray(point(0, 0, -5), vec(0, 0, 1));
  Sphere sphere = glass_sphere();
  sphere.set_transform(translation(0, 0, 1));

  Intersection intersection(5, &sphere);
  std::vector<Intersection> intersections({intersection});
  IntersectionData intersection_data(intersection, ray, intersections);

  EXPECT_GT(intersection_data.under_point.z, consts::epsilon / 2.0f);
  EXPECT_LT(intersection_data.intersection_point.z,
            intersection_data.under_point.z);
}

TEST(Schlick, TheSchlickApproximationUnderTotalIternalReflection) {
  Sphere sphere = glass_sphere();
  Ray ray(point(0, 0, sqrtf(2.0f) / 2.0f), vec(0, 1, 0));

  std::vector<Intersection> intersections(
      {Intersection(-sqrtf(2.0f) / 2.0f, &sphere),
       Intersection(sqrtf(2.0f) / 2.0f, &sphere)});

  IntersectionData intersection_data(intersections[1], ray, intersections);
  float reflectance = schlick(intersection_data);

  EXPECT_TRUE(eql(reflectance, 1.0f));
}

TEST(Schlick, TheSchlickApproximationWithAPerpendicularViewingAngle) {
  Sphere sphere = glass_sphere();

  Ray ray(point(0, 0, 0), vec(0, 1, 0));
  std::vector<Intersection> intersections(
      {Intersection(-1.0f, &sphere), Intersection(1.0f, &sphere)});
  IntersectionData intersection_data(intersections[1], ray, intersections);
  float reflectance = schlick(intersection_data);

  EXPECT_TRUE(eql(reflectance, 0.04f));
}

TEST(
    Schlick,
    TheSchlickApproximationWithSmallAngleAndEnteringGreaterThanExitingRefractiveIndex) {
  Sphere sphere = glass_sphere();
  Ray ray(point(0, 0.99, -2), vec(0, 0, 1));
  std::vector<Intersection> intersections({Intersection(1.8589, &sphere)});
  IntersectionData intersection_data(intersections[0], ray, intersections);
  float reflectance = schlick(intersection_data);

  EXPECT_TRUE(eql(reflectance, 0.48873f));
}

TEST(Intersection, AnIntersectionCanEncapsulateUAndV) {
  Triangle triangle(point(0, 1, 0), point(-1, 0, 0), point(1, 0, 0));
  Intersection intersection(3.5, &triangle, 0.2, 0.4);

  EXPECT_TRUE(eql(intersection.u, 0.2));
  EXPECT_TRUE(eql(intersection.v, 0.4));
}
