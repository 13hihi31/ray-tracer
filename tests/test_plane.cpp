#include <gtest/gtest.h>

#include <limits>
#include <raytracer/plane.hpp>

TEST(Plane, TheNormalOfAPlaneIsConstantEverywhere) {
  Plane plane;
  Intersection dummy_intersection(0, &plane);
  Vec normal1 = plane.local_normal_at(point(0, 0, 0), dummy_intersection);
  Vec normal2 = plane.local_normal_at(point(10, 0, -10), dummy_intersection);
  Vec normal3 = plane.local_normal_at(point(-5, 0, 150), dummy_intersection);

  EXPECT_EQ(normal1, vec(0, 1, 0));
  EXPECT_EQ(normal2, vec(0, 1, 0));
  EXPECT_EQ(normal3, vec(0, 1, 0));
}

TEST(Plane, IntersectWithARayParallelToThePlane) {
  Plane plane;
  Ray ray(point(0, 10, 0), vec(0, 0, 1));
  std::vector<Intersection> intersections;
  plane.intersect(ray, intersections);

  EXPECT_TRUE(intersections.empty());
}

TEST(Plane, IntersectWithACoplanarRay) {
  Plane plane;
  Ray ray(point(0, 0, 0), vec(0, 0, 1));
  std::vector<Intersection> intersections;
  plane.intersect(ray, intersections);

  EXPECT_TRUE(intersections.empty());
}

TEST(Plane, ARayIntersectingAPlaneFromAbove) {
  Plane plane;
  Ray ray(point(0, 1, 0), vec(0, -1, 0));
  std::vector<Intersection> intersections;
  plane.intersect(ray, intersections);

  EXPECT_EQ(intersections.size(), 1);
  EXPECT_TRUE(eql(intersections[0].t, 1));
  EXPECT_TRUE(intersections[0].object->eq(plane));
}

TEST(Plane, ARayIntersectingAPlaneFromBelow) {
  Plane plane;
  Ray ray(point(0, -1, 0), vec(0, 1, 0));
  std::vector<Intersection> intersections;
  plane.intersect(ray, intersections);

  EXPECT_EQ(intersections.size(), 1);
  EXPECT_TRUE(eql(intersections[0].t, 1));
  EXPECT_TRUE(intersections[0].object->eq(plane));
}

TEST(Plane, APlaneHasABoundingBox) {
  Plane plane;
  BoundingBox plane_bounding_box = plane.get_bounds();

  EXPECT_EQ(plane_bounding_box.min_point,
            point(std::numeric_limits<float>::lowest(), 0,
                  std::numeric_limits<float>::lowest()));
  EXPECT_EQ(plane_bounding_box.max_point,
            point(std::numeric_limits<float>::max(), 0,
                  std::numeric_limits<float>::max()));
}
