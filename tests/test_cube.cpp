#include <gtest/gtest.h>

#include <algorithm>
#include <raytracer/cube.hpp>
#include <raytracer/intersection.hpp>
#include <raytracer/ray.hpp>
#include <raytracer/shape.hpp>
#include <raytracer/tuple.hpp>
#include <vector>

TEST(Cube, ARayIntersectsACube) {
  Cube cube;

  std::vector<Point> origin(
      {point(5, 0.5, 0), point(-5, 0.5, 0), point(0.5, 5, 0), point(0.5, -5, 0),
       point(0.5, 0, 5), point(0.5, 0, -5), point(0, 0.5, 0)});
  std::vector<Vec> direction({vec(-1, 0, 0), vec(1, 0, 0), vec(0, -1, 0),
                              vec(0, 1, 0), vec(0, 0, -1), vec(0, 0, 1),
                              vec(0, 0, 1)});
  std::vector<float> intersection0_t({4, 4, 4, 4, 4, 4, -1});
  std::vector<float> intersection1_t({6, 6, 6, 6, 6, 6, 1});

  size_t size = origin.size();
  ASSERT_EQ(size, direction.size());
  ASSERT_EQ(size, intersection0_t.size());
  ASSERT_EQ(size, intersection1_t.size());

  for (size_t i = 0; i < origin.size(); ++i) {
    Ray ray(origin[i], direction[i]);
    std::vector<Intersection> intersections;
    cube.intersect(ray, intersections);
    std::sort(intersections.begin(), intersections.end());

    EXPECT_EQ(intersections.size(), 2);
    EXPECT_EQ(intersections[0].t, intersection0_t[i]);
    EXPECT_EQ(intersections[1].t, intersection1_t[i]);
  }
}

TEST(Cube, ARayMissesACube) {
  Cube cube;

  std::vector<Point> origin({point(-2, 0, 0), point(0, -2, 0), point(0, 0, -2),
                             point(2, 0, 2), point(0, 2, 2), point(2, 2, 0)});
  std::vector<Vec> direction({vec(0.2673, 0.5345, 0.8018),
                              vec(0.8018, 0.2673, 0.5345),
                              vec(0.5345, 0.8018, 0.2673), vec(0, 0, -1),
                              vec(0, -1, 0), vec(-1, 0, 0)});

  ASSERT_EQ(origin.size(), direction.size());

  for (size_t i = 0; i < origin.size(); ++i) {
    Ray ray(origin[i], direction[i]);
    std::vector<Intersection> intersections;
    cube.intersect(ray, intersections);

    EXPECT_EQ(intersections.size(), 0);
  }
}

TEST(Cube, TheNormalOnTheSurfaceOfACube) {
  Cube cube;

  std::vector<Point> points({point(1, 0.5, -0.8), point(-1, 0.2, 0.9),
                             point(-0.4, 1, -0.1), point(0.3, -1, -0.7),
                             point(-0.6, 0.3, 1), point(0.4, 0.4, -1),
                             point(1, 1, 1), point(-1, -1, -1)});
  std::vector<Vec> normals({vec(1, 0, 0), vec(-1, 0, 0), vec(0, 1, 0),
                            vec(0, -1, 0), vec(0, 0, 1), vec(0, 0, -1),
                            vec(1, 0, 0), vec(-1, 0, 0)});
  Intersection dummy_intersection(0, &cube);

  ASSERT_EQ(points.size(), normals.size());

  for (size_t i = 0; i < points.size(); ++i) {
    EXPECT_EQ(cube.local_normal_at(points[i], dummy_intersection), normals[i]);
  }
}

TEST(Cube, ACubeHasABoundingBox) {
  Cube cube;

  BoundingBox cube_bounding_box = cube.get_bounds();

  EXPECT_EQ(cube_bounding_box.min_point, point(-1, -1, -1));
  EXPECT_EQ(cube_bounding_box.max_point, point(1, 1, 1));
}
