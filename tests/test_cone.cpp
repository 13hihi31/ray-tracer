#include <gtest/gtest.h>

#include <raytracer/cone.hpp>
#include <raytracer/helper_functions.hpp>
#include <raytracer/intersection.hpp>
#include <raytracer/ray.hpp>
#include <raytracer/tuple.hpp>
#include <vector>

TEST(Cone, IntersectingAConeWithARay) {
  Cone cone;

  std::vector<Point> origins(
      {point(0, 0, -5), point(0, 0, -5 + 0.000001f), point(1, 1, -5)});
  std::vector<Vec> directions({vec(0, 0, 1), vec(1, 1, 1), vec(-0.5, -1, 1)});

  std::vector<float> intersection0_t({5, 8.66025, 4.55006});
  std::vector<float> intersection1_t({5, 8.66025, 49.44994});

  for (size_t i = 0; i < origins.size(); ++i) {
    Vec direction = norm(directions[i]);
    Ray ray(origins[i], direction);

    std::vector<Intersection> intersections;
    cone.intersect(ray, intersections);
    std::sort(intersections.begin(), intersections.end());

    ASSERT_EQ(intersections.size(), 2);
    EXPECT_TRUE(eql(intersections[0].t, intersection0_t[i]));
    EXPECT_TRUE(eql(intersections[1].t, intersection1_t[i]));
  }
}

TEST(Cone, IntersectingAConeWithARayParallelToOneOfItsHalves) {
  Cone cone;

  Vec direction = norm(vec(0, 1, 1));
  Ray ray(point(0, 0, -1), direction);

  std::vector<Intersection> intersections;
  cone.intersect(ray, intersections);

  ASSERT_EQ(intersections.size(), 1);
  EXPECT_TRUE(eql(intersections[0].t, 0.35355));
}

TEST(Cone, IntersectingAConesEndCaps) {
  Cone cone;
  cone.minimum = -0.5f;
  cone.maximum = 0.5f;
  cone.closed = true;

  std::vector<Point> origins(
      {point(0, 0, -5), point(0, 0, -0.25), point(0, 0, -0.25)});
  std::vector<Vec> directions({vec(0, 1, 0), vec(0, 1, 1), vec(0, 1, 0)});

  std::vector<size_t> sizes({0, 2, 4});

  for (size_t i = 0; i < origins.size(); ++i) {
    Vec direction = norm(directions[i]);
    Ray ray(origins[i], direction);
    std::vector<Intersection> intersections;
    cone.intersect(ray, intersections);

    EXPECT_EQ(intersections.size(), sizes[i]);
  }
}

TEST(Cone, ComputingTheNormalVectorOnACone) {
  Cone cone;

  std::vector<Point> points({point(0, 0, 0), point(1, 1, 1), point(-1, -1, 0)});
  std::vector<Vec> normals(
      {vec(0, 0, 0), vec(1, -sqrtf(2.0f), 1), vec(-1, 1, 0)});

  Intersection dummy_intersection(0, &cone);

  for (size_t i = 0; i < points.size(); ++i) {
    EXPECT_EQ(cone.local_normal_at(points[i], dummy_intersection), normals[i]);
  }
}

TEST(Cone, AnUnboundedConeHasABoundingBox) {
  Cone cone;
  BoundingBox cone_bounding_box = cone.get_bounds();

  EXPECT_EQ(cone_bounding_box.min_point,
            point(std::numeric_limits<float>::lowest(),
                  std::numeric_limits<float>::lowest(),
                  std::numeric_limits<float>::lowest()));
  EXPECT_EQ(cone_bounding_box.max_point,
            point(std::numeric_limits<float>::max(),
                  std::numeric_limits<float>::max(),
                  std::numeric_limits<float>::max()));
}

TEST(Cone, AnBoundedConeHasABoundingBox) {
  Cone cone;
  cone.minimum = -5;
  cone.maximum = 3;
  BoundingBox cone_bounding_box = cone.get_bounds();

  EXPECT_EQ(cone_bounding_box.min_point, point(-5, -5, -5));
  EXPECT_EQ(cone_bounding_box.max_point, point(5, 3, 5));
}
