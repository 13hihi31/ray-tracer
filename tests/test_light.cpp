#include <gtest/gtest.h>

#include <raytracer/light.hpp>
#include <raytracer/tuple.hpp>

TEST(Light, APointLightHasAPositionAndIntensity) {
  Color intensity = color(1, 1, 1);
  Point position = point(0, 0, 0);
  Light light(position, intensity);

  EXPECT_EQ(light.position, position);
  EXPECT_EQ(light.intensity, intensity);
}
