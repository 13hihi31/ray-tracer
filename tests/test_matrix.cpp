#include <gtest/gtest.h>

#include <raytracer/helper_functions.hpp>
#include <raytracer/matrix.hpp>

TEST(Matrix, ConstructingADefault4x4Matrix) {
  Matrix<4> m;

  EXPECT_EQ(m.size(), 4);
}

TEST(Matrix, ConstructingADefault3x3Matrix) {
  Matrix<3> m;

  EXPECT_EQ(m.size(), 3);
}

TEST(Matrix, ConstructingAMatrixFromArray) {
  Matrix<4> m({{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}});

  EXPECT_EQ(m.size(), 4);
}

TEST(Matrix, NewMatrixAssignment) {
  Matrix<2> m;
  EXPECT_EQ(m(0, 0), 0);

  m = {{1, 2}, {3, 4}};
  EXPECT_EQ(m(0, 0), 1);
}

TEST(Matrix, AccessingMatrixElements) {
  Matrix<4> m({{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}});

  EXPECT_EQ(m(0, 0), 1);
  EXPECT_EQ(m(1, 1), 6);
  EXPECT_EQ(m(2, 3), 12);
  EXPECT_EQ(m(2, 3), 12);
}

TEST(Matrix, ConstructingA2x2MatrixAndAccessingItsElements) {
  Matrix<2> m({{1, 2}, {3, 4}});

  EXPECT_EQ(m(0, 0), 1);
  EXPECT_EQ(m(1, 1), 4);
}

TEST(Matrix, MatixEqualityWithSameMatrices) {
  Matrix<4> m1({{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}});
  Matrix<4> m2({{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}});

  EXPECT_EQ(m1, m2);
}

TEST(Matrix, MatixEqualityWithDifferentMatrices) {
  Matrix<4> m1({{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}});
  Matrix<4> m2({{1, 2, 3, 4}, {5, 6, 7, 8}, {12, 11, 10, 9}, {8, 7, 6, 5}});

  EXPECT_TRUE(!(m1 == m2));
}

TEST(Matrix, MultiplyingTwoMatrices) {
  Matrix<4> m1({{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 8, 7, 6}, {5, 4, 3, 2}});
  Matrix<4> m2({{-2, 1, 2, 3}, {3, 2, 1, -1}, {4, 3, 6, 5}, {1, 2, 7, 8}});
  Matrix<4> m3 = mul(m1, m2);

  EXPECT_TRUE(eql(m3(0, 0), 20));
  EXPECT_TRUE(eql(m3(0, 1), 22));
  EXPECT_TRUE(eql(m3(0, 2), 50));
  EXPECT_TRUE(eql(m3(0, 3), 48));
  EXPECT_TRUE(eql(m3(1, 0), 44));
  EXPECT_TRUE(eql(m3(1, 1), 54));
  EXPECT_TRUE(eql(m3(1, 2), 114));
  EXPECT_TRUE(eql(m3(1, 3), 108));
  EXPECT_TRUE(eql(m3(2, 0), 40));
  EXPECT_TRUE(eql(m3(2, 1), 58));
  EXPECT_TRUE(eql(m3(2, 2), 110));
  EXPECT_TRUE(eql(m3(2, 3), 102));
  EXPECT_TRUE(eql(m3(3, 0), 16));
  EXPECT_TRUE(eql(m3(3, 1), 26));
  EXPECT_TRUE(eql(m3(3, 2), 46));
  EXPECT_TRUE(eql(m3(3, 3), 42));
}

TEST(Matrix, MultiplyingATupleByAMatrix) {
  Matrix<4> A({{1, 2, 3, 4}, {2, 4, 4, 2}, {8, 6, 4, 1}, {0, 0, 0, 1}});
  Tuple b(1, 2, 3, 1);
  Tuple c = mul(A, b);

  EXPECT_TRUE(eql(c.x, 18));
  EXPECT_TRUE(eql(c.y, 24));
  EXPECT_TRUE(eql(c.z, 33));
  EXPECT_TRUE(eql(c.w, 1));
}

TEST(Matrix, MultiplyingAMatrixByTheIdentityMatrix) {
  Matrix<4> A({{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}});
  Matrix<4> I = identity_matrix<4>();

  EXPECT_EQ(mul(A, I), A);
}

TEST(Matrix, MultiplyingATupleByAnIdentityMatrix) {
  Matrix<4> I = identity_matrix<4>();
  Tuple a(1, 2, 3, 4);

  EXPECT_EQ(mul(I, a), a);
}

TEST(Matrix, TransposingAMatrix) {
  Matrix<4> A({{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}});
  Matrix<4> B = A.transpose();

  EXPECT_TRUE(eql(B(0, 0), 1));
  EXPECT_TRUE(eql(B(0, 1), 5));
  EXPECT_TRUE(eql(B(0, 2), 9));
  EXPECT_TRUE(eql(B(0, 3), 13));
  EXPECT_TRUE(eql(B(1, 0), 2));
  EXPECT_TRUE(eql(B(1, 1), 6));
  EXPECT_TRUE(eql(B(1, 2), 10));
  EXPECT_TRUE(eql(B(1, 3), 14));
  EXPECT_TRUE(eql(B(2, 0), 3));
  EXPECT_TRUE(eql(B(2, 1), 7));
  EXPECT_TRUE(eql(B(2, 2), 11));
  EXPECT_TRUE(eql(B(2, 3), 15));
  EXPECT_TRUE(eql(B(3, 0), 4));
  EXPECT_TRUE(eql(B(3, 1), 8));
  EXPECT_TRUE(eql(B(3, 2), 12));
  EXPECT_TRUE(eql(B(3, 3), 16));
}

TEST(Matrix, CalculatingTheDeterminantOfA2x2Matrix) {
  Matrix<2> A({{1, 5}, {-3, 2}});

  EXPECT_TRUE(eql(A.det(), 17));
}

TEST(Matrix, ASubmatrixOfA3x3MatrixIsA2x2Matrix) {
  Matrix<3> A({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
  Matrix<2> B = A.submatrix(0, 2);

  EXPECT_EQ(B, Matrix<2>({{4, 5}, {7, 8}}));
  EXPECT_TRUE(eql(B(0, 0), 4));
  EXPECT_TRUE(eql(B(0, 1), 5));
  EXPECT_TRUE(eql(B(1, 0), 7));
  EXPECT_TRUE(eql(B(1, 1), 8));
}

TEST(Matrix, ASubmatrixOfA4x4MatrixIsA3x3Matrix) {
  Matrix<4> A({{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}});
  Matrix<3> B = A.submatrix(2, 1);

  EXPECT_EQ(B, Matrix<3>({{1, 3, 4}, {5, 7, 8}, {13, 15, 16}}));
}

TEST(Matrix, CalculatingAMinorOfA3x3Matrix) {
  Matrix<3> A({{3, 5, 0}, {2, -1, -7}, {6, -1, 5}});
  Matrix<2> B = A.submatrix(1, 0);

  EXPECT_TRUE(eql(B.det(), 25));
  EXPECT_TRUE(eql(A.matrix_minor(1, 0), 25));
}

TEST(Matrix, CalculatingACofactorOfA3x3Matrix) {
  Matrix<3> A({{3, 5, 0}, {2, -1, -7}, {6, -1, 5}});

  EXPECT_TRUE(eql(A.matrix_minor(0, 0), -12));
  EXPECT_TRUE(eql(A.cofactor(0, 0), -12));
  EXPECT_TRUE(eql(A.matrix_minor(1, 0), 25));
  EXPECT_TRUE(eql(A.cofactor(1, 0), -25));
}

TEST(Matrix, CalculatingTheDeterminantOfA3x3Matrix) {
  Matrix<3> A({{1, 2, 6}, {-5, 8, -4}, {2, 6, 4}});

  EXPECT_TRUE(eql(A.cofactor(0, 0), 56));
  EXPECT_TRUE(eql(A.cofactor(0, 1), 12));
  EXPECT_TRUE(eql(A.cofactor(0, 2), -46));
  EXPECT_TRUE(eql(A.det(), -196));
}

TEST(Matrix, CalculatingTheDeterminantOfA4x4Matrix) {
  Matrix<4> A({{-2, -8, 3, 5}, {-3, 1, 7, 3}, {1, 2, -9, 6}, {-6, 7, 7, -9}});

  EXPECT_TRUE(eql(A.cofactor(0, 0), 690));
  EXPECT_TRUE(eql(A.cofactor(0, 1), 447));
  EXPECT_TRUE(eql(A.cofactor(0, 2), 210));
  EXPECT_TRUE(eql(A.cofactor(0, 3), 51));
  EXPECT_TRUE(eql(A.det(), -4071));
}

TEST(Matrix, TestingAnInvertibleMatrixForInvertibility) {
  Matrix<4> A({{6, 4, 4, 4}, {5, 5, 7, 6}, {4, -9, 3, -7}, {9, 1, 7, -6}});

  EXPECT_TRUE(eql(A.det(), -2120));
}

TEST(Matrix, CalculatingTheInverseOfAMatrix) {
  Matrix<4> A({{-5, 2, 6, -8}, {1, -5, 1, 8}, {7, 7, -6, -7}, {1, -3, 7, 4}});
  Matrix<4> B = A.inverse();

  EXPECT_TRUE(eql(A.det(), 532));
  EXPECT_TRUE(eql(A.cofactor(2, 3), -160));
  EXPECT_TRUE(eql(B(3, 2), -160.0f / 532.0f));
  EXPECT_TRUE(eql(A.cofactor(3, 2), 105));
  EXPECT_TRUE(eql(B(2, 3), 105.0f / 532.0f));
  EXPECT_EQ(B, Matrix<4>({{0.21805f, 0.45113f, 0.24060f, -0.04511f},
                          {-0.80827f, -1.45677f, -0.44361f, 0.52068f},
                          {-0.07895f, -0.22368f, -0.05263f, 0.19737f},
                          {-0.52256f, -0.81391f, -0.30075f, 0.30639f}}));
}

TEST(Matrix, CalculatingTheInverseOfAnotherMatrix) {
  Matrix<4> A({{8, -5, 9, 2}, {7, 5, 6, 1}, {-6, 0, 9, 6}, {-3, 0, -9, -4}});
  Matrix<4> B = A.inverse();

  EXPECT_EQ(B, Matrix<4>({{-0.15385f, -0.15385f, -0.28205f, -0.53846f},
                          {-0.07692f, 0.12308f, 0.02564f, 0.03077f},
                          {0.35897f, 0.35897f, 0.43590f, 0.92308f},
                          {-0.69231f, -0.69231f, -0.76923f, -1.92308f}}));
}

TEST(Matrix, MultiplingAProductByItsInverse) {
  Matrix<4> A({{3, -9, 7, 3}, {3, -8, 2, -9}, {-4, 4, 4, 1}, {-6, 5, -1, 1}});
  Matrix<4> B({{8, 2, 2, 2}, {3, -1, 7, 0}, {7, 0, 5, 4}, {6, -2, 0, 5}});
  Matrix<4> C = mul(A, B);

  EXPECT_EQ(mul(C, B.inverse()), A);
}
