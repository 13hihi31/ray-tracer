#include <gtest/gtest.h>

#include <cmath>
#include <raytracer/helper_functions.hpp>
#include <raytracer/matrix.hpp>
#include <raytracer/ray.hpp>
#include <raytracer/sphere.hpp>
#include <raytracer/transforms.hpp>
#include <raytracer/tuple.hpp>
#include <vector>

TEST(Sphere, ARayIntersectsASphereAtTwoPoints) {
  Ray ray(point(0, 0, -5), vec(0, 0, 1));
  Sphere sphere;
  std::vector<Intersection> intersections;
  sphere.intersect(ray, intersections);
  std::sort(intersections.begin(), intersections.end());

  EXPECT_EQ(intersections.size(), 2);
  EXPECT_EQ(intersections[0].t, 4.0f);
  EXPECT_EQ(intersections[1].t, 6.0f);
}

TEST(Sphere, ARayIntersectsASphereAtATangent) {
  Ray ray(point(0, 1, -5), vec(0, 0, 1));
  Sphere sphere;
  std::vector<Intersection> intersections;
  sphere.intersect(ray, intersections);
  std::sort(intersections.begin(), intersections.end());

  EXPECT_EQ(intersections.size(), 2);
  EXPECT_TRUE(eql(intersections[0].t, 5.0f));
  EXPECT_TRUE(eql(intersections[1].t, 5.0f));
}

TEST(Sphere, ARayMissesASphere) {
  Ray ray(point(0, 2, -5), vec(0, 0, 1));
  Sphere sphere;
  std::vector<Intersection> intersections;
  sphere.intersect(ray, intersections);

  ASSERT_EQ(intersections.size(), 0);
}

TEST(Sphere, ARayOriginatesInsideASphere) {
  Ray ray(point(0, 0, 0), vec(0, 0, 1));
  Sphere sphere;
  std::vector<Intersection> intersections;
  sphere.intersect(ray, intersections);
  std::sort(intersections.begin(), intersections.end());

  EXPECT_TRUE(eql(intersections[0].t, -1.0f));
  EXPECT_TRUE(eql(intersections[1].t, 1.0f));
}

TEST(Sphere, ASphereIsBehindARay) {
  Ray ray(point(0, 0, 5), vec(0, 0, 1));
  Sphere sphere;
  std::vector<Intersection> intersections;
  sphere.intersect(ray, intersections);
  std::sort(intersections.begin(), intersections.end());

  EXPECT_EQ(intersections.size(), 2);
  EXPECT_TRUE(eql(intersections[0].t, -6.0f));
  EXPECT_TRUE(eql(intersections[1].t, -4.0f));
}

TEST(Sphere, SpheresAreEqualIfTheyAreTheSameObjects) {
  Sphere sphere1;
  Sphere &sphere2 = sphere1;

  EXPECT_TRUE(sphere1.eq(sphere2));
}

TEST(Sphere, IntersectSetsTheObjectOnTheIntersection) {
  Ray ray(point(0, 0, -5), vec(0, 0, 1));
  Sphere sphere;
  std::vector<Intersection> intersections;
  sphere.intersect(ray, intersections);
  std::sort(intersections.begin(), intersections.end());

  EXPECT_EQ(intersections.size(), 2);
  EXPECT_TRUE(intersections[0].object->eq(sphere));
  EXPECT_TRUE(intersections[1].object->eq(sphere));
}

TEST(Sphere, IntersectiongAScaledSphereWithARay) {
  Ray ray(point(0, 0, -5), vec(0, 0, 1));
  Sphere sphere;
  sphere.set_transform(scaling(2, 2, 2));
  std::vector<Intersection> intersections;
  sphere.intersect(ray, intersections);
  std::sort(intersections.begin(), intersections.end());

  EXPECT_EQ(intersections.size(), 2);
  EXPECT_EQ(intersections[0].t, 3);
  EXPECT_EQ(intersections[1].t, 7);
}

TEST(Sphere, IntersectingATranslatedSphereWithARay) {
  Ray ray(point(0, 0, -5), vec(0, 0, 1));
  Sphere sphere;
  sphere.set_transform(translation(5, 0, 0));
  std::vector<Intersection> intersections;
  sphere.intersect(ray, intersections);

  EXPECT_TRUE(intersections.empty());
}

TEST(Sphere, TheNormalOnASphereAtAPointOnTheXAxis) {
  Sphere sphere;
  Intersection dummy_intersection(0, &sphere);

  EXPECT_EQ(sphere.normal_at(point(1, 0, 0), dummy_intersection), vec(1, 0, 0));
}

TEST(Sphere, TheNormalOnASphereAtAPointOnTheYAxis) {
  Sphere sphere;
  Intersection dummy_intersection(0, &sphere);

  EXPECT_EQ(sphere.normal_at(point(0, 1, 0), dummy_intersection), vec(0, 1, 0));
}

TEST(Sphere, TheNormalOnASphereAtAPointOnTheZAxis) {
  Sphere sphere;
  Intersection dummy_intersection(0, &sphere);

  EXPECT_EQ(sphere.normal_at(point(0, 0, 1), dummy_intersection), vec(0, 0, 1));
}

TEST(Sphere, TheNormalOnASphereAtANonaxialPoint) {
  Sphere sphere;
  Intersection dummy_intersection(0, &sphere);

  EXPECT_EQ(sphere.normal_at(point(sqrtf(3.0f) / 3.0f, sqrtf(3.0f) / 3.0f,
                                   sqrtf(3.0f) / 3.0f),
                             dummy_intersection),
            vec(sqrtf(3.0f) / 3.0f, sqrtf(3.0f) / 3.0f, sqrtf(3.0f) / 3.0f));
}

TEST(Sphere, TheNormalIsANormalizedVector) {
  Sphere sphere;
  Intersection dummy_intersection(0, &sphere);
  Vec normal = sphere.normal_at(
      point(sqrtf(3.0f) / 3.0f, sqrtf(3.0f) / 3.0f, sqrtf(3.0f) / 3.0f),
      dummy_intersection);

  EXPECT_EQ(normal, norm(normal));
}

TEST(Sphere, ASphereHasABoundingBox) {
  Sphere sphere;
  BoundingBox sphere_bounding_box = sphere.get_bounds();

  EXPECT_EQ(sphere_bounding_box.min_point, point(-1, -1, -1));
  EXPECT_EQ(sphere_bounding_box.max_point, point(1, 1, 1));
}
