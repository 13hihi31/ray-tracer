#include <gtest/gtest.h>

#include <raytracer/triangle.hpp>
#include <raytracer/tuple.hpp>

TEST(Triangle, ConstructingATriangle) {
  Point point1 = point(0, 1, 0);
  Point point2 = point(-1, 0, 0);
  Point point3 = point(1, 0, 0);
  Triangle triangle(point1, point2, point3);

  EXPECT_EQ(triangle.p1, point1);
  EXPECT_EQ(triangle.p2, point2);
  EXPECT_EQ(triangle.p3, point3);
  EXPECT_EQ(triangle.e1, vec(-1, -1, 0));
  EXPECT_EQ(triangle.e2, vec(1, -1, 0));
  EXPECT_EQ(triangle.normal, vec(0, 0, -1));
}

TEST(Triangle, FindingTheNormalOnATriangle) {
  Triangle triangle(point(0, 1, 0), point(-1, 0, 0), point(1, 0, 0));
  Intersection dummy_intersection(0, &triangle);
  Vec normal1 = triangle.local_normal_at(point(0, 0.5, 0), dummy_intersection);
  Vec normal2 =
      triangle.local_normal_at(point(-0.5, 0 - 75, 0), dummy_intersection);
  Vec normal3 =
      triangle.local_normal_at(point(0.5, 0.25, 0), dummy_intersection);

  EXPECT_EQ(normal1, triangle.normal);
  EXPECT_EQ(normal2, triangle.normal);
  EXPECT_EQ(normal3, triangle.normal);
}

TEST(Triangle, IntersectingARayParallelToTheTriangle) {
  Triangle triangle(point(0, 1, 0), point(-1, 0, 0), point(1, 0, 0));
  Ray ray(point(0, -1, -2), vec(0, 1, 0));
  std::vector<Intersection> intersections;
  triangle.intersect(ray, intersections);

  EXPECT_TRUE(intersections.empty());
}

TEST(Triangle, ARayMissesTheP1P3Edge) {
  Triangle triangle(point(0, 1, 0), point(-1, 0, 0), point(1, 0, 0));
  Ray ray(point(1, 1, -2), vec(0, 0, 1));
  std::vector<Intersection> intersections;
  triangle.intersect(ray, intersections);

  EXPECT_TRUE(intersections.empty());
}

TEST(Triangle, ARayMissesTheP1P2Edge) {
  Triangle triangle(point(0, 1, 0), point(-1, 0, 0), point(1, 0, 0));
  Ray ray(point(-1, 1, -2), vec(0, 0, 1));
  std::vector<Intersection> intersections;
  triangle.intersect(ray, intersections);

  EXPECT_TRUE(intersections.empty());
}

TEST(Triangle, ARayMissesTheP2P3Edge) {
  Triangle triangle(point(0, 1, 0), point(-1, 0, 0), point(1, 0, 0));
  Ray ray(point(0, -1, -2), vec(0, 0, 1));
  std::vector<Intersection> intersections;
  triangle.intersect(ray, intersections);

  EXPECT_TRUE(intersections.empty());
}

TEST(Triangle, ARayStrikesATriangle) {
  Triangle triangle(point(0, 1, 0), point(-1, 0, 0), point(1, 0, 0));
  Ray ray(point(0, 0.5, -2), vec(0, 0, 1));
  std::vector<Intersection> intersections;
  triangle.intersect(ray, intersections);

  ASSERT_EQ(intersections.size(), 1);
  EXPECT_EQ(intersections[0].t, 2);
}

TEST(Triangle, ATriangleHasABoundingBox) {
  Triangle triangle(point(-3, 7, 2), point(6, 2, -4), point(2, -1, -1));
  BoundingBox triangle_bounding_box = triangle.get_bounds();

  EXPECT_EQ(triangle_bounding_box.min_point, point(-3, -1, -4));
  EXPECT_EQ(triangle_bounding_box.max_point, point(6, 7, 2));
}
