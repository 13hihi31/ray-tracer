#include <gtest/gtest.h>

#include <raytracer/string_operations.hpp>

TEST(StringOperations, SplitStringBySpaces) {
  std::string str = "aaa bbb ccc ddd";

  std::string delim_str = " ";
  std::vector<std::string> tokens = string_operations::split(str, delim_str);

  ASSERT_EQ(tokens.size(), 4);
  EXPECT_EQ(tokens[0], "aaa");
  EXPECT_EQ(tokens[1], "bbb");
  EXPECT_EQ(tokens[2], "ccc");
  EXPECT_EQ(tokens[3], "ddd");
}

TEST(StringOperations, RemoveExtraSpaces) {
  std::string str = "aaa  bbb   ccc";
  string_operations::remove_double_spaces(str);

  EXPECT_EQ(str, "aaa bbb ccc");
}

TEST(StringOperations, RemoveCharacterFromString) {
  std::string str = "aa\ta\t";
  string_operations::remove_character(str, '\t');

  EXPECT_EQ(str, "aaa");
}

TEST(StringOperations, StringIsNumericType) {
  EXPECT_TRUE(string_operations::is_numeric_type<int>("-123"));
  EXPECT_FALSE(string_operations::is_numeric_type<int>("1.234"));
  EXPECT_FALSE(string_operations::is_numeric_type<int>("0.0"));

  EXPECT_TRUE(string_operations::is_numeric_type<float>("1.234"));
  EXPECT_TRUE(string_operations::is_numeric_type<float>("123"));
  EXPECT_TRUE(string_operations::is_numeric_type<float>("-1"));
  EXPECT_TRUE(string_operations::is_numeric_type<float>("0.0000"));
  EXPECT_TRUE(string_operations::is_numeric_type<float>("12.0000"));
}
