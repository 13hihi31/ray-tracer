#include <gtest/gtest.h>

#include <limits>
#include <raytracer/cylinder.hpp>
#include <raytracer/helper_functions.hpp>
#include <raytracer/intersection.hpp>
#include <raytracer/ray.hpp>
#include <raytracer/shape.hpp>
#include <raytracer/tuple.hpp>
#include <vector>

TEST(Cylinder, ARayMissesACylinder) {
  Cylinder cylinder;

  std::vector<Vec> directions({vec(0, 1, 0), vec(0, 1, 0), vec(1, 1, 1)});
  std::vector<Point> origins({point(1, 0, 0), point(0, 0, 0), point(0, 0, -5)});

  ASSERT_EQ(directions.size(), origins.size());

  for (size_t i = 0; i < directions.size(); ++i) {
    Ray ray(origins[i], norm(directions[i]));
    std::vector<Intersection> intersections;
    cylinder.intersect(ray, intersections);

    EXPECT_EQ(intersections.size(), 0);
  }
}

TEST(Cylinder, ARayStrikesACylinder) {
  Cylinder cylinder;

  std::vector<Point> origin(
      {point(1, 0, -5), point(0, 0, -5), point(0.5, 0, -5)});
  std::vector<Vec> directions({vec(0, 0, 1), vec(0, 0, 1), vec(0.1, 1, 1)});
  std::vector<float> intersection0_t({5, 4, 6.80798});
  std::vector<float> intersection1_t({5, 6, 7.08872});

  for (size_t i = 0; i < origin.size(); ++i) {
    Ray ray(origin[i], norm(directions[i]));
    std::vector<Intersection> intersections;
    cylinder.intersect(ray, intersections);
    std::sort(intersections.begin(), intersections.end());

    ASSERT_EQ(intersections.size(), 2);
    EXPECT_TRUE(eql(intersections[0].t, intersection0_t[i], 0.0001f));
    EXPECT_TRUE(eql(intersections[1].t, intersection1_t[i], 0.0001f));
  }
}

TEST(Cylinder, NormalVectorOnACylinder) {
  Cylinder cylinder;

  std::vector<Point> points(
      {point(1, 0, 0), point(0, 5, -1), point(0, -2, 1), point(-1, 1, 0)});
  std::vector<Vec> normal(
      {vec(1, 0, 0), vec(0, 0, -1), vec(0, 0, 1), vec(-1, 0, 0)});
  Intersection dummy_intersection(0, &cylinder);

  for (size_t i = 0; i < normal.size(); ++i) {
    EXPECT_EQ(cylinder.local_normal_at(points[i], dummy_intersection),
              normal[i]);
  }
}

TEST(Cylinder, TheDefaultMinimumAndMaxiumuForACylinder) {
  Cylinder cylinder;

  EXPECT_EQ(cylinder.maximum, std::numeric_limits<float>::max());
  EXPECT_EQ(cylinder.minimum, std::numeric_limits<float>::lowest());
}

TEST(Cylinder, IntersectingAConstrainedCylinder) {
  Cylinder cylinder;
  cylinder.minimum = 1.0f;
  cylinder.maximum = 2.0f;

  std::vector<Point> points({point(0, 1.5, 0), point(0, 3, -5), point(0, 0, -5),
                             point(0, 2, -5), point(0, 1, -5),
                             point(0, 1.5, -2)});
  std::vector<Vec> directions({vec(0.1, 1, 0), vec(0, 0, 1), vec(0, 0, 1),
                               vec(0, 0, 1), vec(0, 0, 1), vec(0, 0, 1)});
  std::vector<size_t> sizes({0, 0, 0, 0, 0, 2});

  for (size_t i = 0; i < points.size(); ++i) {
    Ray ray(points[i], norm(directions[i]));
    std::vector<Intersection> intersections;
    cylinder.intersect(ray, intersections);

    EXPECT_EQ(intersections.size(), sizes[i]);
  }
}

TEST(Cylinder, TheDefaultClosedValueForACylinder) {
  Cylinder cylinder;

  EXPECT_FALSE(cylinder.closed);
}

TEST(Cylinder, IntersectingTheCapsOfAClosedCylinder) {
  Cylinder cylinder;
  cylinder.minimum = 1.0f;
  cylinder.maximum = 2.0f;
  cylinder.closed = true;

  std::vector<Point> points({point(0, 3, 0), point(0, 3, -2), point(0, 4, -2),
                             point(0, 0, -2), point(0, -1, -2)});
  std::vector<Vec> directions({vec(0, -1, 0), vec(0, -1, 2), vec(0, -1, 1),
                               vec(0, 1, 2), vec(0, 1, 1)});
  std::vector<size_t> sizes({2, 2, 2, 2, 2});

  for (size_t i = 0; i < points.size(); ++i) {
    Ray ray(points[i], norm(directions[i]));
    std::vector<Intersection> intersections;
    cylinder.intersect(ray, intersections);

    EXPECT_EQ(intersections.size(), sizes[i]);
  }
}

TEST(Cylinder, TheNormalVectorOnACylindersEndCaps) {
  Cylinder cylinder;
  cylinder.minimum = 1.0f;
  cylinder.maximum = 2.0f;
  cylinder.closed = true;

  std::vector<Point> points({point(0, 1, 0), point(0.5, 1, 0), point(0, 1, 0.5),
                             point(0, 2, 0), point(0.5, 2, 0),
                             point(0, 2, 0.5)});
  std::vector<Vec> normals({vec(0, -1, 0), vec(0, -1, 0), vec(0, -1, 0),
                            vec(0, 1, 0), vec(0, 1, 0), vec(0, 1, 0)});
  Intersection dummy_intersection(0, &cylinder);

  for (size_t i = 0; i < points.size(); ++i) {
    EXPECT_EQ(cylinder.local_normal_at(points[i], dummy_intersection),
              normals[i]);
  }
}

TEST(Cylinder, AnUnboundedCylinderHasABoundingBox) {
  Cylinder cylinder;

  BoundingBox cylinder_bounding_box = cylinder.get_bounds();

  EXPECT_EQ(cylinder_bounding_box.min_point,
            point(-1, std::numeric_limits<float>::lowest(), -1));
  EXPECT_EQ(cylinder_bounding_box.max_point,
            point(1, std::numeric_limits<float>::max(), 1));
}

TEST(Cylinder, AnBoundedCylinderHasABoundingBox) {
  Cylinder cylinder;
  cylinder.minimum = -5;
  cylinder.maximum = 3;

  BoundingBox cylinder_bounding_box = cylinder.get_bounds();

  EXPECT_EQ(cylinder_bounding_box.min_point, point(-1, -5, -1));
  EXPECT_EQ(cylinder_bounding_box.max_point, point(1, 3, 1));
}
