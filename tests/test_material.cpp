#include <gtest/gtest.h>

#include <raytracer/helper_functions.hpp>
#include <raytracer/material.hpp>
#include <raytracer/pattern.hpp>
#include <raytracer/sphere.hpp>

TEST(Material, TheDefaultMaterial) {
  Material material;

  EXPECT_EQ(material.color, color(1, 1, 1));
  EXPECT_TRUE(eql(material.ambient, 0.1));
  EXPECT_TRUE(eql(material.diffuse, 0.9));
  EXPECT_TRUE(eql(material.specular, 0.9));
  EXPECT_TRUE(eql(material.shininess, 200.0));
}

TEST(Material, LightingWithTheEyeBetweenTheLightAndTheSurface) {
  Material material;
  Point position = point(0, 0, 0);
  Vec eyev = vec(0, 0, -1);
  Vec normalv = vec(0, 0, -1);
  Light light(point(0, 0, -10), color(1, 1, 1));
  Sphere sphere;

  EXPECT_EQ(material.lighting(&sphere, light, position, eyev, normalv),
            color(1.9, 1.9, 1.9));
}

TEST(Material, LightingWithTheEyeBetweenLightAndSurfaceEyeOffset45Deg) {
  Material material;
  Point position = point(0, 0, 0);
  Vec eyev = vec(0, sqrtf(2.0f) / 2.0f, -sqrtf(2.0f) / 2.0f);
  Vec normalv = vec(0, 0, -1);
  Light light(point(0, 0, -10), color(1, 1, 1));
  Sphere sphere;

  EXPECT_EQ(material.lighting(&sphere, light, position, eyev, normalv),
            color(1, 1, 1));
}

TEST(Material, LightingWithEyeOppositeSurfaceLightOffset45Deg) {
  Material material;
  Point position = point(0, 0, 0);
  Vec eyev = vec(0, 0, -1);
  Vec normalv = vec(0, 0, -1);
  Light light(point(0, 10, -10), color(1, 1, 1));
  Sphere sphere;

  EXPECT_EQ(material.lighting(&sphere, light, position, eyev, normalv),
            color(0.7364, 0.7364, 0.7364));
}

TEST(Material, LightingWithEyeInThePathOfTheReflectionVector) {
  Material material;
  Point position = point(0, 0, 0);
  Vec eyev = vec(0, -sqrtf(2.0f) / 2.0f, -sqrtf(2.0f) / 2.0f);
  Vec normalv = vec(0, 0, -1);
  Light light(point(0, 10, -10), color(1, 1, 1));
  Sphere sphere;

  EXPECT_EQ(material.lighting(&sphere, light, position, eyev, normalv),
            color(1.63639, 1.63639, 1.63639));
}

TEST(Material, LightingWithTheLightBehindTheSurface) {
  Material material;
  Point position = point(0, 0, 0);
  Vec eyev = vec(0, 0, -1);
  Vec normalv = vec(0, 0, -1);
  Light light(point(0, 0, 10), color(1, 1, 1));
  Sphere sphere;

  EXPECT_EQ(material.lighting(&sphere, light, position, eyev, normalv),
            color(0.1, 0.1, 0.1));
}

TEST(Material, LightingWithTheSurfaceInShadow) {
  Material material;
  Point position = point(0, 0, 0);
  Vec eyev = vec(0, 0, -1);
  Vec normalv = vec(0, 0, -1);
  Light light(point(0, 0, -10), color(1, 1, 1));
  bool in_shadow = true;
  Sphere sphere;

  EXPECT_EQ(
      material.lighting(&sphere, light, position, eyev, normalv, in_shadow),
      color(0.1, 0.1, 0.1));
}

TEST(Material, LightingWithAPatternApplied) {
  Material material;
  StripePattern stripe_pattern(color(1, 1, 1), color(0, 0, 0));
  material.pattern = stripe_pattern;
  material.ambient = 1;
  material.diffuse = 0;
  material.specular = 0;

  Vec eyev = vec(0, 0, -1);
  Vec normalv = vec(0, 0, -1);

  Light light(point(0, 0, -10), color(1, 1, 1));

  Sphere sphere;

  Color color1 =
      material.lighting(&sphere, light, point(0.9, 0, 0), eyev, normalv, false);
  Color color2 =
      material.lighting(&sphere, light, point(1.1, 0, 0), eyev, normalv, false);

  EXPECT_EQ(color1, color(1, 1, 1));
  EXPECT_EQ(color2, color(0, 0, 0));
}

TEST(Material, ReflectivityForTheDefaultMaterial) {
  Material material;

  EXPECT_EQ(material.reflective, 0.0f);
}

TEST(Material, TransparencyAndRefractiveIndexForTheDefaultMaterial) {
  Material material;

  EXPECT_EQ(material.transparency, 0.0f);
  EXPECT_EQ(material.refractive_index, 1.0f);
}
