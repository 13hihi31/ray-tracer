#include <gtest/gtest.h>

#include <cmath>
#include <raytracer/group.hpp>
#include <raytracer/matrix.hpp>
#include <raytracer/shape.hpp>
#include <raytracer/sphere.hpp>
#include <raytracer/test_shape.hpp>
#include <raytracer/transforms.hpp>

TEST(Shape, TheDefaultTransformation) {
  auto test_shape = TestShape();

  EXPECT_EQ(test_shape.transform_inverse, identity_matrix<4>());
}

TEST(Shape, AssigningATransformation) {
  auto test_shape = TestShape();
  test_shape.set_transform(translation(2, 3, 4));

  EXPECT_EQ(test_shape.transform_inverse, translation(2, 3, 4).inverse());
}

TEST(Shape, TheDefaultMaterial) {
  auto test_shape = TestShape();

  EXPECT_EQ(test_shape.material, Material());
}

TEST(Shape, AssigningAMaterial) {
  auto test_shape = TestShape();
  Material material;
  material.ambient = 1.0f;
  test_shape.material = material;

  EXPECT_EQ(test_shape.material, material);
}

TEST(Shape, ComputingTheNormalOnATranslatedShape) {
  auto test_shape = TestShape();
  test_shape.set_transform(translation(0, 1, 0));
  Intersection dummy_intersection(0, &test_shape);

  EXPECT_EQ(
      test_shape.normal_at(point(0, 1.70711, -0.70711), dummy_intersection),
      vec(0, 0.70711, -0.70711));
}

TEST(Shape, ComputingTheNormalOnATransformedShape) {
  auto test_shape = TestShape();
  test_shape.set_transform(mul(scaling(1, 0.5, 1), rotation_z(M_PI / 5.0f)));
  Intersection dummy_intersection(0, &test_shape);

  EXPECT_EQ(
      test_shape.normal_at(point(0, sqrtf(2.0f) / 2.0f, -sqrtf(2.0f) / 2.0f),
                           dummy_intersection),
      vec(0, 0.97014, -0.24254));
}

TEST(Shape, AShapeHasAParentAttribute) {
  auto test_shape = TestShape();

  EXPECT_EQ(test_shape.parent, nullptr);
}

TEST(Shape, ConvertingAPointFromWorldToObjectSpace) {
  Group group1;
  group1.set_transform(rotation_y(M_PI / 2.0));

  Group group2;
  group2.set_transform(scaling(2, 2, 2));

  Sphere sphere;
  sphere.set_transform(translation(5, 0, 0));

  group2.add_child(std::move(sphere));
  group1.add_child(std::move(group2));

  auto &group1_group_0_spheres = group1.get<Group>()[0].get<Sphere>();
  Point p = group1_group_0_spheres[0].world_to_object(point(-2, 0, -10));

  EXPECT_EQ(p, point(0, 0, -1));
}

TEST(Shape, ConvertingANormalFromObjectToWorldSpace) {
  Group group1;
  group1.set_transform(rotation_y(M_PI / 2.0));

  Group group2;
  group2.set_transform(scaling(1, 2, 3));

  Sphere sphere;
  sphere.set_transform(translation(5, 0, 0));

  group2.add_child(std::move(sphere));
  group1.add_child(std::move(group2));

  auto &group1_group_0_spheres = group1.get<Group>()[0].get<Sphere>();
  Vec normal = group1_group_0_spheres[0].normal_to_world(
      vec(sqrtf(3.0f) / 3.0f, sqrtf(3.0f) / 3.0f, sqrtf(3.0f) / 3.0f));

  EXPECT_EQ(normal, vec(0.285714, 0.428571, -0.857143));
}

TEST(Shape, FindingTheNormalOnAChildObject) {
  Group group1;
  group1.set_transform(rotation_y(M_PI / 2.0));

  Group group2;
  group2.set_transform(scaling(1, 2, 3));

  Sphere sphere;
  sphere.set_transform(translation(5, 0, 0));

  group2.add_child(std::move(sphere));
  group1.add_child(std::move(group2));

  Intersection dummy_intersection(0, &sphere);
  Vec normal =
      sphere.normal_at(point(1.7321, 1.1547, -5.5774), dummy_intersection);

  EXPECT_EQ(normal, vec(-0.497657, 0.175845, -0.849362));
  // EXPECT_EQ(normal, vec(0.2857, 0.4286, -0.8571));
}

TEST(Shape, AShapeIncludesItSelf) {
  Sphere sphere;

  EXPECT_TRUE(sphere.includes(&sphere));
}

TEST(Shape, TestShapeHasArbitraryBounds) {
  TestShape test_shape;
  BoundingBox test_shape_box = test_shape.get_bounds();

  EXPECT_EQ(test_shape_box.min_point, point(-1, -1, -1));
  EXPECT_EQ(test_shape_box.max_point, point(1, 1, 1));
}

TEST(Shape, QueryingAShapesBoundingBoxInItsParentsSpace) {
  Sphere sphere;
  sphere.set_transform(mul(translation(1, -3, 5), scaling(0.5, 2, 4)));
  BoundingBox sphere_box_in_parent_space = sphere.parent_space_bounds();

  ASSERT_EQ(sphere_box_in_parent_space.min_point, point(0.5, -5, 1));
  ASSERT_EQ(sphere_box_in_parent_space.max_point, point(1.5, -1, 9));
}
