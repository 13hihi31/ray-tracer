#include <gtest/gtest.h>

#include <raytracer/obj_parser.hpp>
#include <raytracer/smooth_triangle.hpp>
#include <raytracer/triangle.hpp>
#include <string>
#include <vector>

TEST(OBJParser, IgnoringUnrecognizedLines) {
  std::string gibberish =
      "There was a young lady named Bright\n"
      "who traveled much faster than light.\n"
      "She set out one day\n"
      "in a relative way\n"
      "and came back the previous night.\n";

  OBJParser obj_parser;
  obj_parser.parse(gibberish);

  EXPECT_EQ(obj_parser.ignored_lines, 5);
}

TEST(OBJParser, VertexRecords) {
  std::string file =
      "v -1 1 0\n"
      "v -1.0000 0.5000 0.0000\n"
      "v 1 0 0\n"
      "v 1 1 0\n";

  OBJParser obj_parser;
  obj_parser.parse(file);

  ASSERT_EQ(obj_parser.vertices.size(), 5);
  EXPECT_EQ(obj_parser.vertices[1], point(-1, 1, 0));
  EXPECT_EQ(obj_parser.vertices[2], point(-1, 0.5, 0));
  EXPECT_EQ(obj_parser.vertices[3], point(1, 0, 0));
  EXPECT_EQ(obj_parser.vertices[4], point(1, 1, 0));
}

TEST(OBJParser, ParsingTriangleFaces) {
  std::string file =
      "v -1 1 0\n"
      "v -1 0 0\n"
      "v 1 0 0\n"
      "v 1 1 0\n"
      "\n"
      "f 1 2 3\n"
      "f 1 3 4\n";

  OBJParser obj_parser;
  obj_parser.parse(file);

  Group &default_group = obj_parser.default_group;
  auto &triangles = default_group.get<Triangle>();

  Triangle &t1 = triangles[0];
  EXPECT_EQ(t1.p1, obj_parser.vertices[1]);
  EXPECT_EQ(t1.p2, obj_parser.vertices[2]);
  EXPECT_EQ(t1.p3, obj_parser.vertices[3]);

  Triangle &t2 = triangles[1];
  EXPECT_EQ(t2.p1, obj_parser.vertices[1]);
  EXPECT_EQ(t2.p2, obj_parser.vertices[3]);
  EXPECT_EQ(t2.p3, obj_parser.vertices[4]);
}

TEST(OBJParser, TriangulatingPolygons) {
  std::string file =
      "v -1 1 0\n"
      "v -1 0 0\n"
      "v 1 0 0\n"
      "v 1 1 0\n"
      "v 0 2 0\n"
      "\n"
      "f 1 2 3 4 5\n";

  OBJParser obj_parser;
  obj_parser.parse(file);

  Group &default_group = obj_parser.default_group;
  auto &triangles = default_group.get<Triangle>();

  ASSERT_EQ(triangles.size(), 3);

  Triangle &t1 = triangles[0];
  EXPECT_EQ(t1.p1, obj_parser.vertices[1]);
  EXPECT_EQ(t1.p2, obj_parser.vertices[2]);
  EXPECT_EQ(t1.p3, obj_parser.vertices[3]);

  Triangle &t2 = triangles[1];
  EXPECT_EQ(t2.p1, obj_parser.vertices[1]);
  EXPECT_EQ(t2.p2, obj_parser.vertices[3]);
  EXPECT_EQ(t2.p3, obj_parser.vertices[4]);

  Triangle &t3 = triangles[2];
  EXPECT_EQ(t3.p1, obj_parser.vertices[1]);
  EXPECT_EQ(t3.p2, obj_parser.vertices[4]);
  EXPECT_EQ(t3.p3, obj_parser.vertices[5]);
}

TEST(OBJParser, TrianglesInGroups) {
  std::string file =
      "v -1 1 0\n"
      "v -1 0 0\n"
      "v 1 0 0\n"
      "v 1 1 0\n"
      "\n"
      "g first_group\n"
      "f 1 2 3\n"
      "g second_group\n"
      "f 1 3 4\n";

  OBJParser obj_parser;
  obj_parser.parse(file);

  ASSERT_NE(obj_parser.groups.find("first_group"), obj_parser.groups.end());
  ASSERT_NE(obj_parser.groups.find("second_group"), obj_parser.groups.end());

  Group &first_group = obj_parser.groups["first_group"];
  Group &second_group = obj_parser.groups["second_group"];

  auto &first_group_triangles = first_group.get<Triangle>();
  auto &second_group_triangles = second_group.get<Triangle>();

  ASSERT_EQ(first_group_triangles.size(), 1);
  ASSERT_EQ(second_group_triangles.size(), 1);

  ASSERT_EQ(obj_parser.vertices.size(), 5);

  Triangle &t1 = first_group_triangles[0];
  EXPECT_EQ(t1.p1, obj_parser.vertices[1]);
  EXPECT_EQ(t1.p2, obj_parser.vertices[2]);
  EXPECT_EQ(t1.p3, obj_parser.vertices[3]);

  Triangle &t2 = second_group_triangles[0];
  EXPECT_EQ(t2.p1, obj_parser.vertices[1]);
  EXPECT_EQ(t2.p2, obj_parser.vertices[3]);
  EXPECT_EQ(t2.p3, obj_parser.vertices[4]);
}

TEST(OBJParser, ConvertingAnOBJFileToAGroup) {
  std::string file =
      "v -1 1 0\n"
      "v -1 0 0\n"
      "v 1 0 0\n"
      "v 1 1 0\n"
      "\n"
      "g first_group\n"
      "f 1 2 3\n"
      "g second_group\n"
      "f 1 3 4\n";

  OBJParser obj_parser;
  obj_parser.parse(file);

  Group g = obj_parser.to_group();
  auto &group_groups = g.get<Group>();

  EXPECT_EQ(group_groups.size(), 2);
}

TEST(OBJParser, VertexNormalRecords) {
  std::string file =
      "vn 0 0 1\n"
      "vn 0.707 0 -0.707\n"
      "vn 1 2 3\n";

  OBJParser obj_parser;
  obj_parser.parse(file);

  ASSERT_EQ(obj_parser.normals.size(), 4);
  ASSERT_EQ(obj_parser.normals[1], vec(0, 0, 1));
  ASSERT_EQ(obj_parser.normals[2], vec(0.707, 0, -0.707));
  ASSERT_EQ(obj_parser.normals[3], vec(1, 2, 3));
}

TEST(OBJParser, FacesWithNormals) {
  std::string file =
      "v 0 1 0\n"
      "v -1 0 0\n"
      "v 1 0 0\n"
      "\n"
      "vn -1 0 0\n"
      "vn 1 0 0\n"
      "vn 0 1 0\n"
      "\n"
      "f 1//3 2//1 3//2\n"
      "f 1/77/3 2/77/1 3/77/2\n";

  OBJParser obj_parser;
  obj_parser.parse(file);

  Group &default_group = obj_parser.default_group;
  auto &default_group_smooth_triangles = default_group.get<SmoothTriangle>();

  ASSERT_EQ(default_group_smooth_triangles.size(), 2);

  SmoothTriangle &t = default_group_smooth_triangles[0];

  ASSERT_EQ(t.p1, obj_parser.vertices[1]);
  ASSERT_EQ(t.p2, obj_parser.vertices[2]);
  ASSERT_EQ(t.p3, obj_parser.vertices[3]);
  ASSERT_EQ(t.n1, obj_parser.normals[3]);
  ASSERT_EQ(t.n2, obj_parser.normals[1]);
  ASSERT_EQ(t.n3, obj_parser.normals[2]);
}

TEST(OBJParser, TriangulatingPolygonsWithNormals) {
  std::string file =
      "v -1 1 0\n"
      "v -1 0 0\n"
      "v 1 0 0\n"
      "v 1 1 0\n"
      "v 0 2 0\n"
      "\n"
      "vn -1 0 0\n"
      "vn 1 0 0\n"
      "vn 0 1 0\n"
      "vn -1 1 0\n"
      "vn 0 0 1\n"
      "\n"
      "f 1//1 2//2 3//3 4//4 5//5\n";

  OBJParser obj_parser;
  obj_parser.parse(file);

  Group &default_group = obj_parser.default_group;
  auto &smooth_triangles = default_group.get<SmoothTriangle>();

  ASSERT_EQ(smooth_triangles.size(), 3);

  SmoothTriangle &smooth_triangle0 = smooth_triangles[0];
  EXPECT_EQ(smooth_triangle0.p1, obj_parser.vertices[1]);
  EXPECT_EQ(smooth_triangle0.p2, obj_parser.vertices[2]);
  EXPECT_EQ(smooth_triangle0.p3, obj_parser.vertices[3]);

  EXPECT_EQ(smooth_triangle0.n1, obj_parser.normals[1]);
  EXPECT_EQ(smooth_triangle0.n2, obj_parser.normals[2]);
  EXPECT_EQ(smooth_triangle0.n3, obj_parser.normals[3]);

  SmoothTriangle &smooth_triangle1 = smooth_triangles[1];
  EXPECT_EQ(smooth_triangle1.p1, obj_parser.vertices[1]);
  EXPECT_EQ(smooth_triangle1.p2, obj_parser.vertices[3]);
  EXPECT_EQ(smooth_triangle1.p3, obj_parser.vertices[4]);

  EXPECT_EQ(smooth_triangle1.n1, obj_parser.normals[1]);
  EXPECT_EQ(smooth_triangle1.n2, obj_parser.normals[3]);
  EXPECT_EQ(smooth_triangle1.n3, obj_parser.normals[4]);

  SmoothTriangle &smooth_triangle2 = smooth_triangles[2];
  EXPECT_EQ(smooth_triangle2.p1, obj_parser.vertices[1]);
  EXPECT_EQ(smooth_triangle2.p2, obj_parser.vertices[4]);
  EXPECT_EQ(smooth_triangle2.p3, obj_parser.vertices[5]);

  EXPECT_EQ(smooth_triangle2.n1, obj_parser.normals[1]);
  EXPECT_EQ(smooth_triangle2.n2, obj_parser.normals[4]);
  EXPECT_EQ(smooth_triangle2.n3, obj_parser.normals[5]);
}
