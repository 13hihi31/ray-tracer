#include <gtest/gtest.h>

#include <raytracer/group.hpp>
#include <raytracer/matrix.hpp>
#include <raytracer/shape.hpp>
#include <raytracer/transforms.hpp>

TEST(Group, CreatingANewGroup) {
  Group group;

  EXPECT_EQ(group.transform_inverse, identity_matrix<4>());
  EXPECT_TRUE(group.empty());
}

TEST(Group, CopyingConstructingAGroup) {
  Group group1;

  Sphere sphere1;
  group1.add_child(std::move(sphere1));

  Sphere sphere2;
  group1.add_child(std::move(sphere2));

  Group group2(group1);

  auto &spheres = std::get<std::vector<Sphere>>(group2.shapes);

  ASSERT_EQ(spheres.size(), 2);
  EXPECT_EQ(spheres[0].parent, &group2);
  EXPECT_EQ(spheres[1].parent, &group2);
}

TEST(Group, CopyingAGroup) {
  Group group1;

  Sphere sphere1;
  group1.add_child(std::move(sphere1));

  Sphere sphere2;
  group1.add_child(std::move(sphere2));

  Group group2 = group1;

  auto &spheres = std::get<std::vector<Sphere>>(group2.shapes);

  ASSERT_EQ(spheres.size(), 2);
  EXPECT_EQ(spheres[0].parent, &group2);
  EXPECT_EQ(spheres[1].parent, &group2);
}

TEST(Group, AddingAChildToAGroup) {
  Group group;
  Sphere sphere;

  group.add_child(std::move(sphere));
  EXPECT_FALSE(group.empty());

  auto &spheres = std::get<std::vector<Sphere>>(group.shapes);

  ASSERT_EQ(spheres.size(), 1);
  EXPECT_EQ(spheres[0], sphere);
  EXPECT_EQ(spheres[0].parent, &group);
}

TEST(Group, AddingAGroupToAGroup) {
  Group group1;
  Sphere sphere1;

  group1.add_child(std::move(sphere1));

  Group group2;
  group2.add_child(std::move(group1));

  auto &groups = std::get<std::vector<Group>>(group2.shapes);
  ASSERT_EQ(groups.size(), 1);
  EXPECT_EQ(groups[0].parent, &group2);

  auto &group_0_spheres = std::get<std::vector<Sphere>>(groups[0].shapes);
  ASSERT_EQ(group_0_spheres.size(), 1);
  EXPECT_EQ(group_0_spheres[0].parent, &(groups[0]));
}

TEST(Group, IntersectiongARayWithAnEmptyGroup) {
  Group group;
  Ray ray = Ray(point(0, 0, 0), vec(0, 0, 1));
  std::vector<Intersection> intersections;
  group.intersect(ray, intersections);

  EXPECT_TRUE(intersections.empty());
}

TEST(Group, IntersectingARayWithANonemptyGroup) {
  Group group;

  Sphere sphere1;

  Sphere sphere2;
  sphere2.set_transform(translation(0, 0, -3));

  Sphere sphere3;
  sphere3.set_transform(translation(5, 0, 0));

  group.add_child(std::move(sphere1));
  group.add_child(std::move(sphere2));
  group.add_child(std::move(sphere3));

  Ray ray = Ray(point(0, 0, -5), vec(0, 0, 1));
  std::vector<Intersection> intersections;
  group.intersect(ray, intersections);
  std::sort(intersections.begin(), intersections.end());

  ASSERT_EQ(intersections.size(), 4);
  EXPECT_EQ(*dynamic_cast<Sphere *>(intersections[0].object), sphere2);
  EXPECT_EQ(*dynamic_cast<Sphere *>(intersections[1].object), sphere2);
  EXPECT_EQ(*dynamic_cast<Sphere *>(intersections[2].object), sphere1);
  EXPECT_EQ(*dynamic_cast<Sphere *>(intersections[3].object), sphere1);
}

TEST(Group, IntersectingATransformedGroup) {
  Group group;
  group.set_transform(scaling(2, 2, 2));

  Sphere sphere;
  sphere.set_transform(translation(5, 0, 0));
  group.add_child(std::move(sphere));

  Ray ray = Ray(point(10, 0, -10), vec(0, 0, 1));
  std::vector<Intersection> intersections;
  group.intersect(ray, intersections);

  EXPECT_EQ(intersections.size(), 2);
}

TEST(Group, InheritingParentMaterial) {
  Group group1;
  Material material1;
  material1.color = color(1, 0, 0);
  group1.material = material1;

  Sphere sphere;
  Material material2;
  material2.color = color(0, 1, 0);
  sphere.material = material2;

  Group group2;
  group2.add_child(std::move(sphere));

  group1.add_child(std::move(group2), true);

  auto &group1_groups = group1.get<Group>();
  ASSERT_EQ(group1_groups.size(), 1);

  auto &group1_group_0_spheres = group1_groups[0].get<Sphere>();
  ASSERT_EQ(group1_group_0_spheres.size(), 1);
  EXPECT_EQ(group1_group_0_spheres[0].material, material1);
}

TEST(Group, AGroupHasABoundingBoxThatContainsItsChildren) {
  Sphere sphere;
  sphere.set_transform(mul(translation(2, 5, -3), scaling(2, 2, 2)));

  Cylinder cylinder;
  cylinder.minimum = -2;
  cylinder.maximum = 2;
  cylinder.set_transform(mul(translation(-4, -1, 4), scaling(0.5, 1, 0.5)));

  Group group;
  group.add_child(std::move(sphere));
  group.add_child(std::move(cylinder));

  BoundingBox group_bounds = group.get_bounds();

  EXPECT_EQ(group_bounds.min_point, point(-4.5, -3, -5));
  EXPECT_EQ(group_bounds.max_point, point(4, 7, 4.5));
}

TEST(Group, PartitioningAGroupsChildren) {
  Sphere sphere1;
  sphere1.set_transform(translation(-2, 0, 0));

  Sphere sphere2;
  sphere2.set_transform(translation(2, 0, 0));

  Sphere sphere3;

  Group group;
  group.add_child(std::move(sphere1));
  group.add_child(std::move(sphere2));
  group.add_child(std::move(sphere3));

  auto [left_group, right_group] = group.partition();

  auto &left_group_spheres = left_group.get<Sphere>();
  ASSERT_EQ(left_group_spheres.size(), 1);
  EXPECT_EQ(left_group_spheres[0], sphere1);

  auto &group_spheres = group.get<Sphere>();
  ASSERT_EQ(group_spheres.size(), 1);
  EXPECT_EQ(group_spheres[0], sphere3);

  auto &right_group_spheres = right_group.get<Sphere>();
  ASSERT_EQ(right_group_spheres.size(), 1);
  EXPECT_EQ(right_group_spheres[0], sphere2);
}

TEST(Group, SubdividingAGroupPartitionsItsChildren) {
  Sphere sphere1;
  sphere1.set_transform(translation(-2, -2, 0));

  Sphere sphere2;
  sphere2.set_transform(translation(-2, 2, 0));

  Sphere sphere3;
  sphere3.set_transform(scaling(4, 4, 4));

  Group group;
  group.add_child(std::move(sphere1));
  group.add_child(std::move(sphere2));
  group.add_child(std::move(sphere3));

  ASSERT_EQ(group.count, 3);

  group.divide(1);

  auto &spheres = group.get<Sphere>();

  ASSERT_EQ(spheres.size(), 1);
  EXPECT_EQ(spheres[0], sphere3);

  auto &subgroups1 = group.get<Group>();
  ASSERT_EQ(subgroups1.size(), 1);

  auto &subgroups2 = subgroups1[0].get<Group>();
  ASSERT_EQ(subgroups2.size(), 2);

  ASSERT_EQ(subgroups2[0].get<Sphere>().size(), 1);
  EXPECT_EQ(subgroups2[0].get<Sphere>()[0], sphere1);

  ASSERT_EQ(subgroups2[1].get<Sphere>().size(), 1);
  EXPECT_EQ(subgroups2[1].get<Sphere>()[0], sphere2);
}
