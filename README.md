<table align="center"><tr><td align="center" width="9999">
<img src="scenes/scene_app_6000_4000.png" align="center" width="900" alt="Rendered image by the ray tracer">

# Ray Tracer

a simple ray tracer done according to the book ["The Ray Tracer Challenge"](http://raytracerchallenge.com/)
</td></tr></table>

# Build Setup
The requirements are:

* CMake 3.14 or better
* A C++17 compatible compiler
* Git

To configure and build:

```bash
./build_app
```

To run the app

```bash
./build/apps/scene_app > ./scenes/scene_app_result.ppm
```
