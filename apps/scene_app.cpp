#include <cmath>
#include <iostream>
#include <raytracer/raytracer.hpp>
#include <vector>

int main() {
  OBJParser obj_parser;

  std::vector<std::string> model_paths = {"data/dragon.obj", "data/teapot.obj",
                                          "data/teddy.obj",
                                          "data/octahedron.obj"};

  for (const auto &path : model_paths) {
    bool successful_parse = obj_parser.parse_from_file(path);

    if (!successful_parse) {
      std::cout << "Failed to open the obj file: " << path << std::endl;
      return 1;
    }
  }

  World world(Light(point(10, 13, -15), color(1, 1, 1)),
              color(160 / 255.f, 196 / 255.f, 255 / 255.f));

  auto &groups = obj_parser.groups;

  auto &dragon_group = groups["Dragon"];
  dragon_group.divide(20);
  Material dragon_material;
  dragon_material.color = color(189 / 255.f, 178 / 255.f, 255 / 255.f);
  dragon_group.set_material(dragon_material);
  dragon_group.set_transform(
      mul(rotation_y(-M_PI * 3.3f / 4.f), scaling(1.25f, 1.25f, 1.25f)));
  auto dragon_bounding_box = dragon_group.parent_space_bounds();
  dragon_group.set_transform(
      mul(translation(-7.3f, -dragon_bounding_box.min_point.y, 0),
          dragon_group.transform));
  world.groups.emplace_back(std::move(dragon_group));

  float teapot_x_pos = 1.f;
  float teapot_y_pos = 4.f;
  float teapot_z_pos = 5.f;

  Cylinder teapot_pedestal_cylinder_1;
  Material teapot_pedestal_cylinder_1_material;
  teapot_pedestal_cylinder_1_material.color =
      color(253 / 255.f, 255 / 255.f, 182 / 255.f);
  teapot_pedestal_cylinder_1.set_material(teapot_pedestal_cylinder_1_material);
  teapot_pedestal_cylinder_1.set_transform(
      mul(rotation_z(M_PI / 2.f), scaling(0.7f, 0.7f, 0.7f)));

  Cylinder teapot_pedestal_cylinder_2;
  Material teapot_pedestal_cylinder_2_material;
  teapot_pedestal_cylinder_2_material.color =
      color(255 / 255.f, 255 / 255.f, 252 / 255.f);
  teapot_pedestal_cylinder_2.set_material(teapot_pedestal_cylinder_2_material);
  teapot_pedestal_cylinder_2.set_transform(
      mul(rotation_x(M_PI / 2.f), scaling(0.7f, 0.7f, 0.7f)));

  CSG cylinder_cross(CSG::Operation::Union, &teapot_pedestal_cylinder_1,
                     &teapot_pedestal_cylinder_2);

  Cube teapot_pedestal_cube;
  Material teapot_pedestal_cube_material;
  teapot_pedestal_cube_material.color =
      color(255 / 255.f, 198 / 255.f, 255 / 255.f);
  teapot_pedestal_cube.set_material(teapot_pedestal_cube_material);

  CSG teapot_pedestal(CSG::Operation::Difference, &teapot_pedestal_cube,
                      &cylinder_cross);

  teapot_pedestal.set_transform(mul(
      translation(teapot_x_pos, teapot_y_pos / 2.f, teapot_z_pos),
      mul(rotation_y(M_PI / 6.f), scaling(2.6f, teapot_y_pos / 2.f, 2.6f))));
  world.csgs.push_back(teapot_pedestal);

  auto &teapot_group = groups["Teapot001"];
  float teapot_scale = 0.3f;
  teapot_group.divide(20);
  Material teapot_material;
  teapot_material.color = color(202 / 255.f, 255 / 255.f, 191 / 255.f);
  teapot_group.set_material(teapot_material);
  teapot_group.set_transform(
      mul(rotation_y(M_PI / 5.f),
          mul(rotation_x(-M_PI / 2.0f),
              scaling(teapot_scale, teapot_scale, teapot_scale))));
  auto teapot_bounding_box = teapot_group.parent_space_bounds();
  teapot_group.set_transform(
      mul(translation(
              teapot_x_pos,
              teapot_y_pos + teapot_scale * (-teapot_bounding_box.min_point.y),
              teapot_z_pos),
          teapot_group.transform));
  world.groups.emplace_back(std::move(teapot_group));

  auto &teddy_group = groups["Teddy"];
  float teddy_scale = 0.1f;
  teddy_group.divide(20);
  Material teddy_material;
  teddy_material.color = color(155 / 255.f, 246 / 255.f, 255 / 255.f);
  teddy_group.set_material(teddy_material);
  teddy_group.set_transform(
      mul(rotation_y(M_PI * 6.f / 5.f),
          scaling(teddy_scale, teddy_scale, teddy_scale)));
  auto teddy_bounding_box = teddy_group.parent_space_bounds();
  teddy_group.set_transform(
      mul(translation(4.25f, -teddy_bounding_box.min_point.y, -2.1f),
          teddy_group.transform));
  world.groups.emplace_back(std::move(teddy_group));

  auto &octahedron_group = groups["Octahedron"];
  Material octahedron_material;
  octahedron_material.color = color(0.373f, 0.404f, 0.550f);
  octahedron_material.ambient = .0f;
  octahedron_material.diffuse = .2f;
  octahedron_material.specular = 1.f;
  octahedron_material.reflective = .7f;
  octahedron_group.set_material(octahedron_material);
  octahedron_group.set_transform(
      mul(translation(8.5f, 9.3f, 5.f), scaling(5.3f, 5.3f, 5.3f)));
  world.groups.emplace_back(octahedron_group);

  Plane floor;
  Material floor_material;
  floor_material.pattern = RingPattern(
      color(255 / 255.f, 214 / 255.f, 165 / 255.f),
      color(0.95f * 255 / 255.f, 0.95f * 214 / 255.f, 0.95f * 165 / 255.f));
  floor.set_material(floor_material);
  world.planes.emplace_back(floor);

  Sphere mirror_sphere;
  Material mirror_sphere_material;
  mirror_sphere_material.color = color(0.373f, 0.404f, 0.550f);
  mirror_sphere_material.ambient = .0f;
  mirror_sphere_material.diffuse = .2f;
  mirror_sphere_material.specular = 1.f;
  mirror_sphere_material.reflective = .7f;
  mirror_sphere.set_material(mirror_sphere_material);
  mirror_sphere.set_transform(
      mul(translation(-14.f, 18.f, 16.f), scaling(17.f, 17.f, 17.f)));
  world.spheres.emplace_back(mirror_sphere);

  Sphere center_glass_sphere;
  Material center_glass_sphere_material;
  center_glass_sphere_material.color = color(1.f, 1.f, 1.f);
  center_glass_sphere_material.reflective = .0f;
  center_glass_sphere_material.shininess = 400.0f;
  center_glass_sphere_material.transparency = 1.f;
  center_glass_sphere_material.refractive_index = 1.5f;
  center_glass_sphere.set_material(center_glass_sphere_material);
  center_glass_sphere.set_transform(
      mul(translation(-.3f, .9f, -17.f), scaling(.9f, .9f, .9f)));
  world.spheres.emplace_back(center_glass_sphere);

  Sphere right_glass_sphere;
  Material right_glass_sphere_material;
  right_glass_sphere_material.color = color(.6f, 1.f, .6f);
  right_glass_sphere_material.reflective = .0f;
  right_glass_sphere_material.transparency = 1.f;
  right_glass_sphere_material.refractive_index = 1.5f;
  right_glass_sphere.set_material(right_glass_sphere_material);
  right_glass_sphere.set_transform(
      mul(translation(2.95f, 0.4f, -12.5f), scaling(0.4f, 0.4f, 0.4f)));
  world.spheres.emplace_back(right_glass_sphere);

  Sphere small_right_glass_sphere;
  Material small_right_glass_sphere_material;
  small_right_glass_sphere_material.color = color(.6f, .6f, 1.f);
  small_right_glass_sphere_material.reflective = .0f;
  small_right_glass_sphere_material.transparency = 1.f;
  small_right_glass_sphere_material.refractive_index = 1.5f;
  small_right_glass_sphere.set_material(small_right_glass_sphere_material);
  small_right_glass_sphere.set_transform(
      mul(translation(3.2f, 0.17f, -16.f), scaling(0.17f, 0.17f, 0.17f)));
  world.spheres.emplace_back(small_right_glass_sphere);

  Sphere left_glass_sphere;
  Material left_glass_sphere_material;
  left_glass_sphere_material.color = color(1.f, .6f, .6f);
  left_glass_sphere_material.reflective = .0f;
  left_glass_sphere_material.transparency = 1.f;
  left_glass_sphere_material.refractive_index = 1.5f;
  left_glass_sphere.set_material(left_glass_sphere_material);
  left_glass_sphere.set_transform(
      mul(translation(-2.9f, 0.25f, -15.f), scaling(0.25f, 0.25f, 0.25f)));
  world.spheres.emplace_back(left_glass_sphere);

  Camera camera(6000, 4000, M_PI / 4.f);
  //Camera camera(800, 600, M_PI / 4.f);
  camera.set_transform(
      view_transform(point(.2f, 1.f, -22.f), point(0, 1, 0), point(0, 1, 0)));

  Canvas canvas = camera.render_threaded(world, 9, 100, 20, 0.2f);

  std::cout << canvas.to_ppm();
}
