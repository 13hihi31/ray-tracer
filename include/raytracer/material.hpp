#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include <optional>
#include <raytracer/light.hpp>
#include <raytracer/pattern.hpp>
#include <raytracer/tuple.hpp>

class Shape;

struct Material {
  bool operator==(const Material &other) const;
  Color lighting(Shape *shape, const Light &light, const Point &point,
                 const Vec &eye, const Vec &normal,
                 bool in_shadow = false) const;

  std::optional<Pattern> pattern;
  Color color = Color(1, 1, 1, 0);
  float ambient = 0.1;
  float diffuse = 0.9;
  float specular = 0.9;
  float shininess = 200.0;
  float reflective = 0.0;
  float transparency = 0.0f;
  float refractive_index = 1.0f;
};

#endif
