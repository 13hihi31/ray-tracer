#ifndef HELPER_FUNCTIONS_HPP
#define HELPER_FUNCTIONS_HPP

#include <cmath>

template <typename T1, typename T2>
bool eql(const T1 &x, const T2 &y, float eps = 0.00001f) {
  return std::abs(x - y) < eps;
}

#endif
