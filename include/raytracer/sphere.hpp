#ifndef SPHERE_HPP
#define SPHERE_HPP

#include <raytracer/shape.hpp>

class Sphere : public Shape {
 public:
  Sphere() = default;
  Sphere(const Matrix<4> &transform, const Material &material);

  bool operator==(const Sphere &other) const;

  Vec local_normal_at(const Point &local_point,
                      const Intersection &intersection) const override;
  void local_intersect(const Ray &local_ray,
                       std::vector<Intersection> &intersections) override;
  BoundingBox get_bounds() const override;
};

Sphere glass_sphere();

#endif
