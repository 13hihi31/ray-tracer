#ifndef CANVAS_HPP
#define CANVAS_HPP

#include <raytracer/tuple.hpp>
#include <string>
#include <vector>

class Canvas {
 public:
  Canvas() {}
  Canvas(unsigned width, unsigned height);
  Canvas(const Canvas &other);
  Canvas(Canvas &&other) noexcept;
  Canvas &operator=(const Canvas &other);
  Canvas &operator=(Canvas &&other) noexcept;

  Color &operator()(unsigned pixel_x_index, unsigned pixel_y_index);
  const Color &operator()(unsigned pixel_x_index, unsigned pixel_y_index) const;

  std::string to_ppm() const;

  Color laplacian(unsigned pixel_x_index, unsigned pixel_y_index) const;

  unsigned width;
  unsigned height;
  std::vector<std::vector<Color>> canvas;
};

#endif
