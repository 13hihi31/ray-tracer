#ifndef GROUP_HPP
#define GROUP_HPP

#include <raytracer/cone.hpp>
#include <raytracer/csg.hpp>
#include <raytracer/cube.hpp>
#include <raytracer/cylinder.hpp>
#include <raytracer/plane.hpp>
#include <raytracer/shape.hpp>
#include <raytracer/smooth_triangle.hpp>
#include <raytracer/sphere.hpp>
#include <raytracer/triangle.hpp>
#include <tuple>
#include <type_traits>
#include <vector>

class Group : public Shape {
 public:
  Group() = default;
  Group(const Group &other);
  Group(Group &&other) noexcept;
  Group &operator=(const Group &other);
  Group &operator=(Group &&other) noexcept;

  void set_parent(Shape *new_parent) override;
  void set_material(const Material &new_material) override;
  bool includes(const Shape *other) const override;
  Vec local_normal_at(const Point &local_point,
                      const Intersection &intersection) const override;
  void local_intersect(const Ray &local_ray,
                       std::vector<Intersection> &intersections) override;
  BoundingBox get_bounds() const override;
  void divide(unsigned division_granularity) override;

  void set_parent_for_all_in_group();
  bool empty() const;

  template <typename ShapeType>
  void add_child(ShapeType &&shape, bool inherit_material = false);

  template <typename ShapeType>
  std::vector<ShapeType> &get() {
    return std::get<std::vector<ShapeType>>(shapes);
  }

  template <typename ShapeType>
  const std::vector<ShapeType> &get() const {
    return std::get<std::vector<ShapeType>>(shapes);
  }

  std::pair<Group, Group> partition();

  std::tuple<std::vector<Sphere>, std::vector<Plane>, std::vector<Cube>,
             std::vector<Cylinder>, std::vector<Cone>, std::vector<CSG>,
             std::vector<Triangle>, std::vector<SmoothTriangle>,
             std::vector<Group>>
      shapes;

  BoundingBox bounds;
  bool is_empty = true;
  unsigned count = 0;
};

template <typename ShapeType>
void Group::add_child(ShapeType &&shape, bool inherit_material) {
  bounds.add_box(shape.parent_space_bounds());

  auto &ShapeType_vec = get<ShapeType>();
  ShapeType_vec.push_back(std::forward<ShapeType>(shape));

  if (inherit_material) ShapeType_vec.back().set_material(this->material);
  ShapeType_vec.back().set_parent(this);

  if constexpr (std::is_same_v<ShapeType, Group>) {
    is_empty = is_empty && shape.is_empty;
  } else {
    is_empty = false;
  }

  ++count;
}

#endif
