#ifndef PLANE_HPP
#define PLANE_HPP

#include <raytracer/shape.hpp>

class Plane : public Shape {
 public:
  Plane() = default;
  Plane(const Matrix<4> &transform, const Material &material);

  Vec local_normal_at(const Point &point,
                      const Intersection &intersection) const override;
  void local_intersect(const Ray &local_ray,
                       std::vector<Intersection> &intersections) override;
  BoundingBox get_bounds() const override;
};

#endif
