#ifndef OBJ_PARSER_HPP
#define OBJ_PARSER_HPP

#include <map>
#include <raytracer/group.hpp>
#include <raytracer/tuple.hpp>
#include <string>
#include <vector>

class OBJParser {
 public:
  OBJParser() = default;

  bool parse_from_file(const std::string &file_path);
  Group to_group() const;
  std::map<std::string, Group> get_groups();

  void parse(const std::string &file);

  bool parse_group(const std::string &line);
  bool parse_vertex(const std::string &line);
  bool parse_normal(const std::string &line);
  bool parse_polygonal_face(const std::string &line);
  bool parse_extended_polygonal_face(const std::string &line);

  void fan_triangulation(const std::vector<int> &vertex_idxs);
  void fan_triangulation(const std::vector<int> &vertex_idxs,
                         const std::vector<int> &texture_idxs,
                         const std::vector<int> &normal_idxs);

  std::vector<Point> vertices;
  std::vector<Vec> normals;

  int ignored_lines;

  Group default_group;
  Group *current_group;

  std::map<std::string, Group> groups;
};

#endif
