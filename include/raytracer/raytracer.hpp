#ifndef RAYTRACER_HPP
#define RAYTRACER_HPP

#include <raytracer/bounding_box.hpp>
#include <raytracer/camera.hpp>
#include <raytracer/canvas.hpp>
#include <raytracer/colors.hpp>
#include <raytracer/cone.hpp>
#include <raytracer/constants.hpp>
#include <raytracer/csg.hpp>
#include <raytracer/cube.hpp>
#include <raytracer/cylinder.hpp>
#include <raytracer/group.hpp>
#include <raytracer/helper_functions.hpp>
#include <raytracer/intersection.hpp>
#include <raytracer/intersection_data.hpp>
#include <raytracer/light.hpp>
#include <raytracer/material.hpp>
#include <raytracer/matrix.hpp>
#include <raytracer/obj_parser.hpp>
#include <raytracer/pattern.hpp>
#include <raytracer/pattern_funcs.hpp>
#include <raytracer/plane.hpp>
#include <raytracer/ray.hpp>
#include <raytracer/schlick.hpp>
#include <raytracer/shape.hpp>
#include <raytracer/smooth_triangle.hpp>
#include <raytracer/sphere.hpp>
#include <raytracer/transforms.hpp>
#include <raytracer/triangle.hpp>
#include <raytracer/tuple.hpp>
#include <raytracer/world.hpp>

#endif
