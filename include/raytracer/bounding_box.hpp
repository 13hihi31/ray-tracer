#ifndef BOUNDING_BOX_HPP
#define BOUNDING_BOX_HPP

#include <raytracer/matrix.hpp>
#include <raytracer/ray.hpp>
#include <raytracer/tuple.hpp>
#include <utility>

struct BoundingBox {
  BoundingBox();
  BoundingBox(const BoundingBox &other);
  BoundingBox(const Point &min, const Point &max);
  BoundingBox &operator=(const BoundingBox &other);

  void add_point(const Point &point);
  void add_box(const BoundingBox &box);
  bool contains(const Point &point) const;
  bool contains(const BoundingBox &box) const;
  BoundingBox transform(const Matrix<4> &transformation) const;
  bool intersects(const Ray &ray) const;
  std::pair<BoundingBox, BoundingBox> split() const;

  Point min_point;
  Point max_point;
};

#endif
