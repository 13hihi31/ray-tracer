#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include <raytracer/shape.hpp>

class Triangle : public Shape {
 public:
  Triangle() = default;
  Triangle(const Point &p1, const Point &p2, const Point &p3);

  Vec local_normal_at(const Point &local_point,
                      const Intersection &intersection) const override;
  void local_intersect(const Ray &local_ray,
                       std::vector<Intersection> &intersections) override;
  BoundingBox get_bounds() const override;

  Point p1;
  Point p2;
  Point p3;
  Vec e1;
  Vec e2;
  Vec normal;
};

#endif
