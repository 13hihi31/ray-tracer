#ifndef CSG_HPP
#define CSG_HPP

#include <raytracer/shape.hpp>

class CSG : public Shape {
 public:
  enum class Operation { Union, Intersection, Difference };

  CSG() = default;
  CSG(const Operation &operation, Shape *left, Shape *right);

  void set_material(const Material &new_material) override;
  bool includes(const Shape *other) const override;
  Vec local_normal_at(const Point &local_point,
                      const Intersection &intersection) const override;
  void local_intersect(const Ray &local_ray,
                       std::vector<Intersection> &intersections) override;
  BoundingBox get_bounds() const override;
  void divide(unsigned division_granularity) override;

  bool intersection_allowed(const Operation &op, bool hit_left,
                            bool inside_left, bool inside_right) const;
  void filter_intersections(std::vector<Intersection> &intersections,
                            const size_t old_size) const;

  Operation operation = Operation::Union;
  Shape *left = nullptr;
  Shape *right = nullptr;
  BoundingBox bounds;
};

#endif
