#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <array>
#include <initializer_list>
#include <iostream>
#include <raytracer/helper_functions.hpp>
#include <raytracer/tuple.hpp>

template <size_t n>
class Matrix {
 public:
  Matrix();
  Matrix(std::initializer_list<std::array<float, n>> matrix);
  bool operator==(const Matrix<n> &other) const;
  void operator=(std::initializer_list<std::array<float, n>> matrix_list);
  float &operator()(int row, int col);
  float operator()(int row, int col) const;
  int size() const;

  // linear algebra operations
  template <size_t m>
  friend Matrix<m> mul(const Matrix<m> &a, const Matrix<m> &b);
  friend Tuple mul(const Matrix<4> &a, const Tuple &b);
  Matrix<n> transpose() const;
  float det() const;
  Matrix<n - 1> submatrix(int row, int col) const;
  float matrix_minor(int row, int col) const;
  float cofactor(int row, int col) const;
  Matrix<n> inverse() const;

  template <size_t m>
  friend std::ostream &operator<<(std::ostream &stream, const Matrix<m> &a);

  float matrix[n][n];
};

template <size_t n>
Matrix<n> identity_matrix();

//// implementation

template <size_t n>
Matrix<n>::Matrix() {
  for (size_t r = 0; r < n; ++r) {
    for (size_t c = 0; c < n; ++c) {
      matrix[r][c] = 0.0f;
    }
  }
}

template <size_t n>
Matrix<n>::Matrix(std::initializer_list<std::array<float, n>> matrix_list) {
  auto matrix_list_it = matrix_list.begin();
  for (size_t r = 0; r < n; ++r) {
    const std::array<float, n> &row = *matrix_list_it;
    for (size_t c = 0; c < n; ++c) {
      matrix[r][c] = row[c];
    }
    ++matrix_list_it;
  }
}

template <size_t n>
bool Matrix<n>::operator==(const Matrix<n> &other) const {
  bool equal = true;
  for (size_t r = 0; r < n; ++r) {
    for (size_t c = 0; c < n; ++c) {
      equal = equal && eql(matrix[r][c], other(r, c));
    }
  }
  return equal;
}

template <size_t n>
void Matrix<n>::operator=(
    std::initializer_list<std::array<float, n>> matrix_list) {
  auto matrix_list_it = matrix_list.begin();
  for (size_t r = 0; r < n; ++r) {
    const std::array<float, n> &row = *matrix_list_it;
    for (size_t c = 0; c < n; ++c) {
      matrix[r][c] = row[c];
    }
    ++matrix_list_it;
  }
}

template <size_t n>
float &Matrix<n>::operator()(int row, int col) {
  return matrix[row][col];
}

template <size_t n>
float Matrix<n>::operator()(int row, int col) const {
  return matrix[row][col];
}

template <size_t n>
int Matrix<n>::size() const {
  return n;
}

//// linear algebra operations implementation

template <size_t m>
inline Matrix<m> mul(const Matrix<m> &a, const Matrix<m> &b) {
  Matrix<m> c;
  for (int i = 0; i < m; ++i) {
    for (int j = 0; j < m; ++j) {
      for (int k = 0; k < m; ++k) {
        c(i, j) += a(i, k) * b(k, j);
      }
    }
  }
  return c;
}

inline Tuple mul(const Matrix<4> &a, const Tuple &b) {
  return {a.matrix[0][0] * b.x + a.matrix[0][1] * b.y + a.matrix[0][2] * b.z +
              a.matrix[0][3] * b.w,
          a.matrix[1][0] * b.x + a.matrix[1][1] * b.y + a.matrix[1][2] * b.z +
              a.matrix[1][3] * b.w,
          a.matrix[2][0] * b.x + a.matrix[2][1] * b.y + a.matrix[2][2] * b.z +
              a.matrix[2][3] * b.w,
          a.matrix[3][0] * b.x + a.matrix[3][1] * b.y + a.matrix[3][2] * b.z +
              a.matrix[3][3] * b.w};
}

template <size_t n>
Matrix<n> Matrix<n>::transpose() const {
  Matrix<n> transposed;
  for (size_t r = 0; r < n; ++r) {
    for (size_t c = 0; c < n; ++c) {
      transposed.matrix[c][r] = matrix[r][c];
    }
  }
  return transposed;
}

template <>
inline float Matrix<2>::det() const {
  return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
}

template <size_t n>
inline float Matrix<n>::det() const {
  float result = 0.0f;
  for (size_t c = 0; c < n; ++c) {
    result += matrix[0][c] * this->cofactor(0, c);
  }
  return result;
}

template <size_t n>
Matrix<n - 1> Matrix<n>::submatrix(int row, int col) const {
  Matrix<n - 1> result;
  size_t rr = 0;
  for (size_t r = 0; r < n; ++r) {
    if (r != row) {
      size_t cc = 0;
      for (size_t c = 0; c < n; ++c) {
        if (c != col) {
          result.matrix[rr][cc] = matrix[r][c];
          ++cc;
        }
      }
      ++rr;
    }
  }
  return result;
}

template <size_t n>
float Matrix<n>::matrix_minor(int row, int col) const {
  Matrix<n - 1> A = this->submatrix(row, col);
  return A.det();
}

template <size_t n>
float Matrix<n>::cofactor(int row, int col) const {
  return (1 - 2 * ((row + col) % 2)) * this->matrix_minor(row, col);
}

template <size_t n>
Matrix<n> Matrix<n>::inverse() const {
  Matrix<n> result;
  float determinant = this->det();
  for (size_t r = 0; r < n; ++r) {
    for (size_t c = 0; c < n; ++c) {
      result.matrix[c][r] = this->cofactor(r, c) / determinant;
    }
  }
  return result;
}

template <size_t m>
std::ostream &operator<<(std::ostream &stream, const Matrix<m> &a) {
  for (size_t r = 0; r < m; ++r) {
    if (r != 0) stream << "  ";
    for (size_t c = 0; c < m; ++c) {
      stream << a.matrix[r][c] << " ";
    }
  }
  return stream;
}

template <size_t n>
Matrix<n> identity_matrix() {
  Matrix<n> i;
  for (size_t j = 0; j < n; ++j) {
    i(j, j) = 1.0f;
  }
  return i;
}

#endif
