#ifndef PATTERN_HPP
#define PATTERN_HPP

#include <raytracer/matrix.hpp>
#include <raytracer/tuple.hpp>
#include <variant>

struct TwoColorPattern {
  TwoColorPattern(const Color &color_a, const Color &color_b)
      : color_a(color_a), color_b(color_b) {}
  void set_transform(const Matrix<4> &transform);

  Matrix<4> transform_inverse = identity_matrix<4>();
  Color color_a;
  Color color_b;
};

struct StripePattern : public TwoColorPattern {
  StripePattern(const Color &color_a, const Color &color_b)
      : TwoColorPattern(color_a, color_b) {}
  Color pattern_at(const Point &point) const;
};

struct GradientPattern : public TwoColorPattern {
  GradientPattern(const Color &color_a, const Color &color_b)
      : TwoColorPattern(color_a, color_b) {}
  Color pattern_at(const Point &point) const;
};

struct RingPattern : public TwoColorPattern {
  RingPattern(const Color &color_a, const Color &color_b)
      : TwoColorPattern(color_a, color_b) {}
  Color pattern_at(const Point &point) const;
};

struct CheckerPattern : public TwoColorPattern {
  CheckerPattern(const Color &color_a, const Color &color_b)
      : TwoColorPattern(color_a, color_b) {}
  Color pattern_at(const Point &point) const;
};

struct TestPattern {
  Color pattern_at(const Point &point) const;
  void set_transform(const Matrix<4> &transform) {
    transform_inverse = transform.inverse();
  }
  Matrix<4> transform_inverse = identity_matrix<4>();
};

using Pattern = std::variant<StripePattern, GradientPattern, RingPattern,
                             CheckerPattern, TestPattern>;

#endif
