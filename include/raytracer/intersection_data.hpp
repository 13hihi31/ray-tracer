#ifndef INTERSECTION_DATA_HPP
#define INTERSECTION_DATA_HPP

#include <raytracer/intersection.hpp>
#include <raytracer/ray.hpp>
#include <raytracer/shape.hpp>
#include <raytracer/tuple.hpp>

struct IntersectionData {
  IntersectionData() = default;
  IntersectionData(const Intersection &intersection, const Ray &ray,
                   const std::vector<Intersection> &ints = {});

  float t;
  Shape *object;
  Point intersection_point;
  Point over_point;
  Point under_point;
  Vec eye_vector;
  Vec normal_vector;
  bool inside_object;
  Vec reflect_vector;
  float exiting_refractive_index;
  float entering_refractive_index;
};

#endif
