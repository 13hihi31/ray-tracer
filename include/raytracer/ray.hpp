#ifndef RAY_HPP
#define RAY_HPP

#include <raytracer/matrix.hpp>
#include <raytracer/tuple.hpp>

class Ray {
 public:
  Ray(Point origin, Vec direction);
  Point position(float t) const;
  Ray transform(const Matrix<4> &m) const;

  Point origin;
  Vec direction;
};

#endif
