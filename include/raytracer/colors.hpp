#ifndef COLORS_HPP
#define COLORS_HPP

#include <raytracer/tuple.hpp>

namespace colors {

const Color black = color(0, 0, 0);
const Color white = color(1, 1, 1);

};  // namespace colors

#endif
