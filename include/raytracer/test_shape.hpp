#ifndef TEST_SHAPE_HPP
#define TEST_SHAPE_HPP

#include <raytracer/shape.hpp>

class TestShape : public Shape {
 public:
  TestShape() = default;
  TestShape(const Matrix<4> &transform, const Material &material);

  Vec local_normal_at(const Point &point,
                      const Intersection &intersection) const override;
  void local_intersect(const Ray &local_ray,
                       std::vector<Intersection> &intersections) override;
  BoundingBox get_bounds() const;
};

#endif
