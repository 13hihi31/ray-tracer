#ifndef CONE_HPP
#define CONE_HPP

#include <limits>
#include <raytracer/shape.hpp>

class Cone : public Shape {
 public:
  Cone() = default;
  Cone(const Matrix<4> &transform, const Material &material);

  Vec local_normal_at(const Point &local_point,
                      const Intersection &intersection) const override;
  void local_intersect(const Ray &local_ray,
                       std::vector<Intersection> &intersections) override;
  BoundingBox get_bounds() const override;

  float maximum = std::numeric_limits<float>::max();
  float minimum = std::numeric_limits<float>::lowest();
  bool closed = false;
};

#endif
