#ifndef WORLD_HPP
#define WORLD_HPP

#include <raytracer/cone.hpp>
#include <raytracer/csg.hpp>
#include <raytracer/cube.hpp>
#include <raytracer/cylinder.hpp>
#include <raytracer/group.hpp>
#include <raytracer/intersection.hpp>
#include <raytracer/intersection_data.hpp>
#include <raytracer/light.hpp>
#include <raytracer/plane.hpp>
#include <raytracer/ray.hpp>
#include <raytracer/sphere.hpp>
#include <raytracer/tuple.hpp>
#include <vector>

class World {
 public:
  World() = default;
  World(const Light &light, Color background_color = color(0.f, 0.f, 0.f))
      : light(light), background_color(background_color) {}

  Color color_at(const Ray &ray, std::vector<Intersection> &intersections,
                 int remaining = 5);

  void intersect(const Ray &ray, std::vector<Intersection> &intersections);
  Color shade_hit(const IntersectionData &intersection_data,
                  std::vector<Intersection> &intersections, int remaining = 5);
  bool is_shadowed(const Point &point,
                   std::vector<Intersection> &intersections);
  Color reflected_color(const IntersectionData &intersection_data,
                        std::vector<Intersection> &intersections,
                        int remaining = 5);
  Color refracted_color(const IntersectionData &intersection_data,
                        std::vector<Intersection> &intersections,
                        int remaining = 5);

  Light light;

  Color background_color = color(0.f, 0.f, 0.f);

  std::vector<Sphere> spheres;
  std::vector<Plane> planes;
  std::vector<Cube> cubes;
  std::vector<Cylinder> cylinders;
  std::vector<Cone> cones;
  std::vector<CSG> csgs;
  std::vector<Group> groups;
};

World default_world();

#endif
