#ifndef LIGHT_HPP
#define LIGHT_HPP

#include <raytracer/tuple.hpp>

class Light {
 public:
  Light() {}
  Light(const Point &position, const Color &intensity);
  bool operator==(const Light &other) const;

  Point position;
  Color intensity;
};

#endif
