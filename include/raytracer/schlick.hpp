#ifndef SCHLICK_HPP
#define SCHLICK_HPP

#include <raytracer/intersection_data.hpp>

float schlick(const IntersectionData &intersection_data);

#endif
