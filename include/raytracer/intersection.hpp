#ifndef INTERSECTION_HPP
#define INTERSECTION_HPP

#include <optional>
#include <vector>

class Shape;

class Intersection {
 public:
  Intersection(float t, Shape *object);
  Intersection(float t, Shape *object, float u, float v);
  Intersection(const Intersection &other) = default;

  bool operator==(const Intersection &other) const;
  bool operator<(const Intersection &other) const;

  static std::optional<Intersection> hit(
      const std::vector<Intersection> &intersections);

  float t;
  Shape *object;
  float u;
  float v;
};

#endif
