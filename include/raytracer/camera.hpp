#ifndef CAMERA_H
#define CAMERA_H

#include <raytracer/canvas.hpp>
#include <raytracer/matrix.hpp>
#include <raytracer/ray.hpp>
#include <raytracer/world.hpp>

class Camera {
 public:
  Camera(unsigned horizontal_size, unsigned vertical_size, float field_of_view);

  Canvas render(World &world, size_t initial_intersections_number = 20,
                unsigned num_samples_per_pixel = 1,
                float antialias_threshold = 0.2f,
                bool paint_edges_only = false);
  Canvas render_threaded(World &world, unsigned threads_number,
                         size_t initial_intersections_number = 20,
                         unsigned num_samples_per_pixel = 1,
                         float antialias_threshold = 0.2f,
                         bool paint_edges_only = false);

  void render_lines(World &world, Canvas &canvas, unsigned start_line,
                    unsigned end_line,
                    size_t initial_intersections_number = 20) const;
  Ray ray_for_pixel(unsigned canvas_pixel_x_index,
                    unsigned canvas_pixel_y_index, float pixel_x_offset = 0.5f,
                    float pixel_y_offset = 0.5f) const;

  void smoothout_edges(World &world, Canvas &canvas, Canvas &canvas_antialiased,
                       unsigned start_line, unsigned end_line,
                       size_t initial_intersections_number,
                       unsigned num_samples_per_pixel,
                       float antialias_threshold, bool paint_edges_only);

  void set_transform(const Matrix<4> &transform);

  unsigned horizontal_size;
  unsigned vertical_size;
  float field_of_view;
  float pixel_size;
  float half_width;
  float half_height;
  Matrix<4> world_transform_inverse;
};

#endif
