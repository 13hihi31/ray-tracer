#ifndef PATTERN_FUNCS_HPP
#define PATTERN_FUNCS_HPP

#include <raytracer/matrix.hpp>
#include <raytracer/pattern.hpp>
#include <raytracer/shape.hpp>
#include <raytracer/tuple.hpp>
#include <variant>

inline Color pattern_at_shape(const Pattern &pattern, const Shape &shape,
                              const Point &point) {
  Point shape_point = shape.world_to_object(point);
  return std::visit(
      [&shape_point](const auto &p) -> Color {
        return p.pattern_at(shape_point);
      },
      pattern);
}

inline void set_transform(Pattern &pattern, Matrix<4> &&transform) {
  std::visit([&transform](auto &p) { p.set_transform(transform); }, pattern);
}

#endif
