#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <limits>
#include <raytracer/bounding_box.hpp>
#include <raytracer/intersection.hpp>
#include <raytracer/material.hpp>
#include <raytracer/matrix.hpp>
#include <raytracer/ray.hpp>
#include <raytracer/tuple.hpp>

class Shape {
 public:
  Shape();
  Shape(const Shape &shape_to_copy) = default;
  Shape(const Matrix<4> &transform, const Material &material);
  Shape &operator=(const Shape &other);

  Vec normal_at(const Point &point, const Intersection &hit);
  Point world_to_object(Point point) const;
  Vec normal_to_world(Vec normal) const;
  void intersect(const Ray &ray, std::vector<Intersection> &intersections);

  void set_transform(const Matrix<4> &new_transform);

  virtual void set_parent(Shape *new_parent);
  virtual void set_material(const Material &new_material);
  virtual bool eq(const Shape &other) const;
  virtual bool includes(const Shape *other) const;
  virtual Vec local_normal_at(const Point &local_point,
                              const Intersection &intersection) const = 0;
  virtual void local_intersect(const Ray &local_ray,
                               std::vector<Intersection> &intersections) = 0;
  virtual BoundingBox get_bounds() const;
  virtual void divide(unsigned division_granularity);
  BoundingBox parent_space_bounds() const;

  Matrix<4> transform;
  Matrix<4> transform_inverse;
  Material material;
  Shape *parent = nullptr;
};

#endif
