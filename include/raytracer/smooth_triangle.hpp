#ifndef SMOOTH_TRIANGLE_HPP
#define SMOOTH_TRIANGLE_HPP

#include <raytracer/triangle.hpp>

class SmoothTriangle : public Triangle {
 public:
  SmoothTriangle() = default;
  SmoothTriangle(const Point &p1, const Point &p2, const Point &p3,
                 const Vec &n1, const Vec &n2, const Vec &n3);

  Vec local_normal_at(const Point &local_point,
                      const Intersection &intersection) const override;
  void local_intersect(const Ray &local_ray,
                       std::vector<Intersection> &intersections) override;

  Vec n1;
  Vec n2;
  Vec n3;
};

#endif
