#ifndef STRING_OPERATIONS_HPP
#define STRING_OPERATIONS_HPP

#include <sstream>
#include <string>
#include <vector>

namespace string_operations {

std::vector<std::string> split(const std::string str,
                               const std::string delim_str);
void remove_double_spaces(std::string &str);
void remove_character(std::string &str, const char character);

template <typename T>
bool is_numeric_type(const std::string &str) {
  std::istringstream iss(str);
  T type_instance;
  iss >> std::noskipws >> type_instance;
  return iss && iss.eof();
}

};  // namespace string_operations

#endif
