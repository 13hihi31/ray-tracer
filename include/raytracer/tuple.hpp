#ifndef TUPLE_HPP
#define TUPLE_HPP

#include <iostream>

struct Tuple {
  Tuple() = default;
  Tuple(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}
  bool operator==(const Tuple &other) const;
  Tuple operator+(const Tuple &other) const;
  Tuple operator-(const Tuple &other) const;
  Tuple operator*(const Tuple &other) const;
  Tuple operator*(float scale) const;

  friend Tuple operator-(const Tuple &t);
  friend float mag(const Tuple &t);
  friend Tuple norm(const Tuple &t);
  friend float dot(const Tuple &t1, const Tuple &t2);
  friend Tuple cross(const Tuple &t1, const Tuple &t2);

  template <typename T>
  friend Tuple operator*(const T &num, const Tuple &t);
  template <typename T>
  friend Tuple operator*(const Tuple &t, const T &num);
  template <typename T>
  friend Tuple operator/(const Tuple &t, const T &num);

  friend std::ostream &operator<<(std::ostream &stream, const Tuple &a);

  float x = 0.f;
  float y = 0.f;
  float z = 0.f;
  float w = 0.f;
};

using Color = Tuple;
using Point = Tuple;
using Vec = Tuple;

Tuple point(float x, float y, float z);
Tuple vec(float x, float y, float z);
Tuple color(float red, float green, float blue);
bool pointp(const Tuple &a);
bool vecp(const Tuple &a);
Vec reflect(const Vec &in, const Vec &normal);

//// templated implementation

template <typename T>
Tuple operator*(const T &num, const Tuple &t) {
  return Tuple(t.x * num, t.y * num, t.z * num, t.w * num);
}

template <typename T>
Tuple operator*(const Tuple &t, const T &num) {
  return Tuple(t.x * num, t.y * num, t.z * num, t.w * num);
}

template <typename T>
Tuple operator/(const Tuple &t, const T &num) {
  return Tuple(t.x / num, t.y / num, t.z / num, t.w / num);
}

#endif
