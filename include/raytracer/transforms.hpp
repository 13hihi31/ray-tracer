#ifndef TRANSFORMS_HPP
#define TRANSFORMS_HPP

#include <cmath>
#include <raytracer/matrix.hpp>
#include <raytracer/tuple.hpp>

inline Matrix<4> translation(float x, float y, float z) {
  return Matrix<4>({{1.0f, 0.0f, 0.0f, x},
                    {0.0f, 1.0f, 0.0f, y},
                    {0.0f, 0.0f, 1.0f, z},
                    {0.0f, 0.0f, 0.0f, 1.0f}});
}

inline Matrix<4> scaling(float x, float y, float z) {
  return Matrix<4>({{x, 0.0f, 0.0f, 0.0f},
                    {0.0f, y, 0.0f, 0.0f},
                    {0.0f, 0.0f, z, 0.0f},
                    {0.0f, 0.0f, 0.0f, 1.0f}});
}

inline Matrix<4> rotation_x(float angle) {
  return Matrix<4>({{1.0f, 0.0f, 0.0f, 0.0f},
                    {0.0f, cosf(angle), -sinf(angle), 0.0f},
                    {0.0f, sinf(angle), cosf(angle), 0.0f},
                    {0.0f, 0.0f, 0.0f, 1.0f}});
}

inline Matrix<4> rotation_y(float angle) {
  return Matrix<4>({{cosf(angle), 0.0f, sinf(angle), 0.0f},
                    {0.0f, 1.0f, 0.0f, 0.0f},
                    {-sinf(angle), 0.0f, cosf(angle), 0.0f},
                    {0.0f, 0.0f, 0.0f, 1.0f}});
}

inline Matrix<4> rotation_z(float angle) {
  return Matrix<4>({{cosf(angle), -sinf(angle), 0.0f, 0.0f},
                    {sinf(angle), cosf(angle), 0.0f, 0.0f},
                    {0.0f, 0.0f, 1.0f, 0.0f},
                    {0.0f, 0.0f, 0.0f, 1.0f}});
}

inline Matrix<4> shearing(float xy, float xz, float yx, float yz, float zx,
                          float zy) {
  return Matrix<4>({{1.0f, xy, xz, 0.0f},
                    {yx, 1.0f, yz, 0.0f},
                    {zx, zy, 1.0f, 0.0f},
                    {0.0f, 0.0f, 0.0f, 1.0f}});
}

inline Matrix<4> view_transform(const Point &from, const Point &to,
                                const Vec &up) {
  Vec forward = norm(to - from);
  Vec left = cross(forward, norm(up));
  Vec true_up = cross(left, forward);
  Matrix<4> orientation({{left.x, left.y, left.z, 0.0f},
                         {true_up.x, true_up.y, true_up.z, 0.0f},
                         {-forward.x, -forward.y, -forward.z, 0.0f},
                         {0.0f, 0.0f, 0.0f, 1.0f}});
  return mul(orientation, translation(-from.x, -from.y, -from.z));
}

#endif
