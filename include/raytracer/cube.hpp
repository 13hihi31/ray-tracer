#ifndef CUBE_HPP
#define CUBE_HPP

#include <raytracer/shape.hpp>

class Cube : public Shape {
 public:
  Cube() = default;
  Cube(const Matrix<4> &transform, const Material &material);

  Vec local_normal_at(const Point &local_point,
                      const Intersection &intersection) const override;
  void local_intersect(const Ray &local_ray,
                       std::vector<Intersection> &intersections) override;
  BoundingBox get_bounds() const override;
};

#endif
